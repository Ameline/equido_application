import 'package:equido_flutter/src/authentification/Auth_Service.dart';
import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/ui/connection/dialog/connection_dialog.dart';
import 'package:equido_flutter/ui/connection/service/sign_in_validation.dart';
import 'package:equido_flutter/ui/connection/view/forgot_password.dart';
import 'package:equido_flutter/ui/widget/equido_asset.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/button_list.dart';
import 'package:flutter_signin_button/button_view.dart';

import '../service/authentification_async.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<StatefulWidget> createState() => SignInPageState();
}

class SignInPageState extends State<SignInPage> {
  final SignInValidation _signInValidation = SignInValidation();
  bool _passwordVisible = true;
  GoogleAuthService authService = GoogleAuthService();

  SignInPageState();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const SizedBox(height: 45.0),
              Image.asset(
                EquidoAsset.cyanEquidoLogo,
                width: 70,
                height: 70,
              ),
              Text(
                EquidoLocalizations.of(context).getText("app_name"),
                style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontFamily: 'Equido',
                    fontSize: 30),
              ),
              const SizedBox(height: 45.0),
              emailField(),
              const SizedBox(height: 25.0),
              passwordField(),
              const SizedBox(height: 35.0),
              loginButon(),
              const SizedBox(height: 35.0),
              googleLoginButon(),
              const SizedBox(height: 15.0),
              forgotPassWord(),
            ],
          ),
        ),
      ),
    );
  }

  loginButon() {
    return Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.cyan,
        child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: signInProcess(),
          child: Text(
            EquidoLocalizations.of(context).getText("connection"),
            style: const TextStyle(color: Colors.white),
            textAlign: TextAlign.center,
          ),
        ));
  }

  googleLoginButon() {
    return SignInButton(Buttons.Google, onPressed: signGoogleInProcess());
  }

  signInProcess() {
    return () async {
      if (_signInValidation.allValuesValid(context)) {
        const AuthentificationAsync().login(context, _signInValidation,
            (accessToken) {
          if (accessToken != null && accessToken != "error") {
            Navigator.popUntil(context, ModalRoute.withName('/'));
          } else {
            ConnectionDialog.showInvalidCredential(context);
            Navigator.popUntil(context, ModalRoute.withName('/'));
          }
          throw '';
        });
      }
      // reload to see error message
      setState(() {});
    };
  }

  signGoogleInProcess() {
    return () async {
      GoogleAuthService().signInWithGoogle().then((onValue) {
        //Navigator.popUntil(context, ModalRoute.withName('/'));
      });

      // reload to see error message
      setState(() {});
    };
  }

  emailField() {
    return EquidoWidget.buildRoundedTextField(
        _signInValidation.email,
        EquidoLocalizations.of(context).getText("email"),
        _signInValidation.errorMessageEmail(context));
  }

  passwordField() {
    return EquidoWidget.buildRoundedTextFieldVisibleControl(
        _signInValidation.password,
        EquidoLocalizations.of(context).getText("password"),
        _signInValidation.errorMessagePassword(context),
        _passwordVisible,
        () => setState(() => _passwordVisible = !_passwordVisible));
  }

  forgotPassWord() {
    return Material(
      color: Colors.white,
      child: GestureDetector(
          child: const Text("Mot de passe oublié",
              style: TextStyle(color: Colors.black38)),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const ForgotPasswordPage()));
          }),
    );
  }
}
