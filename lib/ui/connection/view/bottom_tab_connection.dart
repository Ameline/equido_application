import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/ui/connection/view/sign_in.dart';
import 'package:equido_flutter/ui/connection/view/sign_up.dart';
import 'package:flutter/material.dart';

class BottomTabConnection extends StatefulWidget {
  const BottomTabConnection({super.key});

  @override
  BottomTabConnectionState createState() => BottomTabConnectionState();
}

class BottomTabConnectionState extends State<BottomTabConnection>
    with SingleTickerProviderStateMixin {
  late TabController controller;

  @override
  void initState() {
    super.initState();
    controller = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: TabBarView(
        controller: controller,
        children: const <Widget>[SignInPage(), SignUpPage()],
      ),
      bottomNavigationBar: Container(
        color: Colors.white,
        child: TabBar(
          labelColor: Theme.of(context).primaryColor,
          unselectedLabelColor: Colors.blueGrey,
          labelStyle:
              const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          unselectedLabelStyle: const TextStyle(fontWeight: FontWeight.normal),
          indicatorColor: Theme.of(context).primaryColor,
          tabs: <Tab>[
            Tab(
              height: 100,
              text: EquidoLocalizations.of(context).getText("connection"),
            ),
            Tab(
              height: 100,
              text: EquidoLocalizations.of(context).getText("inscription"),
            )
          ],
          controller: controller,
        ),
      ),
    );
  }
}
