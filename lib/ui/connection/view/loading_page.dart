import 'package:equido_flutter/ui/widget/equido_asset.dart';
import 'package:flutter/material.dart';

class LoadingPage extends StatelessWidget {
  const LoadingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
            Image.asset(
              EquidoAsset.whiteEquidoLogo,
              height: 150,
              width: 150,
            ),
                const Padding(
                padding: EdgeInsets.only(top: 30.0),
                child: Text(
                  "Equido",
                  style: TextStyle(color: Colors.white, fontSize: 50, fontFamily: 'Equido'),
                )),
          ])),
    );
  }
}
