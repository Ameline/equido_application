import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/ui/connection/service/sign_in_validation.dart';
import 'package:equido_flutter/ui/widget/equido_asset.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';

import '../service/authentification_async.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({super.key});

  @override
  State<StatefulWidget> createState() => ForgotPasswordPageState();
}

class ForgotPasswordPageState extends State<ForgotPasswordPage> {
  final SignInValidation _signInValidation = SignInValidation();

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(36.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const SizedBox(height: 45.0),
            Image.asset(
              EquidoAsset.cyanEquidoLogo,
              width: 70,
              height: 70,
            ),
            Text(
              EquidoLocalizations.of(context).getText("app_name"),
              style: TextStyle(color: Theme.of(context).primaryColor, fontFamily: 'Equido', fontSize: 30),
            ),
            const SizedBox(height: 45.0),
            emailField(),
            const SizedBox(height: 35.0),
            sendButton(),
            const SizedBox(height: 15.0),
            backButton()
          ],
        ),
      ),
    );
  }

  emailField() {
    return EquidoWidget.buildRoundedTextField(_signInValidation.email, "Mail", _signInValidation.errorMessageEmail(context));
  }

  backButton() {
    return GestureDetector(
      child: const Text("Retour a la connexion"),
      onTap: () {
        Navigator.pop(context);
      },
    );
  }

  sendButton() {
    return Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.cyan,
        child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: forgotPasswordProcess(),
          child: const Text(
            "Confirmation",
            style: TextStyle(color: Colors.white),
            textAlign: TextAlign.center,
          ),
        ));
  }

  forgotPasswordProcess() {
    if (_signInValidation.emailValid(context)) {
      const AuthentificationAsync().forgotPassword(context, _signInValidation, () {
        const AuthentificationAsync().forgotPasswordSucess(context, _signInValidation, () {
          if (!mounted) return;
          Navigator.popUntil(context, ModalRoute.withName('/'));
        });
      });
    }
  }
}
