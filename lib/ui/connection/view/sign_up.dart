import 'package:equido_flutter/src/authentification/authentification_service.dart';
import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/ui/connection/dialog/connection_dialog.dart';
import 'package:equido_flutter/ui/connection/service/authentification_async.dart';
import 'package:equido_flutter/ui/connection/service/sign_up_validation.dart';
import 'package:equido_flutter/ui/equido_alert_dialog.dart';
import 'package:equido_flutter/ui/widget/equido_asset.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<StatefulWidget> createState() => SignUpPageState();
}

class SignUpPageState extends State<SignUpPage> {
  final AuthentificationService _authentificationService = AuthentificationService();
  final SignUpValidation _signUpValidation = SignUpValidation();
  bool _passwordVisible = true;

  @override
  Widget build(BuildContext context) {
    final emailField = EquidoWidget.buildRoundedTextField(
        _signUpValidation.email, EquidoLocalizations.of(context).getText("email"), _signUpValidation.errorMessageMail(context));

    final usernameField = EquidoWidget.buildRoundedTextField(_signUpValidation.username,
        EquidoLocalizations.of(context).getText("username"), _signUpValidation.errorMessageUsername(context));

    final passwordField = EquidoWidget.buildRoundedTextFieldVisibleControl(
        _signUpValidation.password,
        EquidoLocalizations.of(context).getText("password"),
        _signUpValidation.errorMessagePassword(context),
        _passwordVisible,
        () => setState(() => _passwordVisible = !_passwordVisible));

    final passwordRepeteField = EquidoWidget.buildRoundedTextFieldVisibleControl(
        _signUpValidation.passwordRepete,
        EquidoLocalizations.of(context).getText("password_repete"),
        _signUpValidation.errorMessagePasswordRepete(context),
        _passwordVisible,
        () => setState(() => _passwordVisible = !_passwordVisible));

    final loginButon = Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.cyan,
        child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: () async {
            if (_signUpValidation.allValuesValid(context)) {
              //EquidoAlertDialog.onLoading(context, text: EquidoLocalizations.of(context).getText("registration_in_progress"));

              final email = _signUpValidation.email.text.trim();
              final username = _signUpValidation.username.text.trimRight().trimLeft();
              final password = _signUpValidation.password.text;

              const AuthentificationAsync().createUser(context, _authentificationService, email, username, password,
                  (refreshToken) {
                if (refreshToken.length != 3) {
                  ConnectionDialog.showWelcome(context, username, password).then((var t) {
                    _connection();
                  });
                } else if (refreshToken == "409") {
                  Navigator.popUntil(context, ModalRoute.withName('/'));
                  ConnectionDialog.showConflictAlertMail(context);
                } else {
                  Navigator.popUntil(context, ModalRoute.withName('/'));
                  ConnectionDialog.showError(context);
                }
                throw ''; // Todo: this exeption is throw when we sign up
              });
            }
            setState(() {});
          },
          child: Text(
            EquidoLocalizations.of(context).getText("inscription"),
            style: const TextStyle(color: Colors.white),
            textAlign: TextAlign.center,
          ),
        ));

    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const SizedBox(height: 45.0),
              Image.asset(
                EquidoAsset.cyanEquidoLogo,
                width: 70,
                height: 70,
              ),
              Text(
                EquidoLocalizations.of(context).getText("app_name"),
                style: TextStyle(color: Theme.of(context).primaryColor, fontFamily: 'Equido', fontSize: 30),
              ),
              const SizedBox(height: 45.0),
              emailField,
              const SizedBox(height: 10.0),
              usernameField,
              const SizedBox(height: 10.0),
              passwordField,
              const SizedBox(height: 10.0),
              passwordRepeteField,
              const SizedBox(height: 20.0),
              loginButon,
              const SizedBox(height: 15.0),
            ],
          ),
        ),
      ),
    );
  }

  _connection() async {
    EquidoAlertDialog.onLoading(context, text: EquidoLocalizations.of(context).getText("connection_loading"));

    const AuthentificationAsync().connection(context, _signUpValidation, (accessToken) {
      if (accessToken != null) {
        Navigator.popUntil(context, ModalRoute.withName('/'));
      } else {
      }
      throw '';
    });
  }
}
