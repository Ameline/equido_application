import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:flutter/material.dart';

class ConnectionDialog {
  static Future showInvalidCredential(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content:
                Text(EquidoLocalizations.of(context).getText("wrong_sign_in")),
            actions: <Widget>[
              TextButton(
                child: Text(EquidoLocalizations.of(context).getText("retries")),
                onPressed: () {
                  Navigator.popUntil(context, ModalRoute.withName('/'));
                },
              ),
            ],
          );
        });
  }

  static Future showConflictAlertMail(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(EquidoLocalizations.of(context)
                .getText("mail_already_registered")),
            actions: <Widget>[
              TextButton(
                child: Text(
                    EquidoLocalizations.of(context).getText("go_connection")),
                onPressed: () {
                  Navigator.popUntil(context, ModalRoute.withName('/'));
                },
              ),
            ],
          );
        });
  }

  static Future showError(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(EquidoLocalizations.of(context).getText("error")),
            actions: <Widget>[
              TextButton(
                style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                onPressed: () {
                  Navigator.popUntil(context, ModalRoute.withName('/'));
                },
                child: Text(EquidoLocalizations.of(context).getText("ok"),
                    style: const TextStyle(color: Colors.white)),
              ),
            ],
          );
        });
  }

  static Future showWelcome(
      BuildContext context, String username, String password) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(EquidoLocalizations.of(context)
                .getTextParameter("welcom_user", username)),
            content: Text(EquidoLocalizations.of(context)
                .getText("account_created_success")),
            actions: <Widget>[
              TextButton(
                child: Text(
                    EquidoLocalizations.of(context).getText("discover_app"),
                    style: const TextStyle(color: Colors.cyan)),
                onPressed: () async {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  static Future forgotPasswordSucess(BuildContext context, String email) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content:
                Text("Un email vient d'être envoyer à l'adresse mail $email."),
            actions: <Widget>[
              TextButton(
                style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                onPressed: () async {
                  Navigator.pop(context);
                },
                child: Text(EquidoLocalizations.of(context).getText("ok"),
                    style: const TextStyle(color: Colors.white)),
              ),
            ],
          );
        });
  }

  static Future forgotPasswordNotOk(BuildContext context, String email) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: const Text(
                "Une erreur est survenue sur notre serveur. Merci de réessayé ultérieurement"),
            actions: <Widget>[
              TextButton(
                style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                onPressed: () async {
                  Navigator.pop(context);
                },
                child: Text(EquidoLocalizations.of(context).getText("ok")),
              ),
            ],
          );
        });
  }
}
