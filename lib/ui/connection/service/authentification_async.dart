import 'package:equido_flutter/ui/connection/service/sign_in_validation.dart';
import 'package:equido_flutter/ui/connection/service/sign_up_validation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../src/authentification/authentification_service.dart';
import '../dialog/connection_dialog.dart';

//Todo SignInValidation present everywhere, delete ?
class AuthentificationAsync {
  const AuthentificationAsync();

  Future<void> forgotPasswordSucess(BuildContext context, SignInValidation signInValidation, VoidCallback onSuccess) async {
    await ConnectionDialog.forgotPasswordSucess(context, signInValidation.email.text);
    onSuccess.call();
  }

  Future<void> forgotPasswordNotOk(BuildContext context, SignInValidation signInValidation, VoidCallback onSuccess) async {
    await ConnectionDialog.forgotPasswordNotOk(context, signInValidation.email.text);
    onSuccess.call();
  }

  Future<void> forgotPassword(BuildContext context, SignInValidation signInValidation, VoidCallback onSuccess) async {
    await AuthentificationService.forgotPassword(signInValidation.email.text);
    onSuccess.call();
  }

  Future<void> login(BuildContext context, SignInValidation signInValidation, VoidCallback Function(String?) onSuccess) async {
    String? accessToken = await AuthentificationService.login(signInValidation.email.text, signInValidation.password.text);
    onSuccess.call(accessToken);
  }

  Future<void> connection(BuildContext context, SignUpValidation signUpValidation, VoidCallback Function(String?) onSuccess) async {
    String? accessToken = await AuthentificationService.login(signUpValidation.email.text, signUpValidation.password.text);
    onSuccess.call(accessToken);
  }

  Future<void> createUser(BuildContext context, AuthentificationService authentificationService, String email, String username,
      String password, VoidCallback Function(String) onSuccess) async {
    String refreshToken = await authentificationService.createUser(email, username, password);
    onSuccess.call(refreshToken);
  }
}
