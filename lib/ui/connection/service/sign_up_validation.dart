import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignUpValidation {
  static const String _emptyString = "";
  final email = TextEditingController();
  final username = TextEditingController();
  final password = TextEditingController();
  final passwordRepete = TextEditingController();

  bool _emptyEmail = false;
  bool _emptyUsername = false;
  bool _emptyPassword = false;
  bool _emptyPasswordRepete = false;

  bool _isEmailValid = true;
  bool _isUsernameValid = true;
  bool _isPasswordValid = true;

  bool _init = true;

  String errorMessagePasswordRepete(BuildContext context) {
    return _emptyPasswordRepete
        ? EquidoLocalizations.of(context).getText("requiered_fields")
        : _validatePasswordRepete(
            context, passwordRepete.text, passwordRepete.text);
  }

  String errorMessagePassword(BuildContext context) {
    return _emptyPassword
        ? EquidoLocalizations.of(context).getText("requiered_fields")
        : _validatePassword(context, password.text, passwordRepete.text);
  }

  String errorMessageUsername(BuildContext context) {
    return _emptyUsername
        ? EquidoLocalizations.of(context).getText("requiered_fields")
        : _validateUserName(context, username.text);
  }

  String errorMessageMail(BuildContext context) {
    return _emptyEmail
        ? EquidoLocalizations.of(context).getText("requiered_fields")
        : !_isEmailValid
            ? EquidoLocalizations.of(context).getText("incorect_email")
            : "";
  }

  bool allValuesValid(BuildContext context) {
    _init = false;
    _emptyEmail = email.text.isEmpty;
    _emptyUsername = username.text.isEmpty;
    _emptyPassword = password.text.isEmpty;
    _emptyPasswordRepete = passwordRepete.text.isEmpty;

    _isEmailValid = _validateEmail(email.text.trim());
    _isUsernameValid =
        _validateUserName(context, username.text) == _emptyString;
    _isPasswordValid =
        _validatePassword(context, password.text, passwordRepete.text) ==
            _emptyString;

    return !_emptyUsername &&
        !_emptyPassword &&
        !_emptyEmail &&
        _isUsernameValid &&
        _isPasswordValid &&
        _isEmailValid;
  }

  bool _validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    return regex.hasMatch(value);
  }

  String _validatePasswordRepete(
      BuildContext context, String value, String repete) {
    if (value != repete) {
      return EquidoLocalizations.of(context).getText("password_not_same");
    }
    return _emptyString;
  }

  String _validatePassword(BuildContext context, String value, String repete) {
    if (_init) return _emptyString;

    if (value.length < 8 || value.length > 30) {
      return EquidoLocalizations.of(context).getText("valid_password_size");
    } else if (value.contains(' ')) {
      return EquidoLocalizations.of(context).getText("valid_password_space");
    }

    return _validatePasswordRepete(context, value, repete);
  }

  String _validateUserName(BuildContext context, String value) {
    if (_init) return _emptyString;

    value = value.trimRight().trimLeft();
    if (value.length < 3 || value.length > 30) {
      return EquidoLocalizations.of(context).getText("valid_username_size");
    }
    return _emptyString;
  }
}
