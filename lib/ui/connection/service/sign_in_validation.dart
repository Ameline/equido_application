import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignInValidation {
  final email = TextEditingController();
  final password = TextEditingController();

  bool _emptyEmail = false;
  bool _emptyPassword = false;

  bool _isEmailValid = true;
  bool _isPasswordValid = true;

  bool _init = true;

  final String _emptyString = "";

  String errorMessagePassword(BuildContext context) {
    return _emptyPassword
        ? EquidoLocalizations.of(context).getText("requiered_fields")
        : _validatePassword(context, password.text);
  }

  String errorMessageEmail(BuildContext context) {
    return _emptyEmail
        ? EquidoLocalizations.of(context).getText("requiered_fields")
        : _validateEmail(context, email.text);
  }

  bool allValuesValid(BuildContext context) {
    _init = false;

    _emptyEmail = email.text.isEmpty;
    _emptyPassword = password.text.isEmpty;

    _isEmailValid = _validateEmail(context, email.text) == _emptyString;
    _isPasswordValid = _validatePassword(context, email.text) == _emptyString;

    return !_emptyEmail && !_emptyPassword && _isEmailValid && _isPasswordValid;
  }

  bool emailValid(BuildContext context) {
    _init = false;

    _emptyEmail = email.text.isEmpty;
    _isEmailValid = _validateEmail(context, email.text) == _emptyString;

    return !_emptyEmail && _isEmailValid;
  }

  String _validatePassword(BuildContext context, String value) {
    if (_init) return _emptyString;

    if (value.length < 8 || value.length > 30) {
      return EquidoLocalizations.of(context).getText("valid_password_size");
    } else if (value.contains(' ')) {
      return EquidoLocalizations.of(context).getText("valid_password_space");
    }

    return _emptyString;
  }

  String _validateEmail(BuildContext context, String value) {
    if (_init) return _emptyString;

    value = value.trimRight().trimLeft();
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(value)) {
      return EquidoLocalizations.of(context).getText("incorect_email");
    }

    return _emptyString;
  }
}
