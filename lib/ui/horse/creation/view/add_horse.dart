import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/ui/horse/creation/service/add_horse_service.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';

import '../add_horse_information.dart';

class CreateHorse extends StatefulWidget {
  const CreateHorse({super.key});

  @override
  State<StatefulWidget> createState() => CreateHorseState();
}

class CreateHorseState extends State<CreateHorse> {
  final AddHorseService _addHorseService = AddHorseService();

  @override
  Widget build(BuildContext context) {
    final topAppBar = AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          EquidoLocalizations.of(context).getText("add_horse"),
          style: const TextStyle(color: Colors.cyan),
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.cyan,
          ),
          onPressed: () => Navigator.pop(context, false),
        ));

    final horseNameField = EquidoWidget.buildRoundedTextField(
        _addHorseService.horseName,
        EquidoLocalizations.of(context).getText("horse_name"),
        _addHorseService.errorMessageHorseName(context));

    final body = Padding(
        padding: const EdgeInsets.all(36.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            horseNameField,
            const SizedBox(height: 45.0),
            SizedBox(
                width: double.infinity,
                height: 50.0,
                child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: WidgetStateProperty.all<Color>(
                            Theme.of(context).primaryColor),
                        shape: WidgetStateProperty.all<OutlinedBorder>(
                            const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(30))))),
                    child: Text(EquidoLocalizations.of(context).getText("next"),
                        style: const TextStyle(color: Colors.white)),
                    onPressed: () async {
                      if (_addHorseService.isHorseNameValide(context)) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddHorseInformation(
                                  _addHorseService.horseName.text)),
                        );
                      }
                      setState(() {});
                    }))
          ],
        ));

    return Scaffold(
        backgroundColor: Colors.white, appBar: topAppBar, body: body);
  }
}
