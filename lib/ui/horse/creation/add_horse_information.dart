import 'dart:async';

import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/src/models/horse/breed.dart';
import 'package:equido_flutter/src/models/horse/color.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/horse/kind.dart';
import 'package:equido_flutter/ui/form/equido_form.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../equido_alert_dialog.dart';

class AddHorseInformation extends StatefulWidget {
  final String horseName;

  const AddHorseInformation(this.horseName, {super.key});

  @override
  State<StatefulWidget> createState() => AddHorseInformationState();
}

class AddHorseInformationState extends State<AddHorseInformation> {
  final _formKey = GlobalKey();

  String kind = Kind.defaultKind();
  String breed = Breed.defaultBreed();
  String color = HorseColor.defaultColor();

  int size = 0;

  DateTime? birthDate;
  String errorMessage = "";

  Future _selectDate(BuildContext context) async {
    DateTime initDate = birthDate ?? DateTime.now();

    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: initDate,
      firstDate: DateTime(1980),
      lastDate: DateTime(DateTime.now().year + 1),
    );

    if (picked != null && picked != birthDate) {
      setState(() {
        birthDate = picked;
      });
    }
  }

  Row _makeDateForm(String text) {
    return Row(
      children: <Widget>[
        EquidoWidget.makeSubTitle(text),
        Expanded(
            flex: 2,
            child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(Colors.white),
                    shape: WidgetStateProperty.all<OutlinedBorder>(
                        ContinuousRectangleBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                            side: BorderSide(color: Colors.grey[300]!)))),
                onPressed: () => _selectDate(context),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      birthDateText(),
                      birthDate == null ? const SizedBox() : button()
                    ])))
      ],
    );
  }

  Widget button() {
    return IconButton(
        onPressed: () => setState(() {
              birthDate = null;
            }),
        icon: const Icon(Icons.clear, color: Colors.red));
  }

  Widget _laterButton(HorseBloc bloc) {
    return ElevatedButton(
        style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(Colors.grey[300]!)),
        child: Text(EquidoLocalizations.of(context).getText("later"),
            style: const TextStyle(color: Colors.white)),
        onPressed: () async {
          EquidoAlertDialog.onLoading(context);

          Horse newHorse = Horse.create(
              name: widget.horseName,
              size: size,
              kind: kind,
              color: color,
              breed: breed,
              birthDate: birthDate);

          HorseBlocEvent<Horse> blocHorse = HorseBlocEvent<Horse>(
              eventType: HorseBlocType.addHorse, object: newHorse);
          bloc.actionEventsController.add(blocHorse);

          Navigator.popUntil(context, ModalRoute.withName('/'));
        });
  }

  Widget _confirmButton(HorseBloc bloc) {
    return ElevatedButton(
        style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(Colors.cyan)),
        child: Text(
          EquidoLocalizations.of(context).getText("ok"),
          style: const TextStyle(color: Colors.white),
        ),
        onPressed: () async {
          String errorMessage = _checkValueOk();
          if (errorMessage.isEmpty) {
            EquidoAlertDialog.onLoading(context);

            Horse newHorse = Horse.create(
                name: widget.horseName,
                size: size,
                kind: kind,
                color: color,
                breed: breed,
                birthDate: birthDate);

            HorseBlocEvent<Horse> blocHorse = HorseBlocEvent<Horse>(
                eventType: HorseBlocType.addHorse, object: newHorse);
            bloc.actionEventsController.add(blocHorse);

            Navigator.popUntil(context, ModalRoute.withName('/'));
          } else {
            setState(() {
              this.errorMessage = errorMessage;
            });
          }
        });
  }

  String _checkValueOk() {
    if (size > 250) {
      return "La taille ne peut être supérieur a 250 cm.";
    }
    return "";
  }

  @override
  Widget build(BuildContext context) {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);

    final topAppBar = AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          widget.horseName,
          style: const TextStyle(color: Colors.cyan),
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.cyan,
          ),
          onPressed: () => Navigator.pop(context, false),
        ));

    final body = SingleChildScrollView(
        padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        child: Builder(
            builder: (context) => Form(
                key: _formKey,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      EquidoWidget.titleBorderBottum(
                          EquidoLocalizations.of(context)
                              .getText("additional_information")),
                      EquidoForm.makeDropDown(
                          EquidoLocalizations.of(context).getText("gender"),
                          kind,
                          Kind.kinds,
                          (value) => setState(() => kind = value!)),
                      EquidoForm.makeTextFielForm(
                        EquidoLocalizations.of(context).getText("horse_size"),
                        size.toString(),
                        (value) => setState(() => size = int.parse(value)),
                        textInputType: TextInputType.number,
                      ),
                      EquidoForm.makeDropDown(
                          EquidoLocalizations.of(context)
                              .getText("horse_color"),
                          color,
                          HorseColor.colors,
                          (value) => setState(() => color = value!)),
                      EquidoForm.makeDropDown(
                          EquidoLocalizations.of(context)
                              .getText("horse_breed"),
                          breed,
                          Breed.breeds,
                          (value) => setState(() => breed = value!)),
                      _makeDateForm(
                          EquidoLocalizations.of(context).getText("birthdate")),
                      const SizedBox(height: 45.0),
                      _errorMessage(),
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: _laterButton(applicationBloc),
                          ),
                          const SizedBox(width: 10.0),
                          Expanded(
                            flex: 2,
                            child: _confirmButton(applicationBloc),
                          ),
                        ],
                      )
                    ]))));

    return Scaffold(
        backgroundColor: Colors.white, appBar: topAppBar, body: body);
  }

  Widget _errorMessage() {
    if (errorMessage.isEmpty) {
      return const SizedBox();
    }
    return Text(errorMessage, style: const TextStyle(color: Colors.red));
  }

  Widget birthDateText() {
    return birthDate == null
        ? Align(
            alignment: Alignment.centerLeft,
            child: Text(EquidoLocalizations.of(context).getText("unknown"),
                style: const TextStyle(color: Colors.black)))
        : Align(
            alignment: Alignment.centerLeft,
            child: Text(DateFormat("dd-MM-yyyy").format(birthDate!),
                style: const TextStyle(color: Colors.black)));
  }
}
