import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:flutter/cupertino.dart';

class AddHorseService {
  final horseName = TextEditingController();
  bool _emptyHorseName = false;
  late bool _isHorseNameValid;
  final String _emptyString = "";

  String errorMessageHorseName(BuildContext context) {
    return _emptyHorseName
        ? EquidoLocalizations.of(context).getText("requiered_fields")
        : _validateHorseName(context, horseName.text);
  }

  bool isHorseNameValide(BuildContext context) {
    _emptyHorseName = horseName.text.isEmpty;

    _isHorseNameValid =
        _validateHorseName(context, horseName.text) == _emptyString;

    return !_emptyHorseName && _isHorseNameValid;
  }

  String _validateHorseName(BuildContext context, String value) {
    if (horseName.text.isEmpty) return _emptyString;

    value = value.trimRight().trimLeft();
    if (value.length < 3 || value.length > 30) {
      return EquidoLocalizations.of(context).getText("valid_username_size");
    }
    return _emptyString;
  }
}
