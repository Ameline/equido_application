import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/event/outing.dart';
import 'package:equido_flutter/src/models/event/training.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class EventChar {
  final String weekday;
  final List<Event> events;

  EventChar(this.weekday, this.events);
}

class EventsBarChartService {
  static const int numberOfDay = 14;

  static int _makeTimestampData(EventChar event) {
    return event.events.isEmpty
        ? 0
        : event.events
            .map((event) => _makeTimestampData2(event))
            .reduce((curr, next) => curr + next);
  }

  static int _makeTimestampData2(Event event) {
    if (event.eventType == EventType.training) {
      Training training = event as Training;
      return training.getDuration()!.inMinutes;
    } else if (event.eventType == EventType.outing) {
      Outing outing = event as Outing;
      return outing.getDuration()!.inMinutes;
    } else {
      return 0;
    }
  }

  static List<CartesianSeries<dynamic, dynamic>> createEventData(
      List<Event> events,
      {trainingColorHex = "#a5e3bc",
      outingColorHex = "#ff4c4c"}) {
    DateTime currentDay = DateTime.now();
    DateTime lastDay = DateTime(
        currentDay.year, currentDay.month, currentDay.day - numberOfDay);
    events = events.where((event) => event.start.isAfter(lastDay)).toList();

    List<EventChar> outingsData = [];
    List<EventChar> trainingsData = [];

    for (int i = 0; i < 7; i++) {
      Iterable tmpEvents = events
          .whereType<Outing>()
          .where((event) =>
              DateTime(
                  event.start.year, event.start.month, event.start.day - 1) ==
              currentDay)
          .toList();
      outingsData.add(EventChar(
          EquidoDate.translateWeekday(currentDay.weekday.toString()),
          tmpEvents as List<Event>));

      tmpEvents = events
          .where((event) => event.eventType == EventType.training)
          .where((event) =>
              DateTime(
                  event.start.year, event.start.month, event.start.day - 1) ==
              currentDay)
          .toList();
      trainingsData.add(EventChar(
          EquidoDate.translateWeekday(currentDay.weekday.toString()),
          tmpEvents));

      currentDay =
          DateTime(currentDay.year, currentDay.month, currentDay.day - 1);
    }

    for (int i = 7; i < numberOfDay + 1; i++) {
      Iterable tmpEvents = events
          .whereType<Outing>()
          .where((event) =>
              DateTime(
                  event.start.year, event.start.month, event.start.day - 1) ==
              currentDay)
          .toList();
      outingsData.add(EventChar(
          EquidoDate.translateWeekday(currentDay.weekday.toString()),
          tmpEvents as List<Event>));

      tmpEvents = events
          .where((event) => event.eventType == EventType.training)
          .where((event) =>
              DateTime(
                  event.start.year, event.start.month, event.start.day - 1) ==
              currentDay)
          .toList();
      trainingsData.add(EventChar(
          EquidoDate.translateWeekday(currentDay.weekday.toString()),
          tmpEvents));

      currentDay =
          DateTime(currentDay.year, currentDay.month, currentDay.day - 1);
    }

    outingsData = outingsData.reversed.toList();
    trainingsData = trainingsData.reversed.toList();

    return [
      BarSeries<EventChar, String>(
        dataSource: outingsData,
        name: 'Sortie',
        xValueMapper: (EventChar event, _) => event.weekday,
        yValueMapper: (EventChar event, _) => _makeTimestampData(event),
        // TODO: color
        // color: Color.fromHex(code: trainingColorHex),
      ),
      BarSeries<EventChar, String>(
        // TODO: color
        // seriesColor: charts.Color.fromHex(code: outingColorHex),
        name: 'Entrainement',
        xValueMapper: (EventChar event, _) => event.weekday,
        yValueMapper: (EventChar event, _) => _makeTimestampData(event),
        dataSource: trainingsData,
      ),
    ];
  }
}
