import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/event/outing.dart';
import 'package:equido_flutter/src/models/event/training.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class EventChar {
  final String weekday;
  final List<Event> events;

  EventChar(this.weekday, this.events);
}

class TrainingsBarChartService {
  static const numberOfDay = 14;

  static int _makeTimestampData(EventChar event) {
    return event.events.isEmpty
        ? 0
        : event.events
            .map((event) => _makeTimestampData2(event))
            .reduce((curr, next) => curr + next);
  }

  static int _makeTimestampData2(Event event) {
    if (event.eventType == EventType.training) {
      Training training = event as Training;
      return training.getDuration()!.inMinutes;
    } else if (event.eventType == EventType.outing) {
      Outing outing = event as Outing;
      return outing.getDuration()!.inMinutes;
    } else {
      return 0;
    }
  }

  static List<ChartSeries<EventChar, String>> createTrainingData(
      List<Event> events,
      {trainingColorHex = "#a5e3bc",
      outingColorHex = "#ff4c4c"}) {
    DateTime currentDay = DateTime.now();
    DateTime lastDay = DateTime(
        currentDay.year, currentDay.month, currentDay.day - numberOfDay);
    events = events.where((event) => event.start.isAfter(lastDay)).toList();

    List<EventChar> trainingsData = [];

    for (int i = 0; i < numberOfDay + 1; i++) {
      Iterable tmpEvents = events
          .where((event) => event.eventType == EventType.training)
          .where((event) => event.start.weekday == currentDay.weekday)
          .toList();
      trainingsData.add(
          EventChar(currentDay.weekday.toString(), tmpEvents as List<Event>));

      currentDay =
          DateTime(currentDay.year, currentDay.month, currentDay.day - 1);
    }

    trainingsData = trainingsData.reversed.toList();

    return [
      BarSeries<EventChar, String>(
        // TODO
        //seriesColor: charts.Color.fromHex(code: outingColorHex),
        name: 'Entrainement',
        xValueMapper: (EventChar event, _) =>
            EquidoDate.translateWeekday(event.weekday),
        yValueMapper: (EventChar event, _) => _makeTimestampData(event),
        dataSource: trainingsData,
        // fillColorFn: (EventChar event, _) => charts.ColorUtil.fromDartColor(Colors.transparent),
      ),
      BarSeries<EventChar, String>(
        // seriesColor: charts.Color.fromHex(code: outingColorHex),
        name: 'Entrainement',
        xValueMapper: (EventChar event, _) =>
            EquidoDate.translateWeekday(event.weekday),
        yValueMapper: (EventChar event, _) => _makeTimestampData(event),
        dataSource: trainingsData,
        // fillColorFn: (EventChar event, _) => charts.ColorUtil.fromDartColor(Colors.cyan),
      ),
      BarSeries<EventChar, String>(
        // seriesColor: charts.Color.fromHex(code: outingColorHex),
        name: 'Entrainement',
        xValueMapper: (EventChar event, _) =>
            EquidoDate.translateWeekday(event.weekday),
        yValueMapper: (EventChar event, _) => _makeTimestampData(event),
        dataSource: trainingsData,
        // fillColorFn: (EventChar event, _) => charts.ColorUtil.fromDartColor(Colors.transparent),
      ),
    ];
  }

  static List<BarSeries<EventChar, String>> createOutingData(List<Event> events,
      {trainingColorHex = "#a5e3bc", outingColorHex = "#ff4c4c"}) {
    DateTime currentDay = DateTime.now();
    DateTime lastDay =
        DateTime(currentDay.year, currentDay.month, currentDay.day - 6);
    events = events.where((event) => event.start.isAfter(lastDay)).toList();

    List<EventChar> trainingsData = [];

    for (int i = 0; i < numberOfDay + 1; i++) {
      List<Event> tmpEvents = events
          .where((event) => event.eventType == EventType.outing)
          .where((event) => event.start.weekday == currentDay.weekday)
          .toList();
      trainingsData.add(EventChar(currentDay.weekday.toString(), tmpEvents));

      currentDay =
          DateTime(currentDay.year, currentDay.month, currentDay.day - 1);
    }

    trainingsData = trainingsData.reversed.toList();

    return [
      BarSeries<EventChar, String>(
        // TODO
        // seriesColor: charts.Color.fromHex(code: outingColorHex),
        name: 'Sorties',
        xValueMapper: (EventChar event, _) =>
            EquidoDate.translateWeekday(event.weekday),
        yValueMapper: (EventChar event, _) => _makeTimestampData(event),
        dataSource: trainingsData,
        //fillColorFn: (EventChar event, _) => charts.ColorUtil.fromDartColor(Colors.transparent),
      ),
      BarSeries<EventChar, String>(
        // seriesColor: charts.Color.fromHex(code: outingColorHex),
        name: 'Sorties',
        xValueMapper: (EventChar event, _) =>
            EquidoDate.translateWeekday(event.weekday),
        yValueMapper: (EventChar event, _) => _makeTimestampData(event),
        dataSource: trainingsData,
        // fillColorFn: (EventChar event, _) => charts.ColorUtil.fromDartColor(Colors.green),
      ),
      BarSeries<EventChar, String>(
        // seriesColor: charts.Color.fromHex(code: outingColorHex),
        name: 'Sorties',
        xValueMapper: (EventChar event, _) =>
            EquidoDate.translateWeekday(event.weekday),
        yValueMapper: (EventChar event, _) => _makeTimestampData(event),
        dataSource: trainingsData,
        //fillColorFn: (EventChar event, _) => charts.ColorUtil.fromDartColor(Colors.transparent),
      ),
    ];
  }
}
