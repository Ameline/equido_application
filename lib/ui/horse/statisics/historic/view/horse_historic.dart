import 'package:equido_flutter/ui/event/creation/view/event_selection.dart';
import 'package:equido_flutter/ui/event/list/event_list_light.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';

class HorseHistoric extends StatefulWidget {
  final EventListLight eventList;
  final String horseName;

  const HorseHistoric(this.horseName, this.eventList, {super.key});

  @override
  createState() {
    return _HorseHistoric();
  }
}

class _HorseHistoric extends State<HorseHistoric> {
  _HorseHistoric();

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = widget.eventList.buildSlivers();

    // widgets contient toujours le header
    if (widgets.length <= 1) {
      return _empty(widget.horseName);
    }
    return CustomScrollView(
      slivers: widget.eventList.buildSlivers(),
    );
  }

  Widget _empty(String horseName) {
    return Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: [
      Image.asset("assets/logos/empty_chart.png", color: Colors.grey, width: 70, height: 70),
      const SizedBox(height: 20),
      SizedBox(
          width: 200,
          child: Text(
            "Vous n'avez aucun évènement défini pour $horseName",
            textAlign: TextAlign.center,
            style: const TextStyle(color: Colors.grey),
          )),
      const SizedBox(height: 50),
      SizedBox(
          width: 300,
          child: EquidoWidget.blueButton(context, " Ajouter un évènement pour $horseName",
              () => Navigator.push(context, MaterialPageRoute(builder: (context) => const EventSelection(true))))),
    ]));
  }
}
