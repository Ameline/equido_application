import 'package:equido_flutter/src/bloc/events_bloc.dart';
import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/ui/event/list/event_list_light.dart';
import 'package:equido_flutter/ui/horse/info/view/horse_details.dart';
import 'package:equido_flutter/ui/horse/statisics/commonCare/cares_statistics.dart';
import 'package:equido_flutter/ui/horse/statisics/historic/view/horse_historic.dart';
import 'package:equido_flutter/ui/horse/statisics/sharing/view/sharing_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HorseStatisticsTabs extends StatefulWidget {
  final String horseId;
  final int? initIndex;

  static const historicIndex = 0;
  static const careIndex = 1;
  static const sharingIndex = 2;

  const HorseStatisticsTabs(this.horseId, this.initIndex, {super.key});

  @override
  State<StatefulWidget> createState() {
    return HorseStatisticsTabsState();
  }
}

class HorseStatisticsTabsState extends State<HorseStatisticsTabs> {
  EventListLight eventList = EventListLight();

  @override
  void initState() {
    super.initState();
  }

  // _tabs and _childs need to have the same lenght
  final List<Widget> _tabs = [
    const Tab(text: 'Historique'),
    const Tab(text: 'Soin'),
    const Tab(text: 'Partage')
  ];

  List<Widget> _childs(Horse horse, List<Event> events) {
    return [
      HorseHistoric(horse.name, eventList),
      CaresStatistics(horse.id),
      SharingView(horse.id)
    ];
  }

  @override
  Widget build(BuildContext context) {
    final horsesBloc = BlocProvider.of<HorseBloc>(context);
    final eventsBloc = BlocProvider.of<EventBloc>(context);

    return StreamBuilder<List<Horse>>(
        stream: horsesBloc.getHorses,
        initialData: horsesBloc.horses,
        builder: (context, snapshotHorses) {
          Horse? horse = getCurrentHorse(snapshotHorses.data!);

          if (horse == null) {
            // In this case we have deleted the horse and return int this page, but this page do not exist
            return const SizedBox();
          }

          return StreamBuilder<List<Event>>(
              stream: eventsBloc.getEvents,
              initialData: eventsBloc.events,
              builder: (context, snapshotEvents) {
                List<Event> events = snapshotEvents.data!
                    .where((event) => event.horseId == widget.horseId)
                    .toList();

                eventList.initData(events);
                eventList.setContext(context);

                return MaterialApp(
                  home: DefaultTabController(
                    initialIndex: widget.initIndex ?? 0,
                    length: _tabs.length,
                    child: Scaffold(
                      appBar: _appBar(horse),
                      body: TabBarView(
                        children: _childs(horse, events),
                      ),
                    ),
                  ),
                );
              });
        });
  }

  PreferredSizeWidget _appBar(Horse horse) {
    return AppBar(
      backgroundColor: Colors.white,
      title: Text(
        horse.name,
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Theme.of(context).primaryColor,
        ),
        onPressed: () => Navigator.pop(context, false),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.info, color: Theme.of(context).primaryColor),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HorseDetails(horse.id)),
            );
          },
        )
      ],
      bottom: TabBar(
        labelColor: Colors.grey,
        indicatorColor: Colors.cyan,
        tabs: _tabs,
      ),
    );
  }

  Horse? getCurrentHorse(List<Horse> horses) {
    List<Horse> result =
        horses.where((horse) => horse.id == widget.horseId).toList();
    return result.isEmpty ? null : result.first;
  }
}
