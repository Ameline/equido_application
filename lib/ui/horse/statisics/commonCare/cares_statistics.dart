import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/ui/horse/statisics/commonCare/common_care_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CaresStatistics extends StatefulWidget {
  final String horseId;

  const CaresStatistics(this.horseId, {super.key});

  @override
  CaresStatisticsState createState() => CaresStatisticsState();
}

class CaresStatisticsState extends State<CaresStatistics> {
  CaresStatisticsState();

  @override
  Widget build(BuildContext context) {
    final horsesBloc = BlocProvider.of<HorseBloc>(context);

    return StreamBuilder<List<Horse>>(
        stream: horsesBloc.getHorses,
        initialData: horsesBloc.horses,
        builder: (context, horseSnapshot) {
          Horse horse = horseSnapshot.data!
              .where((horse) => horse.id == widget.horseId)
              .first;

          return SingleChildScrollView(
              child: Container(
                  padding: const EdgeInsets.all(5.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      CommonCareWidget.commonCareChart(
                          context, horse.shoeing, horse),
                      const SizedBox(height: 20),
                      CommonCareWidget.commonCareChart(
                          context, horse.vaccine, horse),
                      const SizedBox(height: 20),
                      CommonCareWidget.commonCareChart(
                          context, horse.wormer, horse),
                    ],
                  )));
        });
  }
}
