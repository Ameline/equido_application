import 'package:equido_flutter/src/models/event/care.dart';
import 'package:equido_flutter/src/models/horse/tracked_care.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';

class CareAlertConfig {
  late LinearGradient gradient;
  late LinearGradient softGradient;
  late String text;
  late String subText;
  late String careName;
  late IconData icon;
  late IconData lightIcon;
  late String careTypeIcon;
  late Color iconColor;
  late String actionText;

  CareAlertConfig(gradient, softGradient, text, subText, careName);

  CareAlertConfig.warning(TrackedCare care) {
    gradient = EquidoWidget.orangeGradient();
    softGradient = EquidoWidget.orangeGradientSoft();
    text = warningCareText(care);
    subText = warningCareSubText(care);
    careName = TrackedCare.careTypeToString(care.careType);
    icon = Icons.warning_rounded;
    lightIcon = Icons.warning_amber_rounded;
    iconColor = Colors.orangeAccent;
    careTypeIcon = CareLogo.getAssetBlackFromType(care.careType);
    actionText = actionTextNextCare(care);
  }

  CareAlertConfig.alert(TrackedCare care) {
    gradient = EquidoWidget.redGradient();
    softGradient = EquidoWidget.redGradientSoft();
    text = alertCareText(care);
    subText = alertCareSubText(care);
    careName = TrackedCare.careTypeToString(care.careType);
    icon = Icons.warning_rounded;
    lightIcon = Icons.warning_amber_rounded;
    iconColor = Colors.red;
    careTypeIcon = CareLogo.getAssetBlackFromType(care.careType);
    actionText = actionTextNextCare(care);
  }

  CareAlertConfig.ok(TrackedCare care) {
    text = okCareString(care.careType);
    subText = warningCareSubText(care);
    careName = TrackedCare.careTypeToString(care.careType);
    icon = Icons.check_box_rounded;
    lightIcon = Icons.check;
    iconColor = Colors.green;
    careTypeIcon = CareLogo.getAssetBlackFromType(care.careType);
    gradient = EquidoWidget.greenGradient();
    actionText = "";
  }

  CareAlertConfig.unknown(TrackedCare care) {
    text = unknowFrequencyString(care.careType);
    gradient = EquidoWidget.greyGradient();
    subText = "Cliquer ici pour définir la fréquence";
    careName = TrackedCare.careTypeToString(care.careType);
    icon = Icons.library_add_rounded;
    lightIcon = Icons.add;
    iconColor = Colors.grey;
    careTypeIcon = CareLogo.getAssetBlackFromType(care.careType);
    actionText = actionTextUnknowFrequency(care);
  }

  CareAlertConfig.unknownLastCare(TrackedCare care) {
    text = unknowLastCareString(care.careType);
    gradient = EquidoWidget.greyGradient();
    subText = "Cliquer ici pour ajouter le soin";
    careName = TrackedCare.careTypeToString(care.careType);
    icon = Icons.library_add_rounded;
    lightIcon = Icons.add;
    iconColor = Colors.grey;
    careTypeIcon = CareLogo.getAssetBlackFromType(care.careType);
    actionText = actionTextUnknowLastCare(care);
  }

  static String okCareString(CareType type) {
    return "${TrackedCare.careTypeToString(type)} à jour";
  }

  static String unknowFrequencyString(CareType type) {
    return "Fréquence de ${TrackedCare.careTypeToString(type).toLowerCase()} non renseigné";
  }

  static String unknowLastCareString(CareType type) {
    if (TrackedCare.careTypeisMasculin(type)) {
      return "Dernier ${TrackedCare.careTypeToString(type).toLowerCase()} non renseigné";
    }
    return "Dernière ${TrackedCare.careTypeToString(type).toLowerCase()} non renseignée";
  }

  static String warningCareText(TrackedCare care) {
    return "${TrackedCare.careTypeToString(care.careType)} à prévoir pour ${care.horseName}";
  }

  static String warningCareSubText(TrackedCare care) {
    return "${care.dayBeforeDeadline!.abs()} jours restants";
  }

  static String alertCareText(TrackedCare care) {
    return "${TrackedCare.careTypeToString(care.careType)} à faire pour ${care.horseName}";
  }

  static String alertCareSubText(TrackedCare care) {
    return "${care.dayBeforeDeadline!.abs()} jours de retard";
  }

  static String frequencyText(TrackedCare care) {
    if (care.frequency == null || care.frequency == 0) {
      return "Fréquence: non défini";
    }
    return "Fréquence: ${care.frequency!} jours.";
  }

  static String lastCareText(TrackedCare care) {
    if (care.lastCare == null) {
      return "Dernier ${TrackedCare.careTypeToString(care.careType).toLowerCase()}: non défini";
    }
    return "Dernier ${TrackedCare.careTypeToString(care.careType).toLowerCase()}: ${EquidoDate.makeDateEventWithYear(care.lastCare!.start)}";
  }

  static String nextCareText(TrackedCare care) {
    if (care.nextCare == null) {
      if (TrackedCare.careTypeisMasculin(care.careType)) {
        return "Prochain ${TrackedCare.careTypeToString(care.careType).toLowerCase()}: non défini";
      } else {
        return "Prochaine ${TrackedCare.careTypeToString(care.careType).toLowerCase()}: non défini";
      }
    } else if (TrackedCare.careTypeisMasculin(care.careType)) {
      return "Prochain ${TrackedCare.careTypeToString(care.careType).toLowerCase()}: ${EquidoDate.makeDateEventWithYear(care.nextCare!.start)}";
    } else {
      return "Prochaine ${TrackedCare.careTypeToString(care.careType).toLowerCase()}: ${EquidoDate.makeDateEventWithYear(care.nextCare!.start)}";
    }
  }

  static String actionTextNextCare(TrackedCare care) {
    if (TrackedCare.careTypeisMasculin(care.careType)) {
      return "Prévoir le prochain ${TrackedCare.careTypeToString(care.careType).toLowerCase()}";
    } else {
      return "Prévoir la prochaine ${TrackedCare.careTypeToString(care.careType).toLowerCase()}";
    }
  }

  static String actionTextUnknowFrequency(TrackedCare care) {
    return "Définir la fréquence de ${TrackedCare.careTypeToString(care.careType).toLowerCase()}";
  }

  static String actionTextUnknowLastCare(TrackedCare care) {
    return "Ajouter le soin";
  }
}
