import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/horse/tracked_care.dart';
import 'package:equido_flutter/ui/event/creation/view/event_details_create.dart';
import 'package:equido_flutter/ui/horse/info/view/horse_details_update.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';

import 'care_alert_config.dart';

class CommonCareWidget {
  static Widget commonCareChart(
      BuildContext context, TrackedCare care, Horse horse) {
    switch (care.status) {
      case StatusTrackedCare.unknow:
        return _unknowLastCare(context, care);
      case StatusTrackedCare.unknowFrequency:
        return _unknowCareFrequency(context, care, horse);
      case StatusTrackedCare.ok:
        return _okCare(context, care);
      case StatusTrackedCare.soon:
        return _warningCare(context, care);
      case StatusTrackedCare.delay:
        return _alertCare(context, care);
    }
  }

  static Widget _container(BuildContext context, CareAlertConfig config,
      TrackedCare care, void Function()? onPressed) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(20.0),
          boxShadow: EquidoWidget.shadow()),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(height: 10),
                  Container(
                      padding: const EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(20.0),
                          gradient: config.gradient,
                          boxShadow: EquidoWidget.shadow()),
                      child: Image.asset(
                        config.careTypeIcon,
                        width: 40,
                        height: 40,
                      )),
                  const SizedBox(height: 10),
                  Icon(
                    config.lightIcon,
                    color: config.iconColor,
                    size: 40.0,
                  ),
                ],
              )),
          Expanded(
              flex: 4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 10),
                  Text(config.text, style: const TextStyle(color: Colors.black54, fontWeight: FontWeight.bold, fontSize: 18)),
                  Text(config.subText, style: const TextStyle(color: Colors.grey, fontStyle: FontStyle.italic)),
                  const SizedBox(height: 20),
                  Text(CareAlertConfig.frequencyText(care), style: const TextStyle(color: Colors.black54)),
                  Text(CareAlertConfig.lastCareText(care), style: const TextStyle(color: Colors.black54)),
                  Text(CareAlertConfig.nextCareText(care), style: const TextStyle(color: Colors.black54)),
                  const SizedBox(height: 10),
                  onPressed != null && config.actionText.isNotEmpty
                      ? EquidoWidget.gradientButtonBlueSoft(context, config.actionText, onPressed)
                      : const SizedBox()
                ],
              ))
        ],
      ),
    );
  }

  static Widget _alertCare(BuildContext context, TrackedCare care) {
    return _container(context, CareAlertConfig.alert(care), care,
        () => goToAddCare(context, care));
  }

  static Widget _warningCare(BuildContext context, TrackedCare care) {
    return _container(context, CareAlertConfig.warning(care), care,
        () => goToAddCare(context, care));
  }

  static Widget _okCare(BuildContext context, TrackedCare care) {
    return _container(context, CareAlertConfig.ok(care), care, () => SizedBox);
  }

  static Widget _unknowCareFrequency(
      BuildContext context, TrackedCare care, Horse horse) {
    return _container(context, CareAlertConfig.unknown(care), care,
        () => goToHorseUpdate(context, horse));
  }

  static Widget _unknowLastCare(BuildContext context, TrackedCare care) {
    return _container(context, CareAlertConfig.unknownLastCare(care), care,
        () => goToAddCare(context, care));
  }

  static void goToHorseUpdate(BuildContext context, Horse horse) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => HorseDetailsUpdate(horse)));
  }

  static void goToAddCare(BuildContext context, TrackedCare care) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => EventDetailsCreate(care.careType.toString(), EventType.care, DateTime.now())));
  }
}
