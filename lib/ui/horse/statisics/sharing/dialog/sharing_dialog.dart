import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/user/profile_picture.dart';
import 'package:equido_flutter/src/models/user/sharing.dart';
import 'package:equido_flutter/src/repository/sharing_repository.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:equido_flutter/ui/horse/statisics/sharing/service/sharing_async.dart';
import 'package:equido_flutter/ui/horse/statisics/sharing/view/sharing_add.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:postgres/postgres.dart';

class SharingDialog {
  const SharingDialog();

  Future showConflict(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content:
                const Text("Ce cheval est déjà partager avec cet utilisateur"),
            actions: <Widget>[
              TextButton(
                  style: ButtonStyle(
                      backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(EquidoLocalizations.of(context).getText("ok"),
                      style: const TextStyle(color: Colors.white))),
            ],
          );
        });
  }

  Future showConfirmationUpdateSharingDays(
      BuildContext context,
      Horse horse,
      List<DateTime> days,
      List<DateTime> removedDays,
      String userSharedId) async {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);

    bool result = true;
    if (days.isNotEmpty) {
      const SharingAsync().addSharingDays(context, horse, days, userSharedId,
          (dynamic resultTmp) {
        result = resultTmp;
        return resultTmp;
      });
    }

    if (removedDays.isNotEmpty) {
      const SharingAsync().removeSharingDays(
          context, horse, removedDays, userSharedId, (dynamic param) {
        result = result && param;
        return param;
      });
    }

    if (result) {
      Horse newHorse = horse;
      Sharing addedSharing =
          newHorse.sharing.where((e) => e.userSharedId == userSharedId).first;
      newHorse.sharing = []; // Remove all sharing to find the updated in bloc
      addedSharing.sharedDays = [];
      addedSharing.sharedDays.addAll(days);

      Sharing removedSharing = Sharing.copy(addedSharing);
      removedSharing.sharedDays = [];
      removedSharing.sharedDays.addAll(removedDays);

      newHorse.sharing.add(addedSharing);
      newHorse.sharing.add(removedSharing);

      HorseBlocEvent<Horse> blocHorse = HorseBlocEvent<Horse>(
          eventType: HorseBlocType.updateSharing, object: newHorse);
      applicationBloc.actionEventsController.add(blocHorse);
      Navigator.popUntil(context, ModalRoute.withName('/'));
    }
  }

  Future showConfirmationUpdateSharingDays_2(BuildContext context, Horse horse,
      SharingAdd sharingAdd, String userSharedId) async {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);

    if (await _confirmationMessage(context, horse, sharingAdd)) {
      Horse horseUpdated = Horse.copy(horse);

      // Remove all sharing to find the updated in bloc
      horseUpdated.sharing = [];

      sharingAdd.sharingAdd.forEach((key, value) {
        // Get corresponding sharing between horse sharing and added sharing
        Sharing addedSharing = Sharing.copy(
            horse.sharing.where((e) => e.userSharedId == value.userId).first);

        // Replace shared days by added days
        addedSharing.sharedDays = [];
        addedSharing.sharedDays
            .addAll(sharingAdd.sharingAdd[addedSharing.userSharedId]!.added);

        // Replace shared days by removed days
        Sharing removedSharing = Sharing.copy(
            horse.sharing.where((e) => e.userSharedId == userSharedId).first);
        removedSharing.sharedDays = [];
        removedSharing.sharedDays
            .addAll(sharingAdd.sharingAdd[addedSharing.userSharedId]!.removed);

        // Add added sharing and remove sharing to the updated horse
        horseUpdated.sharing.add(addedSharing);
        horseUpdated.sharing.add(removedSharing);
      });

      HorseBlocEvent<Horse> blocHorse = HorseBlocEvent<Horse>(
          eventType: HorseBlocType.updateSharing, object: horseUpdated);
      applicationBloc.actionEventsController.add(blocHorse);

      //Navigator.pop(context);
      //Navigator.popUntil(context, ModalRoute.withName('/'));
    }
  }

  _addedMessage(
      String horseName, String userName, List<DateTime> date, bool isOwner) {
    date.sort((a, b) => a.compareTo(b));
    return Column(
      children: [
        Text(
            isOwner
                ? "Tu t'occuperas de $horseName"
                : "$userName s'occupera de $horseName",
            style: const TextStyle(color: Colors.cyan, fontSize: 17)),
        SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: date
                    .map((e) => Text(EquidoDate.makeDateEvent(e)))
                    .toList())),
      ],
    );
  }

  _removedMessage(
      String horseName, String userName, List<DateTime> date, bool isOwner) {
    date.sort((a, b) => a.compareTo(b));

    return Column(
      children: [
        Text(
            isOwner
                ? "Tu ne t'occuperas plus de $horseName"
                : "$userName s'occupera plus de $horseName",
            style: const TextStyle(color: Colors.cyan, fontSize: 17)),
        SingleChildScrollView(
            child: Column(
                children: date
                    .map((e) => Text(EquidoDate.makeDateEvent(e)))
                    .toList())),
      ],
    );
  }

  Future _confirmationMessage(
      BuildContext context, Horse horse, SharingAdd sharingAdd) async {
    List<Widget> content = [];
    sharingAdd.sharingAdd.forEach((key, value) {
      if (value.added.isNotEmpty) {
        content.add(_addedMessage(horse.name, value.userName, value.added,
            value.userId == horse.ownerId));
      }

      if (value.removed.isNotEmpty) {
        content.add(_removedMessage(horse.name, value.userName, value.removed,
            value.userId == horse.ownerId));
      }
    });

    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SizedBox(
                height: 200,
                child: SingleChildScrollView(child: Column(children: content))),
            actions: <Widget>[
              ElevatedButton(
                onPressed: () => Navigator.pop(context, false),
                style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all(Colors.red)),
                child: const Text("Annuler"),
              ),
              TextButton(
                  style: ButtonStyle(
                      backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                  onPressed: () => Navigator.pop(context, true),
                  child: Text(EquidoLocalizations.of(context).getText("ok"),
                      style: const TextStyle(color: Colors.white))),
            ],
          );
        });
  }

  Future addSharingDays(BuildContext context, Horse horse, List<DateTime> days,
      String userSharedId) async {
    days.sort((a, b) => a.compareTo(b));
    List<Widget> content =
        days.map((e) => Text(EquidoDate.makeDateEvent(e))).toList();

    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title:
                Text("Es-tu sûr de t'occuper de ${horse.name} ces jours-ci :"),
            content: SizedBox(
                height: 200,
                child: SingleChildScrollView(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: content))),
            actions: <Widget>[
              ElevatedButton(
                onPressed: () => Navigator.pop(context, false),
                style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all(Colors.red)),
                child: const Text("Annuler"),
              ),
              TextButton(
                  style: ButtonStyle(
                      backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                  onPressed: () => Navigator.pop(context, true),
                  child: Text(EquidoLocalizations.of(context).getText("ok"),
                      style: const TextStyle(color: Colors.white))),
            ],
          );
        });
  }

  Future removeSharingDays(BuildContext context, Horse horse,
      List<DateTime> removedDays, String userSharedId) async {
    removedDays.sort((a, b) => a.compareTo(b));
    List<Widget> content =
        removedDays.map((e) => Text(EquidoDate.makeDateEvent(e))).toList();

    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
                "Es-tu sûr de ne plus t'occuper de ${horse.name} ces jours-ci :"),
            content: SizedBox(
                height: 200,
                child: SingleChildScrollView(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: content))),
            actions: <Widget>[
              ElevatedButton(
                onPressed: () => Navigator.pop(context, false),
                style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all(Colors.red)),
                child: const Text("Annuler"),
              ),
              TextButton(
                  style: ButtonStyle(
                      backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                  onPressed: () => Navigator.pop(context, true),
                  child: Text(EquidoLocalizations.of(context).getText("ok"),
                      style: const TextStyle(color: Colors.white))),
            ],
          );
        });
  }

  Future showConfirmation(
      BuildContext context,
      String ownerId,
      String username,
      String userId,
      String horseId,
      ProfilePicture randomPictureSharedProfile) async {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);

    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(EquidoLocalizations.of(context)
                .getTextParameter("sharing_answer", username)),
            actions: <Widget>[
              TextButton(
                child: Text(
                  EquidoLocalizations.of(context).getText("cancel"),
                  style: const TextStyle(color: Colors.red),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text(EquidoLocalizations.of(context).getText("confirm")),
                onPressed: () {
                  DateTime today = DateTime.now();
                  Sharing sharing = Sharing(
                      horseId: horseId,
                      userSharedId: userId,
                      userId: ownerId,
                      userSharedName: username,
                      beginAt: today,
                      endedAt: DateTime(today.year + 1, today.month, today.day),
                      sharedDays: [],
                      sharedPictureProfile: randomPictureSharedProfile);

                  // Todo: bouger se code dans userService
                  SharingRepository.addSharingHorse(sharing)
                      .then((Result result) {
                    applicationBloc.getAllData();
                    Navigator.pop(context);
                  });
                },
              ),
            ],
          );
        });
  }
}
