import "package:collection/collection.dart";
import 'package:equido_flutter/src/models/user/sharing.dart';
import 'package:flutter_calendar_carousel/classes/event.dart'
    as flutter_calendar;
import 'package:flutter_calendar_carousel/classes/event_list.dart';

class SharingCalendarService {
  List<Sharing> sharings = [];
  List<DateTime> addedDays = [];
  List<DateTime> removedDays = [];
  List<DateTime> markedDate = [];

  late EventList<flutter_calendar.Event> markedDateMap;
  late EventList<flutter_calendar.Event> initMarkedDateMap;
  bool init = false;

  initService(List<Sharing> iniSsharings) {
    sharings = iniSsharings;
    markedDateMap = getEventFromMarkedDate();
    initMarkedDateMap = getEventFromMarkedDate();
  }

  clearServiceState(String userId) {
    // Get the initial marked date
    initMarkedDateMap = getEventFromMarkedDate();

    // Get the marked date, with the dates being modified
    markedDate = getMarkedDate(userId);

    // Get only the dates being modified ADDED
    addedDays.removeWhere((date) => initMarkedDateMap.events.containsKey(date));

    // the dates being modified REMOVED
    removedDays
        .where((date) => !initMarkedDateMap.events.containsKey(date))
        .toList()
        .forEach((e) => markedDateMap.removeAll(e));
    removedDays
        .removeWhere((date) => !initMarkedDateMap.events.containsKey(date));
  }

  EventList<flutter_calendar.Event> getEventFromMarkedDate() {
    List<flutter_calendar.Event> calendarEvents = [];
    for (var sharing in sharings) {
      calendarEvents.addAll(sharing.sharedDays
          .map((e) => flutter_calendar.Event(
                date: DateTime(e.year, e.month, e.day),
              ))
          .toList());
    }

    Map<DateTime, List<flutter_calendar.Event>> calendarEventsFromDate =
        groupBy(calendarEvents,
            (t) => DateTime(t.date.year, t.date.month, t.date.day));

    return EventList(events: calendarEventsFromDate);
  }

  List<DateTime> getMarkedDate(String userId) {
    List<DateTime> marked = [];
    for (var sharing in sharings) {
      if (sharing.userSharedId != userId) {
        marked.addAll(sharing.sharedDays
            .map(
              (e) => DateTime(e.year, e.month, e.day),
            )
            .toList());
      }
    }

    return marked;
  }
}
