import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/user/user.dart';
import 'package:equido_flutter/src/services/user_service.dart';
import 'package:equido_flutter/ui/horse/statisics/sharing/dialog/sharing_dialog.dart';
import 'package:flutter/material.dart';

class SharingAsync {
  const SharingAsync();

  Future<void> searchUser(BuildContext context,
      VoidCallback Function(List<EquiUser>) onSuccess) async {
    List<EquiUser> users = await UserService.searchUser();
    onSuccess.call(users);
  }

  Future<void> addSharingDays(
      BuildContext context,
      Horse horse,
      List<DateTime> days,
      String userSharedId,
      VoidCallback Function(bool) onSuccess) async {
    bool result = await const SharingDialog()
        .addSharingDays(context, horse, days, userSharedId);
    onSuccess.call(result);
  }

  Future<void> removeSharingDays(
      BuildContext context,
      Horse horse,
      List<DateTime> removedDays,
      String userSharedId,
      VoidCallback Function(bool) onSuccess) async {
    bool result = await const SharingDialog()
        .removeSharingDays(context, horse, removedDays, userSharedId);
    onSuccess.call(result);
  }
}
