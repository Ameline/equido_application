import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/bloc/user_bloc.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/user/sharing.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:equido_flutter/ui/horse/info/dialog/horse_details_dialogs.dart';
import 'package:equido_flutter/ui/horse/statisics/sharing/view/search_user_view.dart';
import 'package:equido_flutter/ui/horse/statisics/sharing/view/sharing_calendar.dart';
import 'package:equido_flutter/ui/horse/statisics/sharing/view/sharing_owner_calendar.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SharingView extends StatefulWidget {
  final String horseId;

  const SharingView(this.horseId, {super.key});

  @override
  SharingViewState createState() => SharingViewState();
}

class SharingViewState extends State<SharingView> {
  @override
  Widget build(BuildContext context) {
    final userBloc = BlocProvider.of<UserBloc>(context);
    final horsesBloc = BlocProvider.of<HorseBloc>(context);

    return StreamBuilder<List<Horse>>(
        stream: horsesBloc.getHorses,
        initialData: horsesBloc.horses,
        builder: (context, horseSnapshot) {
          Horse horse = horseSnapshot.data!
              .where((horse) => horse.id == widget.horseId)
              .first;

          if (userBloc.user.id != horse.ownerId) {
            return notYourHorse(horse, userBloc.user.id);
          }

          List<Sharing> sharingWithoutOwner = horse.sharing
              .where((s) => s.userSharedId != userBloc.user.id)
              .toList();

          return SingleChildScrollView(
              child: Container(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(height: 10),
                        _researchUser(),
                        const SizedBox(height: 40),
                        _numberOfSharing(context, sharingWithoutOwner),
                        const SizedBox(height: 5),
                        _userOfSharing(context, sharingWithoutOwner),
                        const SizedBox(height: 10),
                        const Text(
                          "*Les utilisateurs avec qui vous partagez votre cheval pourront ajouter des entraînements et des sorties au cheval. Ils auront également accès aux informations du cheval.",
                          style: TextStyle(color: Colors.grey),
                          textAlign: TextAlign.center,
                        ),
                        const SizedBox(height: 10),
                        SharingOwnerCalendar(userBloc.user.id, horse)
                      ])));
        });
  }

  Widget notYourHorse(Horse horse, String userId) {
    Sharing sharing =
        horse.sharing.where((s) => s.userSharedId == userId).first;

    return Align(
        alignment: Alignment.topCenter,
        child: Column(children: [
          _header(sharing, horse),
          _calendar(userId, horse.id, horse.sharing)
        ]));
  }

  Widget _calendar(String userId, String horseId, List<Sharing> sharing) {
    return SharingCalendar(userId, horseId, sharing);
  }

  Widget _header(Sharing sharing, Horse horse) {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
        padding: const EdgeInsets.all(10),
        height: 120,
        decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: EquidoWidget.shadow()),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const SizedBox(height: 10),
              Text("${horse.ownerName} te partage ${horse.name}",
                  style: const TextStyle(
                      color: Colors.cyan,
                      fontSize: 17.0,
                      fontWeight: FontWeight.w500)),
              const SizedBox(height: 10),
              Text(
                  "Depuis le ${EquidoDate.makeDateEventWithYear(sharing.beginAt).toLowerCase()}"),
              Text(
                  "Jusqu'au ${EquidoDate.makeDateEventWithYear(sharing.endedAt).toLowerCase()}"),
            ]));
  }

  Widget _researchUser() {
    return Container(
        decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: EquidoWidget.shadow()),
        child: SearchUser(widget.horseId));
  }

  Widget _numberOfSharing(BuildContext context, List<Sharing> sharings) {
    if (sharings.isEmpty) {
      return const SizedBox();
    } else {
      return Text(
          "Votre cheval est partagé avec ${sharings.length} utilisateurs :",
          style: const TextStyle(
              color: Colors.cyan, fontSize: 17.0, fontWeight: FontWeight.w500));
    }
  }

  Widget _userOfSharing(BuildContext context, List<Sharing> sharings) {
    if (sharings.isEmpty) {
      return const SizedBox(
        height: 100,
        child: Text("Votre cheval n'est partagé avec aucun autre utilisateur"),
      );
    } else {
      List<Widget> widgets = [];
      for (Sharing sharing in sharings) {
        widgets.add(userSharing(context, sharing));
        widgets.add(const SizedBox(height: 10));
      }
      return Column(children: widgets);
    }
  }

  Widget userSharing(BuildContext context, Sharing sharing) {
    return Container(
        height: 100,
        decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: EquidoWidget.shadow()),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const SizedBox(width: 10),
            const Padding(
                padding: EdgeInsets.only(top: 20),
                child: Icon(
                  Icons.account_circle,
                  size: 50,
                )),
            const SizedBox(width: 10),
            Expanded(
                flex: 3,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(sharing.userSharedName,
                          style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 18)),
                      const SizedBox(height: 10),
                      Text(
                          "Depuis ${EquidoDate.makeDateEventWithYear(sharing.beginAt).toLowerCase()}",
                          style: const TextStyle(color: Colors.grey)),
                      Text(
                          "Jusqu'au ${EquidoDate.makeDateEventWithYear(sharing.endedAt).toLowerCase()}",
                          style: const TextStyle(color: Colors.grey)),
                      // Todo: modification du partage (date fin)
                      // SizedBox(height: 10),
                      // EquidoWidget.gradientButtonBlueSoft(
                      //    context, "modifier", () {}),
                    ])),
            Expanded(
                flex: 1,
                child: Column(children: [
                  IconButton(
                      icon: const Icon(
                        Icons.clear,
                        color: Colors.red,
                      ),
                      onPressed: () =>
                          HorseDetailsDialogs.showConfirmationDeleteSharing(
                              context,
                              sharing.userSharedName,
                              sharing.userSharedId,
                              widget.horseId))
                ]))
          ],
        ));
  }
}
