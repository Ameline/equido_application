import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/bloc/user_bloc.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/user/sharing.dart';
import 'package:equido_flutter/ui/horse/statisics/sharing/dialog/sharing_dialog.dart';
import 'package:equido_flutter/ui/horse/statisics/sharing/service/sharing_calendar_service.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_calendar_carousel/classes/event.dart'
    as flutter_calendar;
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel;

class SharingCalendar extends StatefulWidget {
  final String userId;
  final String horseId;
  final List<Sharing> initSharing;

  const SharingCalendar(this.userId, this.horseId, this.initSharing,
      {super.key});

  @override
  SharingCalendarState createState() => SharingCalendarState();
}

class SharingCalendarState extends State<SharingCalendar> {
  final DateTime _currentDate = DateTime.now();

  // Todo why day + 2 ????
  DateTime _targetDateTime = DateTime.now().add(const Duration(days: 2));

  SharingCalendarService service = SharingCalendarService();

  @override
  void initState() {
    super.initState();
    service.initService(widget.initSharing);
  }

  @override
  Widget build(BuildContext context) {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);

    return StreamBuilder<List<Horse>>(
        stream: applicationBloc.getHorses,
        initialData: applicationBloc.horses,
        builder: (context, snapshotHorses) {
          Horse horse =
              snapshotHorses.data!.where((h) => h.id == widget.horseId).first;

          // Update the dates being modified (UDPATED & REMOVED) and the dates initially marked
          service.clearServiceState(widget.userId);

          return Container(
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20.0),
                boxShadow: EquidoWidget.shadow()),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const SizedBox(height: 10),
                _legend(horse),
                const SizedBox(height: 10),
                _calendarCarousel()
              ],
            ),
          );
        });
  }

  Widget _legend(Horse horse) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [_sharingLegend(horse), _unavailableLegend()],
        ),
        _modificationButton(horse)
      ],
    );
  }

  Widget _modificationButton(Horse horse) {
    if (service.addedDays.isEmpty && service.removedDays.isEmpty) {
      return const SizedBox();
    } else {
      final applicationBloc = BlocProvider.of<UserBloc>(context);

      return EquidoWidget.gradientButtonBlueSoft(context, "modifier les jours",
          () {
        const SharingDialog().showConfirmationUpdateSharingDays(context, horse,
            service.addedDays, service.addedDays, applicationBloc.user.id);
      });
    }
  }

  Widget _sharingLegend(Horse horse) {
    return Row(
      children: [
        const Icon(Icons.brightness_1_outlined, color: Colors.pinkAccent),
        Text("Tes jours avec ${horse.name}")
      ],
    );
  }

  Widget _unavailableLegend() {
    return Row(
      children: [
        Icon(Icons.brightness_1_outlined, color: Colors.grey.shade400),
        const Text("Cheval indisponible")
      ],
    );
  }

  _calendarCarousel() {
    return CalendarCarousel<flutter_calendar.Event>(
      todayBorderColor: Colors.cyan,
      onDayPressed: (date, events) => _onPressed(date, events),
      daysHaveCircularBorder: true,
      showOnlyCurrentMonthDate: false,
      weekendTextStyle: const TextStyle(
        color: Colors.black,
      ),
      thisMonthDayBorderColor: Colors.transparent,
      weekFormat: false,
      markedDatesMap: service.markedDateMap,
      height: 360.0,
      selectedDateTime: _currentDate,
      targetDateTime: _targetDateTime,
      customGridViewPhysics: const NeverScrollableScrollPhysics(),
      showHeader: false,
      todayTextStyle:
          const TextStyle(color: Colors.cyan, fontWeight: FontWeight.bold),
      markedDateShowIcon: true,
      markedDateIconMaxShown: 1,
      markedDateIconBuilder: (event) => eventContainer(event),
      todayButtonColor: Colors.white,
      selectedDayTextStyle:
          const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      selectedDayBorderColor: Colors.white,
      selectedDayButtonColor: Colors.cyan,
      minSelectedDate: _currentDate.subtract(const Duration(days: 360)),
      maxSelectedDate: _currentDate.add(const Duration(days: 360)),
      prevDaysTextStyle: const TextStyle(
        fontSize: 16,
        color: Colors.grey,
      ),
      inactiveDaysTextStyle: const TextStyle(
        color: Colors.tealAccent,
        fontSize: 16,
      ),
      onCalendarChanged: (DateTime date) {
        setState(() {
          _targetDateTime = date;
        });
      },
    );
  }

  _onPressed(DateTime date, List<flutter_calendar.Event> event) {
    List<DateTime> addedDates = service.addedDays;
    List<DateTime> removedDates = service.removedDays;

    if (service.markedDate.contains(date)) {
      // We cannot assign an unavailable day
      return;
    }

    // Is a removed Date
    if (service.initMarkedDateMap.getEvents(date).isNotEmpty) {
      // Delete the removal ?
      if (removedDates.contains(date)) {
        removedDates.remove(date);
        service.markedDateMap.add(
            date,
            flutter_calendar.Event(
                date: DateTime(date.year, date.month, date.day)));
      } else {
        removedDates.add(date);
      }
    } else {
      if (addedDates.contains(date)) {
        addedDates.remove(date);
        service.markedDateMap.remove(
            date,
            flutter_calendar.Event(
                date: DateTime(date.year, date.month, date.day)));
      } else {
        addedDates.add(date);
        service.markedDateMap.add(
            date,
            flutter_calendar.Event(
                date: DateTime(date.year, date.month, date.day)));
      }
    }

    service.addedDays = addedDates;
    service.removedDays = removedDates;
    setState(() {});
  }

  Widget eventContainer(flutter_calendar.Event event) {
    if (service.markedDate.contains(event.date)) {
      return Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.all(Radius.circular(1000.0))),
          child: Text(event.date.day.toString(),
              style: const TextStyle(
                  fontWeight: FontWeight.normal, color: Colors.grey)));
    }
    if (service.removedDays.contains(event.date)) {
      return Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: Colors.pinkAccent.shade100,
              borderRadius: const BorderRadius.all(Radius.circular(1000.0))),
          child: Text(event.date.day.toString(),
              style: const TextStyle(
                  fontWeight: FontWeight.normal, color: Colors.white)));
    } else if (service.addedDays.contains(event.date)) {
      return Container(
          alignment: Alignment.center,
          decoration: const BoxDecoration(
              color: Colors.pinkAccent,
              borderRadius: BorderRadius.all(Radius.circular(1000.0))),
          child: Text(event.date.day.toString(),
              style: const TextStyle(
                  fontWeight: FontWeight.normal, color: Colors.white)));
    } else {
      return Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.pinkAccent),
              borderRadius: const BorderRadius.all(Radius.circular(1000.0))),
          child: Text(event.date.day.toString(),
              style: const TextStyle(
                  fontWeight: FontWeight.normal, color: Colors.pinkAccent)));
    }
  }
}
