import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/bloc/user_bloc.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/user/user.dart';
import 'package:equido_flutter/ui/horse/statisics/sharing/dialog/sharing_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchUser extends StatefulWidget {
  final String horseId;

  const SearchUser(this.horseId, {super.key});

  @override
  SearchUserState createState() => SearchUserState();
}

class SearchUserState extends State<SearchUser> {
  final TextEditingController _textController = TextEditingController();
  List<EquiUser> allUsers = [];
  List<EquiUser> filteredList = [];

  @override
  void initState() {
    super.initState();
    //_getNames();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: _height(),
        child: Column(
          children: <Widget>[
            TextField(
                controller: _textController,
                onChanged: (text) {
                  text = text.toLowerCase();
                  setState(() {
                    filteredList = allUsers
                        .where((user) =>
                            user.username.toLowerCase().contains(text))
                        .toList();
                  });
                },
                decoration: const InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Rechercher un utilisateur',
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.grey,
                  ),
                )),
            if (notSearching())
              const SizedBox()
            else if (_notResult())
              const Expanded(
                child: Text('Aucun utilisateur trouvé'),
              )
            else
              Expanded(
                  child: ListView.builder(
                      itemCount: filteredList.length,
                      itemBuilder: (BuildContext context, index) {
                        final applicationBloc =
                            BlocProvider.of<UserBloc>(context);

                        return GestureDetector(
                            onTap: () {
                              if (_isAlreadyShared(filteredList[index].id)) {
                                const SharingDialog().showConflict(context);
                              } else {
                                const SharingDialog().showConfirmation(
                                    context,
                                    applicationBloc.user.id,
                                    filteredList[index].username,
                                    filteredList[index].id,
                                    widget.horseId,
                                    filteredList[index].pictureProfile);
                              }
                            },
                            child: Container(
                              padding: const EdgeInsets.only(left: 10),
                              height: 30,
                              child: Text(_getUsername(filteredList[index])),
                            ));
                      })),
          ],
        ));
  }

  String _getUsername(EquiUser user) {
    if (_isAlreadyShared(user.id)) {
      return "${user.username} (déjà dans tes partages)";
    } else {
      return user.username;
    }
  }

  bool _isAlreadyShared(String userString) {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);
    Horse horse =
        applicationBloc.horses.where((h) => h.id == widget.horseId).first;

    return horse.sharing
        .map((s) => s.userSharedId)
        .where((id) => id == userString)
        .isNotEmpty;
  }

  double _height() {
    if (notSearching()) {
      return 50;
    } else if (_notResult()) {
      return 100;
    }
    return 200;
  }

  bool notSearching() {
    return filteredList.isEmpty && _textController.text.isEmpty;
  }

  bool _notResult() {
    return filteredList.isEmpty && _textController.text.isNotEmpty;
  }
}
