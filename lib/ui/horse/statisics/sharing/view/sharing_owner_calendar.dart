import "package:collection/collection.dart";
import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/bloc/user_bloc.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/user/sharing.dart';
import 'package:equido_flutter/ui/horse/statisics/sharing/dialog/sharing_dialog.dart';
import 'package:equido_flutter/ui/horse/statisics/sharing/view/sharing_add.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_calendar_carousel/classes/event.dart'
    as flutter_calendar;
import 'package:flutter_calendar_carousel/classes/event_list.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel;
import 'package:uuid/uuid.dart';

class SharingOwnerCalendar extends StatefulWidget {
  String userId;
  Horse horse;

  SharingOwnerCalendar(this.userId, this.horse, {super.key});

  @override
  SharingOwnerCalendarState createState() => SharingOwnerCalendarState();
}

class SharingOwnerCalendarState extends State<SharingOwnerCalendar> {
  final DateTime _currentDate = DateTime.now();
  DateTime _targetDateTime = DateTime.now().add(const Duration(days: 2));
  String currentUserId = Uuid().v4();

  SharingAdd sharingAdd = SharingAdd();

  late EventList<flutter_calendar.Event> markedDateMap;
  late EventList<flutter_calendar.Event> initMarkedDateMap;
  bool init = false;

  @override
  void initState() {
    super.initState();
    markedDateMap = _getMapEventsFromEvent(widget.horse.sharing);
    initMarkedDateMap = _getMapEventsFromEvent(widget.horse.sharing);
    sharingAdd.init(widget.horse.sharing);
    currentUserId = widget.horse.sharing.isEmpty
        ? widget.horse.ownerId
        : widget.horse.sharing.first.userSharedId;
  }

  EventList<flutter_calendar.Event> _getMapEventsFromEvent(
      List<Sharing> sharings) {
    List<flutter_calendar.Event> calendarEvents = [];

    for (var sharing in sharings) {
      calendarEvents.addAll(sharing.sharedDays.map((e) {
        return flutter_calendar.Event(
          date: DateTime(e.year, e.month, e.day),
        );
      }).toList());
    }

    Map<DateTime, List<flutter_calendar.Event>> calendarEventsFromDate =
        groupBy(calendarEvents,
            (t) => DateTime(t.date.year, t.date.month, t.date.day));

    return EventList(events: calendarEventsFromDate);
  }

  @override
  Widget build(BuildContext context) {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);

    return StreamBuilder<List<Horse>>(
        stream: applicationBloc.getHorses,
        initialData: applicationBloc.horses,
        builder: (context, snapshotHorses) {
          Horse horse =
              snapshotHorses.data!.where((h) => h.id == widget.horse.id).first;

          // Reset the initMarkedDateMap
          initMarkedDateMap = _getMapEventsFromEvent(widget.horse.sharing);

          // Clean the marked date
          sharingAdd
              .removedAlreadyRemoved(initMarkedDateMap)
              .forEach((date) => markedDateMap.removeAll(date));

          // Suppress sharingAdd when user is not in the horse sharing (removed)
          sharingAdd.sharingAdd.removeWhere((key, value) => horse.sharing
              .where((s) => s.userSharedId == value.userId)
              .isEmpty);

          sharingAdd.reset(initMarkedDateMap);

          return Container(
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20.0),
                boxShadow: EquidoWidget.shadow()),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const SizedBox(height: 10),
                _legend(horse),
                const SizedBox(height: 10),
                _calendarCarousel(horse, markedDateMap)
              ],
            ),
          );
        });
  }

  Widget _legend(Horse horse) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _sharingLegend(),
        ),
        _modificationButton(horse)
      ],
    );
  }

  Widget _modificationButton(Horse horse) {
    if (!sharingAdd.isUpdateOrRemoved()) {
      return const SizedBox();
    } else {
      final applicationBloc = BlocProvider.of<UserBloc>(context);

      return EquidoWidget.gradientButtonBlueSoft(context, "modifier les jours",
          () {
        const SharingDialog().showConfirmationUpdateSharingDays_2(
            context, horse, sharingAdd, applicationBloc.user.id);
      });
    }
  }

  List<Widget> _sharingLegend() {
    List<Widget> widgets = [];

    sharingAdd.sharingAdd.forEach((key, value) {
      if (value.userId == currentUserId) {
        widgets.add(Row(
          children: [
            Icon(Icons.brightness_1, color: value.color),
            const SizedBox(width: 5),
            Text(_getUserName(value),
                style: const TextStyle(fontWeight: FontWeight.bold))
          ],
        ));
      } else {
        widgets.add(GestureDetector(
            onTap: () => setState(() {
                  currentUserId = value.userId;
                }),
            child: Row(
              children: [
                Icon(Icons.brightness_1_outlined, color: value.color),
                const SizedBox(width: 5),
                Text(_getUserName(value),
                    style: const TextStyle(color: Colors.black54))
              ],
            )));
      }
    });

    return widgets;
  }

  _getUserName(SharedDate sharing) {
    if (sharing.isOwner) {
      return "${sharing.userName} (Moi)";
    }
    return sharing.userName;
  }

  _calendarCarousel(
      Horse horse, EventList<flutter_calendar.Event> markedDateMap) {
    return CalendarCarousel<flutter_calendar.Event>(
      todayBorderColor: Colors.cyan,
      onDayPressed: (date, events) => _onPressed(date, events),
      daysHaveCircularBorder: true,
      showOnlyCurrentMonthDate: false,
      weekendTextStyle: const TextStyle(
        color: Colors.black,
      ),
      thisMonthDayBorderColor: Colors.transparent,
      weekFormat: false,
      markedDatesMap: markedDateMap,
      height: 360.0,
      selectedDateTime: _currentDate,
      targetDateTime: _targetDateTime,
      customGridViewPhysics: const NeverScrollableScrollPhysics(),
      showHeader: false,
      todayTextStyle:
          const TextStyle(color: Colors.cyan, fontWeight: FontWeight.bold),
      markedDateShowIcon: true,
      markedDateIconMaxShown: 1,
      markedDateIconBuilder: (event) => eventContainer(horse, event),
      todayButtonColor: Colors.white,
      selectedDayTextStyle:
          const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      selectedDayBorderColor: Colors.white,
      selectedDayButtonColor: Colors.grey,
      minSelectedDate: _currentDate.subtract(const Duration(days: 360)),
      maxSelectedDate: _currentDate.add(const Duration(days: 360)),
      prevDaysTextStyle: const TextStyle(
        fontSize: 16,
        color: Colors.grey,
      ),
      inactiveDaysTextStyle: const TextStyle(
        color: Colors.tealAccent,
        fontSize: 16,
      ),
      onCalendarChanged: (DateTime date) {
        setState(() {
          _targetDateTime = date;
        });
      },
    );
  }

  _onPressed(DateTime date, List<flutter_calendar.Event> event) {
    if (_dateExisting(date)) {
      if (sharingAdd.dateMarkedAsRemoved(date)) {
        sharingAdd.removeRemoved(date);
        markedDateMap.add(
            date,
            flutter_calendar.Event(
                date: DateTime(date.year, date.month, date.day)));
      } else {
        sharingAdd.addRemoved(date, sharingAdd.getUserId(date));
      }
    } else {
      if (sharingAdd.dateMarkedAsAdded(date)) {
        sharingAdd.removeAdded(date);
        markedDateMap.remove(
            date,
            flutter_calendar.Event(
                date: DateTime(date.year, date.month, date.day)));
      } else {
        sharingAdd.addAdded(date, currentUserId);
        markedDateMap.add(
            date,
            flutter_calendar.Event(
                date: DateTime(date.year, date.month, date.day)));
      }
    }

    setState(() {
      sharingAdd = sharingAdd;
    });
  }

  bool _dateExisting(DateTime date) {
    return initMarkedDateMap.getEvents(date).isNotEmpty;
  }

  Widget eventContainer(Horse horse, flutter_calendar.Event event) {
    String userId = sharingAdd.getUserId(event.date);

    if (sharingAdd.dateMarkedAsRemoved(event.date)) {
      return Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: sharingAdd.sharingAdd[userId]!.color.withOpacity(0.5),
              borderRadius: const BorderRadius.all(Radius.circular(1000.0))),
          child: Text(event.date.day.toString(),
              style: const TextStyle(
                  fontWeight: FontWeight.normal, color: Colors.white)));
    } else if (sharingAdd.dateMarkedAsAdded(event.date)) {
      return Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: sharingAdd.sharingAdd[userId]!.color),
              borderRadius: const BorderRadius.all(Radius.circular(1000.0))),
          child: Text(event.date.day.toString(),
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  color: sharingAdd.sharingAdd[userId]!.color)));
    } else {
      return Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: sharingAdd.sharingAdd[userId] == null
                  ? Colors.grey
                  : sharingAdd.sharingAdd[userId]!.color,
              borderRadius: const BorderRadius.all(Radius.circular(1000.0))),
          child: Text(event.date.day.toString(),
              style: const TextStyle(
                  fontWeight: FontWeight.normal, color: Colors.white)));
    }
  }
}
