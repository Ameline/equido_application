import 'package:equido_flutter/src/models/user/sharing.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart'
    as flutter_calendar;
import 'package:flutter_calendar_carousel/classes/event_list.dart';
import 'package:uuid/uuid.dart';

class SharingAdd {
  Map<String, SharedDate> sharingAdd = {};
  List<Color> colors = [
    Colors.deepOrange,
    Colors.lightGreen,
    Colors.deepPurpleAccent,
    Colors.amberAccent
  ];
  int _colorIndex = 0;

  init(List<Sharing> sharings) {
    for (var sharing in sharings) {
      Color color = _getColor(sharing);
      sharingAdd.putIfAbsent(
          sharing.userSharedId,
          () => SharedDate(
              sharing.sharedDays,
              sharing.userSharedId,
              sharing.userSharedName,
              color,
              sharing.userSharedId == sharing.userId));
    }
  }

  reset(EventList<flutter_calendar.Event> initMarkedDateMap) {
    sharingAdd.forEach((key, value) {
      // Remove the added date who are present in the initMarkedDate ( => already added)
      value.marked.addAll(value.added
          .where((date) => initMarkedDateMap.events.containsKey(date)));
      value.added
          .removeWhere((date) => initMarkedDateMap.events.containsKey(date));

      // Remove the removed date who are NOT present in the initMarkedDate ( => already removed)
      value.removed
          .removeWhere((date) => !initMarkedDateMap.events.containsKey(date));
    });
  }

  List<DateTime> removedAlreadyRemoved(
      EventList<flutter_calendar.Event> initMarkedDateMap) {
    List<DateTime> result = [];
    sharingAdd.forEach((key, value) {
      result.addAll(value.removed
          .where((date) => !initMarkedDateMap.events.containsKey(date)));
    });

    return result;
  }

  bool dateMarkedAsRemoved(DateTime date) {
    bool result = false;
    sharingAdd.forEach((key, value) {
      result = result || value.removed.contains(date);
    });
    return result;
  }

  removeRemoved(DateTime date) {
    sharingAdd.forEach((key, value) {
      value.removed.remove(date);
    });
  }

  addRemoved(DateTime date, String userId) {
    sharingAdd.update(userId, (value) {
      value.removed.add(date);
      return value;
    });
  }

  bool dateMarkedAsAdded(DateTime date) {
    bool result = false;
    sharingAdd.forEach((key, value) {
      result = result || value.added.contains(date);
    });
    return result;
  }

  removeAdded(DateTime date) {
    sharingAdd.forEach((key, value) {
      value.added.remove(date);
    });
  }

  addAdded(DateTime date, String userId) {
    sharingAdd.update(userId, (value) {
      value.added.add(date);
      return value;
    });
  }

  bool isUpdateOrRemoved() {
    return sharingAdd.values.where((e) => e.added.isNotEmpty).isNotEmpty ||
        sharingAdd.values.where((e) => e.removed.isNotEmpty).isNotEmpty;
  }

  String getUserId(DateTime date) {
    String result = Uuid().v4();
    DateTime dateYear = DateTime(date.year, date.month, date.day);
    sharingAdd.forEach((key, value) {
      if (value.marked.contains(dateYear) ||
          value.added.contains(dateYear) ||
          value.removed.contains(dateYear)) {
        result = key;
      }
    });
    return result;
  }

  _getColor(Sharing sharing) {
    if (sharing.userSharedId == sharing.userId) {
      return Colors.cyan;
    }

    Color color = colors[_colorIndex];
    _colorIndex = (_colorIndex + 1) % colors.length;
    return color;
  }
}

class SharedDate {
  List<DateTime> added = [];
  List<DateTime> removed = [];
  List<DateTime> marked;
  String userId;
  String userName;
  Color color;
  bool isOwner;

  SharedDate(this.marked, this.userId, this.userName, this.color, this.isOwner);
}
