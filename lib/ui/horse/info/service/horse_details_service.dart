import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/ui/horse/info/view/horse_details_update.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HorseDetailsService {
  Horse gotHorseFromHorseId(BuildContext context, String horseId) {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);
    return applicationBloc.getHorse(horseId);
  }

  void goToHorseUpdate(BuildContext context, Horse horse) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => HorseDetailsUpdate(horse)));
  }
}
