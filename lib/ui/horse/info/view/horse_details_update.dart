import 'dart:async';

import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/models/horse/breed.dart';
import 'package:equido_flutter/src/models/horse/color.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/horse/kind.dart';
import 'package:equido_flutter/ui/form/equido_form.dart';
import 'package:equido_flutter/ui/horse/info/dialog/horse_details_dialogs.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../../src/utils/equido_utils.dart';

class HorseDetailsUpdate extends StatefulWidget {
  final Horse horse;

  const HorseDetailsUpdate(this.horse, {super.key});

  @override
  State<StatefulWidget> createState() => _HorseDetailsUpdateState();
}

class _HorseDetailsUpdateState extends State<HorseDetailsUpdate> {
  final _formKey = GlobalKey();
  late Horse updated;
  DateTime? birthDate;

  _HorseDetailsUpdateState();

  @override
  void initState() {
    super.initState();

    updated = Horse.copy(widget.horse);
    birthDate = updated.birthDate;
  }

  Future _selectDate(BuildContext context) async {
    DateTime initDate;
    if (birthDate == null) {
      initDate = DateTime.now();
    } else {
      initDate = birthDate!;
    }

    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: initDate,
      firstDate: DateTime(1980),
      lastDate: DateTime(2030),
    );

    if (picked != null && picked != birthDate) {
      setState(() {
        birthDate = picked;
      });
    }
  }

  Row makeDateForm(String text) {
    return Row(
      children: <Widget>[
        EquidoWidget.makeSubTitle(text),
        Expanded(
            flex: 2,
            child: ElevatedButton(
              style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.all(Colors.white),
                  elevation: WidgetStateProperty.all(0),
                  shape: WidgetStateProperty.all(RoundedRectangleBorder(
                      side: BorderSide(color: Colors.grey[300]!)))),
              onPressed: () => _selectDate(context),
              child: birthDate == null
                  ? updated.birthDate == null
                      ? const Text("Inconnu")
                      : Text(updated.getBirthDateFormatted())
                  : Text(DateFormat("dd-MM-yyyy").format(birthDate!)),
            ))
      ],
    );
  }

  bool isValidBirthDate(Horse horse) {
    if (horse.birthDate != null) {
      return horse.birthDate!.isBefore(DateTime.now());
    }
    return true;
  }

  bool isValidSize(Horse horse) {
    int size = horse.size ?? 0;
    return size < 250;
  }

  String _commonCarePlaceholder(String? value) {
    if (value == null || value == EquidoUtils.nR) {
      return "Ex: 10 ( en jours )";
    }
    return "$value jours";
  }

  @override
  build(BuildContext context) {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);

    Breed.breeds.sort((a, b) => a.compareTo(b));
    HorseColor.colors.sort((a, b) => a.compareTo(b));

    final topAppBar = AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          updated.name,
          style: const TextStyle(color: Colors.cyan),
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.cyan,
          ),
          onPressed: () => Navigator.pop(context, false),
        ));

    final body = SingleChildScrollView(
        padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        child: Builder(
            builder: (context) => Form(
                key: _formKey,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      EquidoWidget.titleBorderBottum(
                          "Modification des informations"),
                      EquidoForm.makeDropDown("Genre", updated.kind, Kind.kinds,
                          (value) => setState(() => updated.kind = value!)),
                      EquidoForm.makeTextFielForm(
                          "Taille (cm)",
                          updated.size.toString(),
                          (value) =>
                              setState(() => updated.size = int.parse(value)),
                          textInputType: TextInputType.number),
                      EquidoForm.makeDropDown(
                          "Robe",
                          updated.color,
                          HorseColor.colors,
                          (value) => setState(() => updated.color = value!)),
                      EquidoForm.makeDropDown(
                          "Race",
                          updated.breed,
                          Breed.breeds,
                          (value) => setState(() => updated.breed = value!)),
                      makeDateForm("Date de naissance"),
                      EquidoForm.makeTextFielForm(
                          "Fréquence ferrure",
                          _commonCarePlaceholder(updated
                              .getDeadlineFormatted(updated.shoeing.frequency)),
                          (value) => setState(() =>
                              updated.shoeing.frequency = int.parse(value)),
                          textInputType: TextInputType.number),
                      EquidoForm.makeTextFielForm(
                          "Fréquence fermifuge",
                          _commonCarePlaceholder(updated
                              .getDeadlineFormatted(updated.wormer.frequency)),
                          (value) => setState(() =>
                              updated.wormer.frequency = int.parse(value)),
                          textInputType: TextInputType.number),
                      EquidoForm.makeTextFielForm(
                          "Fréquence vaccin",
                          _commonCarePlaceholder(updated
                              .getDeadlineFormatted(updated.vaccine.frequency)),
                          (value) => setState(() =>
                              updated.vaccine.frequency = int.parse(value)),
                          textInputType: TextInputType.number),
                      const SizedBox(height: 45.0),
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor: WidgetStateProperty.all(
                                        Colors.red[300])),
                                child: const Text("Annuler",
                                    style: TextStyle(color: Colors.white)),
                                onPressed: () async {
                                  Navigator.pop(context, false);
                                }),
                          ),
                          const SizedBox(width: 10.0),
                          Expanded(
                            flex: 2,
                            child: ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor: WidgetStateProperty.all(
                                        Theme.of(context).primaryColor)),
                                child: const Text("OK",
                                    style: TextStyle(color: Colors.white)),
                                onPressed: () async {
                                  updated.birthDate = birthDate;
                                  if (!isValidBirthDate(updated)) {
                                    await HorseDetailsDialogs
                                        .showErrorBirthdate(context);
                                  } else if (!isValidSize(updated)) {
                                    await HorseDetailsDialogs.showErrorSize(
                                        context);
                                  } else {
                                    HorseBlocEvent<Horse> blocHorse =
                                        HorseBlocEvent<Horse>(
                                            eventType:
                                                HorseBlocType.updateHorse,
                                            object: updated);
                                    applicationBloc.actionEventsController
                                        .add(blocHorse);
                                    Navigator.pop(context, false);
                                    //60Navigator.pop(context, false);
                                  }
                                }),
                          ),
                        ],
                      )
                    ]))));

    return Scaffold(
        backgroundColor: Colors.white, appBar: topAppBar, body: body);
  }
}
