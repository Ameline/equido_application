import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/ui/horse/info/dialog/horse_details_dialogs.dart';
import 'package:equido_flutter/ui/horse/info/service/horse_details_service.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../src/utils/equido_utils.dart';

class HorseDetails extends StatefulWidget {
  final String horseId;

  const HorseDetails(this.horseId, {super.key});

  @override
  State<StatefulWidget> createState() => _HorseDetailsState();
}

class _HorseDetailsState extends State<HorseDetails> {
  late Horse _horse;
  final HorseDetailsService horseDetailsService = HorseDetailsService();

  @override
  Widget build(BuildContext context) {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);
    _horse = applicationBloc.getHorse(widget.horseId);

    final topAppBar = AppBar(
      backgroundColor: Colors.white,
      elevation: 0.0,
      title: Text(
        _horse.name,
        style: const TextStyle(color: Colors.cyan),
      ),
      leading: IconButton(
        icon: const Icon(
          Icons.arrow_back,
          color: Colors.cyan,
        ),
        onPressed: () => Navigator.pop(context, false),
      ),
    );

    return Scaffold(
        appBar: topAppBar,
        body: SingleChildScrollView(
            padding:
                const EdgeInsets.symmetric(vertical: 16.0, horizontal: 10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _container(
                    EquidoLocalizations.of(context)
                        .getText("horse_information"),
                    Column(children: _detailsHorses(context))),
                const SizedBox(height: 20.0),
                _container(
                    EquidoLocalizations.of(context)
                        .getText("horse_care_frequency"),
                    Column(children: _commonCaresDetailsHorses(context))),
                const SizedBox(height: 20.0),
                _container(
                    EquidoLocalizations.of(context).getText("horse_supress"),
                    Column(children: _suppressHorse(context)))
              ],
            )));
  }

  Widget _container(String title, Widget widget) {
    return Container(
        decoration: _decoration(),
        padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        //color: Colors.white,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              EquidoWidget.titleContainer(title),
              const SizedBox(height: 5.0),
              widget
            ]));
  }

  BoxDecoration _decoration() {
    return BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: EquidoWidget.shadow());
  }

  List<Widget> _detailsHorses(BuildContext context) {
    return [
      EquidoWidget.makeTextInformation(
          EquidoLocalizations.of(context).getText("name"), _horse.name),
      const SizedBox(height: 5.0),
      EquidoWidget.makeTextInformation(
          EquidoLocalizations.of(context).getText("gender"), _horse.kind),
      const SizedBox(height: 5.0),
      EquidoWidget.makeTextInformation(
          EquidoLocalizations.of(context).getText("horse_color"), _horse.color),
      const SizedBox(height: 5.0),
      EquidoWidget.makeTextInformation(
          EquidoLocalizations.of(context).getText("horse_size"),
          _horse.size.toString()),
      const SizedBox(height: 5.0),
      EquidoWidget.makeTextInformation(
          EquidoLocalizations.of(context).getText("horse_breed"), _horse.breed),
      const SizedBox(height: 5.0),
      EquidoWidget.makeTextInformation(
          EquidoLocalizations.of(context).getText("birthdate"),
          _horse.birthDate == null
              ? _horse.getBirthDateFormatted()
              : "${_horse.getBirthDateFormatted()} (${_horse.getYearsOldFormatted()} ans)"),
      const SizedBox(height: 5.0),
      EquidoWidget.makeTextInformation(
          EquidoLocalizations.of(context).getText("proprietary"),
          _horse.ownerName),
      const SizedBox(height: 15.0),
      EquidoWidget.gradientButtonBlue(
          context,
          EquidoLocalizations.of(context).getText("modification_information"),
          () => horseDetailsService.goToHorseUpdate(context,
              horseDetailsService.gotHorseFromHorseId(context, widget.horseId)))
    ];
  }

  List<Widget> _commonCaresDetailsHorses(BuildContext context) {
    return [
      EquidoWidget.makeTextInformation(
          EquidoLocalizations.of(context).getText("shoeingDeadline"),
          _makeDay(_horse.getDeadlineFormatted(_horse.shoeing.frequency))),
      const SizedBox(height: 5.0),
      EquidoWidget.makeTextInformation(
          EquidoLocalizations.of(context).getText("wormerDeadline"),
          _makeDay(_horse.getDeadlineFormatted(_horse.wormer.frequency))),
      const SizedBox(height: 5.0),
      EquidoWidget.makeTextInformation(
          EquidoLocalizations.of(context).getText("vaccineDeadline"),
          _makeDay(_horse.getDeadlineFormatted(_horse.vaccine.frequency))),
      const SizedBox(height: 15.0),
      Text(EquidoLocalizations.of(context).getText("care_frequency_subtitle"),
          style: const TextStyle(color: Colors.grey)),
      const SizedBox(height: 15.0),
      EquidoWidget.gradientButtonBlue(
          context,
          EquidoLocalizations.of(context)
              .getText("modification_care_frequency"),
          () => horseDetailsService.goToHorseUpdate(context,
              horseDetailsService.gotHorseFromHorseId(context, widget.horseId)))
    ];
  }

  String _makeDay(String value) {
    if (value != EquidoUtils.nR) {
      return '$value jours';
    }
    return value;
  }

  List<Widget> _suppressHorse(BuildContext context) {
    return [
      Text(EquidoLocalizations.of(context).getText("horse_supress_subtitle"),
          style: const TextStyle(color: Colors.grey)),
      const SizedBox(height: 4.0),
      EquidoWidget.gradientButtonRed(context,
          EquidoLocalizations.of(context).getText("horse_supress_action"), () {
        HorseDetailsDialogs.showConfirmation(context, _horse);
      }),
    ];
  }
}
