import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/repository/sharing_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HorseDetailsDialogs {
  static Future showOK(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content:
                Text(EquidoLocalizations.of(context).getText("horse_deleted")),
            actions: <Widget>[
              TextButton(
                  style: ButtonStyle(
                      backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                  onPressed: () {
                    Navigator.popUntil(context, ModalRoute.withName('/'));
                  },
                  child: Text(EquidoLocalizations.of(context).getText("ok"),
                      style: const TextStyle(color: Colors.white))),
            ],
          );
        });
    //.then((var v) {
    // Navigator.popUntil(context, ModalRoute.withName('/'));
    // });
  }

  static Future showOKDeleteSharing(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(
                EquidoLocalizations.of(context).getText("sharing_deleted")),
            actions: <Widget>[
              TextButton(
                  style: ButtonStyle(
                      backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(EquidoLocalizations.of(context).getText("ok"),
                      style: const TextStyle(color: Colors.white))),
            ],
          );
        });
  }

  static Future showConfirmation(BuildContext context, Horse horse) async {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);

    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(EquidoLocalizations.of(context)
                .getTextParameter("horse_delete_answer", horse.name)),
            content: Text(EquidoLocalizations.of(context)
                .getText("horse_delete_subtitle")),
            actions: <Widget>[
              TextButton(
                child: Text(
                  EquidoLocalizations.of(context).getText("cancel"),
                  style: const TextStyle(color: Colors.red),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text(EquidoLocalizations.of(context).getText("confirm")),
                onPressed: () {
                  HorseBlocEvent<Horse> blocHorse = HorseBlocEvent<Horse>(
                      eventType: HorseBlocType.deleteHorse, object: horse);
                  applicationBloc.actionEventsController.add(blocHorse);
                  showOK(context);
                },
              ),
            ],
          );
        });
  }

  static Future showConfirmationDeleteSharing(BuildContext context,
      String userName, String userId, String horseId) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(EquidoLocalizations.of(context)
                .getTextParameter("sharing_delete_answer", userName)),
            actions: <Widget>[
              TextButton(
                child: Text(
                  EquidoLocalizations.of(context).getText("cancel"),
                  style: const TextStyle(color: Colors.red),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text(EquidoLocalizations.of(context).getText("confirm")),
                onPressed: () {
                  final applicationBloc = BlocProvider.of<HorseBloc>(context);
                  SharingRepository.deleteSharingHorse(horseId, userId);

                  applicationBloc.getAllData();
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  static Future showErrorSize(BuildContext context) async {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);

    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content:
                Text(EquidoLocalizations.of(context).getText("size_not_valid")),
            actions: <Widget>[
              TextButton(
                style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                onPressed: () {
                  applicationBloc.getAllData();
                  Navigator.pop(context);
                },
                child: Text(EquidoLocalizations.of(context).getText("ok")),
              ),
            ],
          );
        });
  }

  static Future showErrorBirthdate(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(
                EquidoLocalizations.of(context).getText("birthdate_not_valid")),
            actions: <Widget>[
              TextButton(
                style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(EquidoLocalizations.of(context).getText("ok")),
              ),
            ],
          );
        });
  }
}
