import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';

class EquidoForm {
  ///*********** UPDATE ***********///
  static Widget namedInformationOnPressed(
      String text, String value, void Function() onPressed) {
    return Container(
        padding: const EdgeInsets.only(top: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Text(
                  text,
                  textAlign: TextAlign.start,
                  style: const TextStyle(color: Colors.grey, fontSize: 20),
                )),
            Expanded(
              flex: 1,
              child: GestureDetector(
                onTap: onPressed,
                child: Text(
                  value,
                  textAlign: TextAlign.end,
                  style: const TextStyle(fontSize: 20, color: Colors.black54),
                ),
              ),
            ),
            const SizedBox(width: 5),
            const Icon(
              Icons.create,
              color: Colors.black54,
              size: 15,
            )
          ],
        ));
  }

  static Container makeDropDown(String text, String value, List<String> list,
      void Function(String?)? func) {
    return Container(
      padding: const EdgeInsets.only(top: 15),
      child: Row(
        children: <Widget>[
          EquidoWidget.makeSubTitle(text),
          Expanded(
              flex: 2,
              child: DropdownButton<String>(
                items: list.map((String value) {
                  return DropdownMenuItem<String>(
                    alignment: Alignment.center,
                    value: value,
                    child: SizedBox(
                      width: 200.0,
                      child: Text(value, textAlign: TextAlign.left),
                    ),
                  );
                }).toList(),
                value: value,
                onChanged: func,
              ))
        ],
      ),
    );
  }

  static Container makeTextFielForm(
      String text, String controller, void Function(String) func,
      {TextInputType textInputType = TextInputType.text}) {
    return Container(
      padding: const EdgeInsets.only(top: 15),
      child: Row(
        children: <Widget>[
          EquidoWidget.makeSubTitle(text),
          Expanded(
              flex: 2,
              child: TextField(
                  onChanged: func,
                  keyboardType: textInputType,
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      hintText: controller,
                      contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade200)),
                      focusedBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade200)))))
        ],
      ),
    );
  }

  static Container makeTextFielForm2(
      String text, String controller, void Function(String) func,
      {TextInputType textInputType = TextInputType.text}) {
    return Container(
      padding: const EdgeInsets.only(top: 15),
      child: Row(
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Text(
                text,
                textAlign: TextAlign.start,
                style: const TextStyle(color: Colors.grey, fontSize: 20),
              )),
          Expanded(
              flex: 2,
              child: TextField(
                  textAlign: TextAlign.end,
                  onChanged: func,
                  keyboardType: textInputType,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    hintText: controller,
                    contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    border: InputBorder.none,
                  )))
        ],
      ),
    );
  }
}
