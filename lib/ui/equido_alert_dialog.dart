import 'package:flutter/material.dart';

class EquidoAlertDialog {
  static Future onLoading(BuildContext context, {String? text}) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const CircularProgressIndicator(),
              const SizedBox(width: 30),
              Text(text ?? "Loading"),
            ],
          ),
        );
      },
    );
  }
}
