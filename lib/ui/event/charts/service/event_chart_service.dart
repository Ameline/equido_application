import 'dart:math';

import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/ui/event/charts/service/event_color_service.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../src/models/event/training.dart';
import '../../../../src/utils/equido_date.dart';

// Todo comment all this class
class EventChartService {
  List<Event> events;

  EventChartService(List<Event> events)
      : events =
            events.where((e) => e.eventType == EventType.training).toList();

  bool noTraining() {
    return events.isEmpty;
  }

  Training lastTraining() {
    events.sort((a, b) => a.start.compareTo(b.start));
    return events.first as Training;
  }

  Event getEvent(int index) {
    DateTime today = DateTime.now();
    DateTime currentDay = DateTime(today.year, today.month, today.day + index);

    return events
        .where((e) => EquidoDate.isSameDate(e.start, currentDay))
        .first;
  }

  String abbreviationDay(int index) {
    if (index == 0) return "Auj.";
    DateTime today = DateTime.now();
    DateTime currentDay = DateTime(today.year, today.month, today.day + index);

    return EquidoDate.translateWeekday(currentDay.weekday.toString());
  }

  String abbreviationDayWithDayNumber(int index) {
    if (index == 0) return "Auj.";
    DateTime today = DateTime.now();
    DateTime currentDay = DateTime(today.year, today.month, today.day + index);

    return "${EquidoDate.weekdayExpanded(currentDay.weekday.toString())} ${currentDay.day}";
  }

  String trainingType(int index, {bool isBefore = true}) {
    List<Event> filterEvent = _eventOfIndexDay(index, isBefore: isBefore);

    if (filterEvent.isEmpty) return "";
    return filterEvent.first.title;
  }

  String numberOfTheDay(int index) {
    DateTime today = DateTime.now();
    DateTime currentDay = DateTime(today.year, today.month, today.day - index);

    return EquidoDate.translateDate(DateFormat('EEEE').format(currentDay));
  }

  String makeExpandedHeaderTime(Event event) {
    return EquidoDate.translateDate(
        DateFormat('EEEE dd MMMM').format(event.start));
  }

  int trainingTime(int index) {
    if (events.isEmpty) {
      var rng = Random();

      return rng.nextInt(120);
    }

    List<Event> filterEvent = _eventOfIndexDay(index);

    if (filterEvent.isEmpty) return 0;

    int result = filterEvent.map((e) {
      Training training = e as Training;
      return training.stop
          .difference(training.start)
          .inMinutes; // Todo avec les extensions de methode on pourrais faire une methode pour la durée d'un event
    }).reduce((value, element) => value + element);

    return result;
  }

  Color principalColorTraining(int index) {
    List<Event> filterEvent = _eventOfIndexDay(index);

    if (filterEvent.isEmpty) return EventColor.other().primaryColor;

    EventColor eventColor =
        EventColorService.getEventColor(filterEvent.first.title);
    return eventColor.primaryColor;
  }

  Color shadeColorTraining(int index) {
    List<Event> filterEvent = _eventOfIndexDay(index, isBefore: false);

    if (filterEvent.isEmpty) return EventColor.other().shadeColor;

    EventColor eventColor =
        EventColorService.getEventColor(filterEvent.first.title);
    return eventColor.shadeColor;
  }

  Gradient gradientColorTraining(int index) {
    List<Event> filterEvent = _eventOfIndexDay(index, isBefore: false);

    if (filterEvent.isEmpty) return EventColor.other().gradient;

    EventColor eventColor =
        EventColorService.getEventColor(filterEvent.first.title);
    return eventColor.gradient;
  }

  List<Event> _eventOfIndexDay(int index, {bool isBefore = true}) {
    DateTime today = DateTime.now();
    DateTime currentDay = DateTime(today.year, today.month, today.day + index);
    if (isBefore) {
      currentDay = DateTime(today.year, today.month, today.day - index);
    }

    return events
        .where((event) => event.eventType == EventType.training)
        .where((event) => event.start.isSameDate(
            currentDay)) // Todo utiliser isSameDate partout pou la comparaison de date
        .toList();
  }

  bool trainingThisDay(int index) {
    DateTime today = DateTime.now();
    DateTime currentDay = DateTime(today.year, today.month, today.day + index);

    return events
        .where((e) => EquidoDate.isSameDate(e.start, currentDay))
        .isNotEmpty;
  }
}
