import 'package:flutter/material.dart';

class EventColorService {
  static EventColor getEventColor(String trainingType) {
    switch (trainingType) {
      case "Obstacle":
        return EventColor.jumping();
      case "Dressage":
        return EventColor.dressage();
      case "Travail à pied":
        return EventColor.liberty();
      case "Travail en exterieur":
        return EventColor.outside();
      default:
        return EventColor.other();
    }
  }
}

class EventColor {
  Color primaryColor;
  Color shadeColor;
  Gradient gradient;

  EventColor(this.primaryColor, this.shadeColor, this.gradient);

  EventColor.dressage()
      : primaryColor = const Color.fromRGBO(251, 57, 139, 1),
        shadeColor = Colors.pink.shade50,
        gradient = const LinearGradient(
          colors: [Color.fromRGBO(251, 57, 139, 1), Color.fromRGBO(255, 76, 100, 1)],
          begin: FractionalOffset(0.5, 0.0),
          end: FractionalOffset(0.0, 0.5),
        );

  EventColor.jumping()
      : primaryColor = const Color.fromRGBO(45, 180, 202, 1),
        shadeColor = Colors.blue.shade50,
        gradient = const LinearGradient(
          colors: [Color.fromRGBO(45, 180, 202, 1), Color.fromRGBO(85, 220, 242, 1)],
          begin: FractionalOffset(0.5, 0.0),
          end: FractionalOffset(0.0, 0.5),
        );

  EventColor.liberty()
      : primaryColor = Colors.orangeAccent,
        shadeColor = Colors.orange.shade50,
        gradient = const LinearGradient(
          colors: [Color.fromRGBO(248, 180, 68, 1), Color.fromRGBO(248, 218, 68, 1)],
          begin: FractionalOffset(0.5, 0.0),
          end: FractionalOffset(0.0, 0.5),
        );

  EventColor.outside()
      : primaryColor = Colors.lightGreen,
        shadeColor = Colors.lightGreen.shade50,
        gradient = const LinearGradient(
          colors: [Color.fromRGBO(24, 198, 111, 1), Color.fromRGBO(118, 216, 139, 1)],
          begin: FractionalOffset(0.5, 0.0),
          end: FractionalOffset(0.0, 0.5),
        );

  EventColor.other()
      : primaryColor = const Color.fromRGBO(219, 220, 220, 1),
        shadeColor = Colors.grey.shade50,
        gradient = const LinearGradient(
          colors: [Color.fromRGBO(251, 57, 139, 1), Color.fromRGBO(255, 76, 100, 1)],
          begin: FractionalOffset(0.5, 0.0),
          end: FractionalOffset(0.0, 0.5),
        );
}
