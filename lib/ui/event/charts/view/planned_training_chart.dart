import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/event/training.dart';
import 'package:equido_flutter/src/models/horse/logo.dart';
import 'package:equido_flutter/ui/event/charts/service/event_chart_service.dart';
import 'package:equido_flutter/ui/event/creation/view/training_selection.dart';
import 'package:equido_flutter/ui/event/info/view/event_info.dart';
import 'package:equido_flutter/ui/user/services/horses_statistics_service.dart';
import 'package:flutter/material.dart';

class PlannedTrainingChart extends StatefulWidget {
  final List<Event> events;
  final Map<DateTime, SharedUserInfo> userByDate;

  const PlannedTrainingChart(this.events, this.userByDate, {super.key});

  @override
  State<StatefulWidget> createState() => PlannedTrainingChartState();
}

class PlannedTrainingChartState extends State<PlannedTrainingChart> {
  EventChartService service = EventChartService([]);

  PlannedTrainingChartState();

  @override
  void initState() {
    super.initState();
    service = EventChartService(widget.events);
  }

  @override
  Widget build(BuildContext context) {
    if (service.events.length != widget.events.length) {
      setState(() {
        service = EventChartService(widget.events);
      });
    }

    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          const Text(
            'Entrainements planifiés',
            style: TextStyle(color: Colors.blueGrey, fontSize: 20, fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 4),
          const Text(
            'des 7 prochains jours',
            style: TextStyle(color: Colors.cyan, fontSize: 15),
          ),
          const SizedBox(height: 20),
          Row(children: List.generate(7, (index) => _container(index)))
        ]);
  }

  _container(int index) {
    return Expanded(
        flex: 1,
        child: GestureDetector(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 1),
              padding: const EdgeInsets.symmetric(vertical: 10),
              decoration: BoxDecoration(
                  color: service.shadeColorTraining(index), borderRadius: const BorderRadius.all(Radius.circular(10))),
              height: 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  trainingLogo(index),
                  sharingLogo(index),
                  Text(service.abbreviationDayWithDayNumber(index), style: const TextStyle(fontSize: 10)),
                ],
              ),
            ),
            onTap: () {
              if (service.trainingThisDay(index)) {
                Navigator.push(context, MaterialPageRoute(builder: (context) => EventInfo(service.getEvent(index))));
              } else {
                Navigator.push(context, MaterialPageRoute(builder: (context) => TrainingSelection(currentdate(index))));
              }
            }));
  }

  trainingLogo(int _) {
    String trainingType = service.trainingType(_, isBefore: false);

    if (trainingType.isEmpty) return addTrainingLogo();

    return Logo.trainingLogo(TrainingType.getAssetFromType(service.trainingType(_, isBefore: false)),
        size: LogoSize.min, gradient: service.gradientColorTraining(_));
  }

  currentdate(int index) {
    DateTime today = DateTime.now();
    return DateTime(today.year, today.month, today.day + index);
  }

  sharingLogo(int index) {
    DateTime currentDay = currentdate(index);
    // Todo mettre userByDate dans le service
    // Todo userByDate ne peut contenir que la photo de profile => profilePictureByDate
    // Todo profilePicture pictureProfile ? Il faut choisir !
    if (widget.userByDate.containsKey(currentDay)) {
      return Column(
        children: [widget.userByDate[currentDay]!.randomPictureProfile.widget(25)],
      );
    }
    return const SizedBox();
  }

  addTrainingLogo() {
    return const Icon(
      Icons.add_circle,
      color: Color.fromRGBO(219, 220, 220, 1),
      size: 35,
    );
  }
}
