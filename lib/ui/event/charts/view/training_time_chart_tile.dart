import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/ui/event/charts/service/event_chart_service.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import '../../../../src/utils/equido_date.dart';

class TrainingTimeChartTile extends StatefulWidget {
  final List<Event> events;

  const TrainingTimeChartTile(this.events, {super.key});

  @override
  State<StatefulWidget> createState() => TrainingTimeChartTileState();
}

class TrainingTimeChartTileState extends State<TrainingTimeChartTile> {
  EventChartService service = EventChartService([]);
  static const int numberOfDay = 14;
  int touchedIndex = -1;

  TrainingTimeChartTileState();

  @override
  void initState() {
    super.initState();
    service = EventChartService(widget.events);
  }

  @override
  Widget build(BuildContext context) {
    // Update service events if it needed
    if (service.events.length != widget.events.length) {
      service = EventChartService(widget.events);
    }

    return Container(
        height: 134,
        margin: const EdgeInsets.only(bottom: 2),
        color: Colors.white,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              const Text(
                "Dernier entrainement",
                style: TextStyle(
                    color: Colors.blueGrey,
                    fontSize: 12,
                    fontWeight: FontWeight.bold),
              ),
              lastTraining(),
              Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          displayType(),
                          displayDuration(),
                          //StoryListTile.displayHorseName(training)
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                      flex: 1,
                      child: SizedBox(
                        height: 100,
                        child: trainingChart(),
                      ))
                ],
              ),
            ]));
  }

  Widget lastTraining() {
    if (service.noTraining()) {
      return const Text(
        "Pas d'entrainement",
        style: TextStyle(color: Colors.cyan, fontSize: 12),
      );
    }
    return Text(
      service.makeExpandedHeaderTime(service.lastTraining()),
      style: const TextStyle(color: Colors.cyan, fontSize: 12),
    );
  }

  Widget displayType() {
    if (service.noTraining()) {
      return const SizedBox();
    }
    return Text(
      service.lastTraining().title,
      style: const TextStyle(fontSize: 20, color: Colors.blueGrey),
    );
  }

  Widget displayDuration() {
    if (service.noTraining()) {
      return const Text("Ajoute un entrainement",
          style: TextStyle(fontSize: 20, color: Colors.grey));
    }
    return Text(EquidoDate.printDuration(service.lastTraining().getDuration()),
        style: const TextStyle(fontSize: 20, color: Colors.grey));
  }

  Widget trainingChart() {
    return AspectRatio(
      aspectRatio: 1,
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              const SizedBox(
                height: 10,
              ),
              Expanded(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8.0, vertical: 10),
                  child: BarChart(
                    mainBarData(),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ],
      ),
    );
  }

  BarChartGroupData makeGroupData(
    int x,
    double y, {
    Color barColor = Colors.white,
    double width = 5,
    List<int> showTooltips = const [],
  }) {
    return BarChartGroupData(
      x: x,
      barRods: [
        BarChartRodData(
          toY: y,
          color: service.principalColorTraining(x),
          width: width,
          borderSide:
              BorderSide(color: service.principalColorTraining(x), width: 0),
          backDrawRodData: BackgroundBarChartRodData(
            show: true,
            toY: 2,
            color: const Color.fromRGBO(219, 220, 220, 1),
          ),
        ),
      ],
      showingTooltipIndicators: showTooltips,
    );
  }

  List<BarChartGroupData> showingGroups() => List.generate(numberOfDay, (i) {
        int index = numberOfDay - (i + 1);
        return makeGroupData(index, service.trainingTime(index).toDouble());
      });

  BarChartData mainBarData() {
    return BarChartData(
      barTouchData: BarTouchData(
        touchTooltipData: BarTouchTooltipData(
            tooltipBgColor: Colors.blueGrey,
            getTooltipItem: (group, groupIndex, rod, rodIndex) {
              String weekDay = service.numberOfTheDay(group.x.toInt());
              return BarTooltipItem(
                '$weekDay\n',
                const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
                children: <TextSpan>[
                  TextSpan(
                    text:
                        '${service.trainingType(group.x.toInt())} ${rod.toY.toInt() - 1} m',
                    style: TextStyle(
                      color: service.principalColorTraining(group.x.toInt()),
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              );
            }),
        touchCallback: (FlTouchEvent event, barTouchResponse) {
          setState(() {
            if (!event.isInterestedForInteractions ||
                barTouchResponse == null ||
                barTouchResponse.spot == null) {
              touchedIndex = -1;
              return;
            }
            touchedIndex = barTouchResponse.spot!.touchedBarGroupIndex;
          });
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        rightTitles: const AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        topTitles: const AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        bottomTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            getTitlesWidget: getTitles,
            reservedSize: 15,
          ),
        ),
        leftTitles: const AxisTitles(
          sideTitles: SideTitles(
            showTitles: false,
          ),
        ),
      ),
      borderData: FlBorderData(
        show: false,
      ),
      barGroups: showingGroups(),
      gridData: const FlGridData(show: false),
    );
  }

  Widget getTitles(double value, TitleMeta meta) {
    const style = TextStyle(
      color: Colors.grey,
      fontSize: 8,
    );
    return Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Text(service.abbreviationDay(value.toInt()), style: style));
  }
}
