import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:equido_flutter/ui/event/list/event_list_tile_light.dart';
import 'package:equido_flutter/ui/event/list/sliver_list_event.dart';
import 'package:equido_flutter/ui/horse/chart/view/event_bar_chart_expanded.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';

class EventListLight extends SliverListEvent {
  @override
  void initData(List<Event> newEvents) {
    Map<String, List<Event>> map = {};

    for (final Event event in newEvents.where((Event e) =>
        e.start.isBefore(DateTime.now()) &&
        (e.eventType == EventType.training ||
            e.eventType == EventType.outing))) {
      map.update(EquidoDate.makeDateEvent(event.start), (List<Event> e) {
        e.add(event);
        return e;
      }, ifAbsent: () => List.from([event]));
    }

    events = map;
  }

  List<Event> makeEvent(Map<String, List<Event>> map) {
    List<Event> events = [];

    map.forEach((key, value) {
      events.addAll(value);
    });

    return events;
  }

  @override
  List<Widget> buildSlivers() {
    List<Widget> slivers = [];

    slivers.add(SliverAppBar(
      backgroundColor: Colors.white,
      automaticallyImplyLeading: false,
      pinned: true,
      expandedHeight: 300,
      bottom: const PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: Text(''),
      ),
      flexibleSpace: EventsBarChartExpanded(makeEvent(events)),
    ));

    slivers.addAll(_buildHeaderBuilderLists());
    return slivers;
  }

  List<Widget> _buildHeaderBuilderLists() {
    List<Widget> widgets = [];

    for (var entry in events.entries) {
      String sliverDate = entry.key;
      int childCount = entry.value.length;

      widgets.add(SliverStickyHeader.builder(
        builder: (context, state) => buildAnimatedHeader(sliverDate, state),
        sliver: SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, i) => makeListTile(entry.value[i]),
            childCount: childCount,
          ),
        ),
      ));
    }

    return widgets.reversed.toList();
  }

  @override
  Widget makeListTile(Event event) {
    return EventListTileLight.makeListTile(event, context);
  }
}
