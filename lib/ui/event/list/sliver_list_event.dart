import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';

abstract class SliverListEvent {
  late Map<String, List<Event>> events;
  late BuildContext context;

  void initData(List<Event> newEvents) {
    Map<String, List<Event>> map = {};
    newEvents.sort((a, b) => a.start.compareTo(b.start));

    for (final Event event in newEvents) {
      map.update(EquidoDate.makeDateEvent(event.start), (List<Event> e) {
        e.add(event);
        return e;
      }, ifAbsent: () => List.from([newEvents]));
    }

    events = map;
  }

  void setContext(BuildContext context) {
    this.context = context;
  }

  Widget buildAnimatedHeader(String date, SliverStickyHeaderState? state,
      [GlobalKey? key]) {
    return GestureDetector(
      onTap: () => ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(date))),
      child: Container(
        key: key,
        height: 50,
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        alignment: Alignment.centerLeft,
        decoration: (state!.isPinned
            ? const BoxDecoration(color: Colors.white)
            : BoxDecoration(
                border: Border(
                    top: BorderSide(width: 10, color: Colors.grey[100]!)),
                color: Colors.white)),
        child: Text(
          date,
          style: (state.isPinned
              ? const TextStyle(
                  color: Colors.cyan, fontSize: 20, fontWeight: FontWeight.bold)
              : const TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold)),
        ),
      ),
    );
  }

  List<Widget> buildSlivers();

  Widget makeListTile(Event event);
}
