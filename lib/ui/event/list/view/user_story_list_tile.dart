import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

abstract class StoryListTile<T extends Event> {
  StoryListTile(T);

  static String makeHeaderTime(Event event) {
    return EquidoDate.toHourFormat(event.start);
  }

  static String makeExpandedHeaderTime(Event event) {
    return EquidoDate.translateDate(
        DateFormat('EEEE dd MMMM').format(event.start));
  }

  static Widget displayHorseName(Event event) {
    return Text(
      "${event.actorName} & ${event.horseName}",
      style: const TextStyle(fontSize: 15, color: Colors.black),
    );
  }

  static Widget displayType(Event event) {
    return Text(
      event.title,
      style: const TextStyle(fontSize: 20, color: Colors.black),
    );
  }

  Widget makeListTileEvent(T event, BuildContext context);
}
