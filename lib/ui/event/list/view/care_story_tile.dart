import 'package:equido_flutter/src/models/event/care.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/horse/logo.dart';
import 'package:equido_flutter/ui/event/info/view/event_info.dart';
import 'package:equido_flutter/ui/event/list/dialog/event_list_dialog.dart';
import 'package:equido_flutter/ui/event/list/view/user_story_list_tile.dart';
import 'package:flutter/material.dart';

class CareStoryTile extends StoryListTile<Care> {
  CareStoryTile(super.T);

  static Widget displayLogoEvent(Care care) {
    return Logo.careLogo(CareLogo.getAssetFromType(care.careType));
  }

  Widget displayTitle(Care care, BuildContext context) {
    return Text(
      care.title,
      style: const TextStyle(fontSize: 20, color: Colors.black),
    );
  }

  @override
  Widget makeListTileEvent(Care event, BuildContext context) {
    return GestureDetector(
      child: Container(
          height: 130,
          color: Colors.white,
          child:
              Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.end, children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                StoryListTile.makeHeaderTime(event),
                style: const TextStyle(color: Colors.grey, fontSize: 12),
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[displayTitle(event, context), StoryListTile.displayHorseName(event)],
                    ),
                  ),
                ),
                Expanded(
                    flex: 1,
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            displayLogoEvent(event),
                          ],
                        )))
              ],
            ),
          ])),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => EventInfo(event)),
        );
      },
      onLongPress: () async {
        await EventListDiaolog.removeEvent(context, Event.toEventOutput(event));
      },
    );
  }
}
