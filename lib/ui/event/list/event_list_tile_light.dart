import 'package:equido_flutter/src/models/event/care.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/event/outing.dart';
import 'package:equido_flutter/src/models/event/training.dart';
import 'package:equido_flutter/src/models/horse/logo.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:equido_flutter/ui/event/info/view/event_info.dart';
import 'package:flutter/material.dart';

class EventListTileLight {
  static Widget _title(Event event) {
    switch (event.eventType) {
      case EventType.training:
        Training training = event as Training;
        return Text(
            "${event.title} pendant ${EquidoDate.printDuration(training.getDuration())}",
            style: const TextStyle(color: Colors.black87));
      case EventType.outing:
        Outing outing = event as Outing;
        return Text(
            "${event.title} pendant ${EquidoDate.printDuration(outing.getDuration())}",
            style: const TextStyle(color: Colors.black87));
      case EventType.care:
        Care care = event as Care;
        return Text(care.title, style: const TextStyle(color: Colors.black87));
    }
  }

  static Widget _subtitle(Event event) {
    if (event.eventType == EventType.training) {
      Training training = event as Training;
      return Text(
          "${EquidoDate.toHourFormat(training.start)} - ${EquidoDate.toHourFormat(training.stop)}");
    } else if (event.eventType == EventType.outing) {
      Outing outing = event as Outing;
      return Text(
          "${EquidoDate.toHourFormat(outing.start)} - ${EquidoDate.toHourFormat(outing.stop)}");
    } else {
      return const SizedBox();
    }
  }

  static BoxDecoration _tileDecoration() {
    return BoxDecoration(
        border:
            Border(right: BorderSide(width: 1.0, color: Colors.grey[350]!)));
  }

  static Widget _displayLogo(Event event) {
    switch (event.eventType) {
      case EventType.training:
        return Logo.trainingLogoLight(
            TrainingType.getAssetBlackFromType(event.title));
      case EventType.outing:
        return Logo.outingLogoLight();
      case EventType.care:
        Care care = event as Care;
        return Logo.careLogoLight(
            CareLogo.getAssetBlackFromType(care.careType));
    }
  }

  static Widget makeListTile(Event event, BuildContext context) {
    return ListTile(
        title: _title(event),
        subtitle: _subtitle(event),
        leading: Container(
            padding:
                const EdgeInsets.only(left: 0, top: 15, right: 15, bottom: 15),
            decoration: _tileDecoration(),
            child: _displayLogo(event)),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => EventInfo(event)),
          );
        });
  }
}
