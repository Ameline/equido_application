import 'package:equido_flutter/src/bloc/events_bloc.dart';
import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/src/models/event_output/event_output.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EventListDiaolog {
  static Future removeEvent(
      BuildContext context, EventOutput eventOutput) async {
    final applicationBloc = BlocProvider.of<EventBloc>(context);

    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(
                EquidoLocalizations.of(context).getText("event_delete_answer")),
            actions: <Widget>[
              ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all(Colors.red)),
                child: Text(EquidoLocalizations.of(context).getText("cancel")),
              ),
              ElevatedButton(
                onPressed: () {
                  _showOkDelete(context);
                  EventBlocEvent blocEvent = EventBlocEvent(
                      eventType: EventBlocType.deleteEvent,
                      object: eventOutput);
                  applicationBloc.actionEventsController.add(blocEvent);
                },
                style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                child: Text(EquidoLocalizations.of(context).getText("confirm")),
              ),
            ],
          );
        });
  }

  static Future _showOkDelete(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content:
                Text(EquidoLocalizations.of(context).getText("event_delete")),
            actions: <Widget>[
              ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(EquidoLocalizations.of(context).getText("ok")))
            ],
          );
        }).then((var v) {
      Navigator.pop(context);
    });
  }
}
