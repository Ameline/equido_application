import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/event/training.dart';
import 'package:equido_flutter/src/models/horse/logo.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:equido_flutter/ui/event/info/service/event_async.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';

class TrainingInfo extends StatelessWidget {
  final Training training;

  const TrainingInfo(this.training, {super.key});

  Widget getLogoEvent() {
    return Logo.trainingLogo(TrainingType.getAssetFromType(training.title));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            EquidoWidget.titleBorderBottum(training.horseName),
            const SizedBox(height: 5.0),
            EquidoWidget.informationTextItalic(EquidoLocalizations.of(context)
                .getTextParameter("user_add_event", training.actorName)),
            const SizedBox(height: 25.0),
            Center(child: getLogoEvent()),
            EquidoWidget.makeTextInformationJustify(
                EquidoLocalizations.of(context).getText("start_hour"),
                "${EquidoDate.makeDateEvent(training.start)}, ${EquidoDate.toHourFormat(training.start)}"),
            EquidoWidget.makeTextInformationJustify(
                EquidoLocalizations.of(context).getText("duration"),
                EquidoDate.printDuration(training.getDuration())),
            EquidoWidget.makeTextInformationJustify(
                EquidoLocalizations.of(context).getText("note"), ""),
            const SizedBox(height: 5.0),
            Text(training.note),
            const SizedBox(height: 5.0),
            EquidoWidget.gradientButtonRed(context, "Supprimé l'évènement", () {
              const EventAsync()
                  .removeEvent(context, Event.toEventOutput(training), () {
                Navigator.popUntil(context, ModalRoute.withName("/"));
              });
            })
          ],
        ));
  }
}
