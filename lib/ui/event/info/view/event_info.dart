import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/src/models/event/care.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/event/outing.dart';
import 'package:equido_flutter/src/models/event/training.dart';
import 'package:equido_flutter/ui/event/info/view/care_info.dart';
import 'package:equido_flutter/ui/event/info/view/training_info.dart';
import 'package:flutter/material.dart';

import 'outing_info.dart';

class EventInfo extends StatelessWidget {
  final Event event;

  const EventInfo(this.event, {super.key});

  @override
  Widget build(BuildContext context) {
    final topAppBar = AppBar(
      backgroundColor: Colors.white,
      elevation: 0.0,
      title: Text(
        EquidoLocalizations.of(context).getText("training_detail"),
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      leading: IconButton(
        icon: const Icon(
          Icons.arrow_back,
          color: Colors.cyan,
        ),
        onPressed: () => Navigator.pop(context, false),
      ),
    );

    final body = _bodyFromEventType();

    return Scaffold(appBar: topAppBar, body: body);
  }

  Widget _bodyFromEventType() {
    switch (event.eventType) {
      case EventType.outing:
        return OutingInfo(event as Outing);
      case EventType.training:
        return TrainingInfo(event as Training);
      case EventType.care:
        return CareInfo(event as Care);
    }
  }
}
