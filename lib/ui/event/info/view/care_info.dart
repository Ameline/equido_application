import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/src/models/event/care.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/horse/logo.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:equido_flutter/ui/event/info/service/event_async.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';

class CareInfo extends StatelessWidget {
  final Care care;

  const CareInfo(this.care, {super.key});

  Widget getLogoEvent() {
    return Logo.careLogo(CareLogo.getAssetFromType(care.careType));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            EquidoWidget.titleBorderBottum(care.horseName),
            const SizedBox(height: 5.0),
            EquidoWidget.informationTextItalic(EquidoLocalizations.of(context)
                .getTextParameter("user_add_event", care.actorName)),
            const SizedBox(height: 25.0),
            Center(child: getLogoEvent()),
            EquidoWidget.makeTextInformationJustify(
                EquidoLocalizations.of(context).getText("start_hour"),
                "${EquidoDate.makeDateEvent(care.start)}, ${EquidoDate.toHourFormat(care.start)}"),
            EquidoWidget.makeTextInformationJustify(
                EquidoLocalizations.of(context).getText("duration"),
                EquidoDate.printDuration(null)),
            EquidoWidget.makeTextInformationJustify(
                EquidoLocalizations.of(context).getText("note"), ""),
            const SizedBox(height: 5.0),
            Text(care.note),
            const SizedBox(height: 5.0),
            EquidoWidget.gradientButtonRed(context, "Supprimé l'évènement", () {
              const EventAsync().removeEvent(context, Event.toEventOutput(care),
                  () {
                Navigator.popUntil(context, ModalRoute.withName("/"));
              });
            })
          ],
        ));
  }
}
