import 'package:equido_flutter/ui/event/list/dialog/event_list_dialog.dart';
import 'package:flutter/material.dart';

import '../../../../src/models/event_output/event_output.dart';

class EventAsync {
  const EventAsync();

  Future<void> removeEvent(BuildContext context, EventOutput outputEvent, VoidCallback onSuccess) async {
    await EventListDiaolog.removeEvent(context, outputEvent);
    onSuccess.call();
  }
}
