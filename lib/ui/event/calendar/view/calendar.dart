import "package:collection/collection.dart";
import 'package:equido_flutter/src/bloc/events_bloc.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:equido_flutter/ui/event/calendar/services/calendar_event_list.dart';
import 'package:equido_flutter/ui/event/creation/view/event_selection.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_calendar_carousel/classes/event.dart'
    as flutter_calendar;
import 'package:flutter_calendar_carousel/classes/event_list.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel;
import 'package:intl/intl.dart';

class Calendar extends StatefulWidget {
  const Calendar({super.key});

  @override
  CalendarState createState() => CalendarState();
}

class CalendarState extends State<Calendar> {
  DateTime _currentDate = DateTime.now();
  String _currentMonth = DateFormat.yMMMM().format(DateTime.now());
  DateTime _targetDateTime = DateTime.now().add(const Duration(days: 2));
  CalendarEventList eventList = CalendarEventList();
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
  }

  static final Widget _eventIcon = Container(
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.all(Radius.circular(1000)),
        border: Border.all(color: Colors.cyan, width: 2.0)),
    child: const Icon(
      Icons.person,
      color: Colors.amber,
    ),
  );

  EventList<flutter_calendar.Event> _getMapEventsFromEvent(List<Event> events) {
    List<flutter_calendar.Event> calendarEvents = events
        .map((e) => flutter_calendar.Event(
              date: e.start,
              title: e.title,
              id: e.id,
              icon: _eventIcon,
            ))
        .toList()
        .sorted((a, b) => a.date.compareTo(b.date));

    Map<DateTime, List<flutter_calendar.Event>> calendarEventsFromDate =
        groupBy(calendarEvents,
            (t) => DateTime(t.date.year, t.date.month, t.date.day));

    return EventList(events: calendarEventsFromDate);
  }

  @override
  Widget build(BuildContext context) {
    final applicationBloc = BlocProvider.of<EventBloc>(context);

    return StreamBuilder<List<Event>>(
        stream: applicationBloc.getEvents,
        initialData: applicationBloc.events,
        builder: (context, snapshotEvents) {
          eventList.initData(snapshotEvents.data!);
          eventList.setContext(context);

          EventList<flutter_calendar.Event> markedDateMap =
              _getMapEventsFromEvent(snapshotEvents.data!);

          return NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxScrolled) =>
                    EquidoWidget.collapsingToolbar(
                        EquidoDate.translateDate(_currentMonth),
                        actions: [
                  IconButton(
                      onPressed: () => setState(() {
                            _targetDateTime = DateTime(_targetDateTime.year,
                                _targetDateTime.month - 1);
                            _currentMonth =
                                DateFormat.yMMM().format(_targetDateTime);
                          }),
                      icon: const Icon(
                        Icons.navigate_before,
                        color: Colors.cyan,
                        //size: 24.0,
                      )),
                  IconButton(
                      onPressed: () => setState(() {
                            _targetDateTime = DateTime(_targetDateTime.year,
                                _targetDateTime.month + 1);
                            _currentMonth =
                                DateFormat.yMMM().format(_targetDateTime);
                          }),
                      icon: const Icon(
                        Icons.navigate_next,
                        color: Colors.cyan,
                        //size: 24.0,
                      )),
                ]),
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                const SizedBox(height: 30),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: _calendarCarousel(markedDateMap),
                ),
                _addEventButton(context),
                Expanded(
                    child: CustomScrollView(
                  controller: _scrollController,
                  slivers: eventList.buildSlivers(),
                )),
              ],
            ),
          );
        });
  }

  _addEventButton(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: EquidoWidget.blueButton(context,
            "Ajouter un évènement pour ${EquidoDate.makeDateEvent(_currentDate)}",
            () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => EventSelection(
                      true,
                      date: _currentDate,
                    )),
          );
        }));
  }

  _calendarCarousel(EventList<flutter_calendar.Event> markedDateMap) {
    return CalendarCarousel<flutter_calendar.Event>(
      todayBorderColor: Colors.cyan,
      onDayPressed: (date, events) {
        if (eventList.keys.containsKey(date)) {
          Scrollable.ensureVisible(eventList.keys[date]!.currentContext!);
        }
        setState(() => _currentDate = date);
      },
      daysHaveCircularBorder: true,
      showOnlyCurrentMonthDate: false,
      weekendTextStyle: const TextStyle(
        color: Colors.black,
      ),
      thisMonthDayBorderColor: Colors.transparent,
      weekFormat: false,
      markedDatesMap: markedDateMap,
      height: 350.0,
      selectedDateTime: _currentDate,
      targetDateTime: _targetDateTime,
      customGridViewPhysics: const NeverScrollableScrollPhysics(),
      showHeader: false,
      todayTextStyle:
          const TextStyle(color: Colors.cyan, fontWeight: FontWeight.bold),
      markedDateMoreShowTotal: true,
      todayButtonColor: Colors.white,
      selectedDayTextStyle:
          const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      selectedDayBorderColor: Colors.white,
      selectedDayButtonColor: Colors.cyan,
      minSelectedDate: _currentDate.subtract(const Duration(days: 360)),
      maxSelectedDate: _currentDate.add(const Duration(days: 360)),
      prevDaysTextStyle: const TextStyle(
        fontSize: 16,
        color: Colors.grey,
      ),
      inactiveDaysTextStyle: const TextStyle(
        color: Colors.tealAccent,
        fontSize: 16,
      ),
      onCalendarChanged: (DateTime date) {
        setState(() {
          _targetDateTime = date;
          _currentMonth = DateFormat.yMMMM().format(_targetDateTime);
        });
      },
      onDayLongPressed: (DateTime date) {},
    );
  }
}
