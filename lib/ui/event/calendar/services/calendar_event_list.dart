import 'dart:collection';

import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:equido_flutter/ui/event/list/event_list_tile_light.dart';
import 'package:equido_flutter/ui/event/list/sliver_list_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';

class CalendarEventList extends SliverListEvent {
  Map<DateTime, GlobalKey> keys = HashMap();

  @override
  void initData(List<Event> newEvents) {
    Map<String, List<Event>> map = {};
    newEvents.sort((a, b) => a.start.compareTo(b.start));

    for (final Event event in newEvents) {
      map.update(EquidoDate.makeDateEvent(event.start), (List<Event> e) {
        e.add(event);
        keys.putIfAbsent(
            DateTime(event.start.year, event.start.month, event.start.day),
            () => GlobalKey());
        return e;
      }, ifAbsent: () => List.from([event]));
    }

    events = map;
  }

  List<Event> makeEvent(Map<String, List<Event>> map) {
    List<Event> events = [];

    map.forEach((key, value) {
      events.addAll(value);
    });

    return events;
  }

  @override
  List<Widget> buildSlivers() {
    List<Widget> widgets = [];

    for (var entry in events.entries) {
      String sliverDate = entry.key;
      int childCount = entry.value.length;

      widgets.add(SliverStickyHeader.builder(
        builder: (context, state) => buildAnimatedHeader(
            sliverDate,
            state,
            keys[DateTime(entry.value.first.start.year,
                entry.value.first.start.month, entry.value.first.start.day)]),
        sliver: SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, i) => makeListTile(entry.value[i]),
            childCount: childCount,
          ),
        ),
      ));
    }

    return widgets.toList();
  }

  @override
  Widget makeListTile(Event event) {
    return Container(
        color: Colors.white,
        child: EventListTileLight.makeListTile(event, context));
  }
}
