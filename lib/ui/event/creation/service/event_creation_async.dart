import 'package:flutter/material.dart';

class EventCreationAsync {
  const EventCreationAsync();

  Future<void> getDatePicked(BuildContext context, DateTime date, VoidCallback Function(DateTime?) onSuccess) async {
    final DateTime? picked =
        await showDatePicker(context: context, initialDate: date, firstDate: DateTime(2015), lastDate: DateTime(2120));
    onSuccess.call(picked);
  }
}
