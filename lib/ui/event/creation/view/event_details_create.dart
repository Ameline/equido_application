import 'dart:async';

import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:equido_flutter/ui/event/creation/service/event_creation_async.dart';
import 'package:equido_flutter/ui/event/creation/view/create_care.dart';
import 'package:equido_flutter/ui/event/creation/view/create_outing.dart';
import 'package:equido_flutter/ui/event/creation/view/create_training.dart';
import 'package:equido_flutter/ui/form/equido_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EventDetailsCreate extends StatefulWidget {
  final EventType eventType;
  final String type;
  final DateTime date;

  const EventDetailsCreate(this.type, this.eventType, this.date, {super.key});

  @override
  State<StatefulWidget> createState() => EventDetailsCreateState();
}

class EventDetailsCreateState extends State<EventDetailsCreate> {
  late DateTime date;
  TimeOfDay hourStart = TimeOfDay.now();

  bool isCurrentHorseInit = false;
  late String currentHorseName;
  late String currentHorseId;

  @override
  void initState() {
    date = widget.date;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);

    final topAppBar = AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          _appBarTitle(),
          style: const TextStyle(color: Colors.cyan),
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.cyan,
          ),
          onPressed: () => Navigator.pop(context, false),
        ));

    DateTime start = DateTime(
        date.year, date.month, date.day, hourStart.hour, hourStart.minute);

    if (isCurrentHorseInit == false) {
      initCurrentHorse(applicationBloc.horses[0]);
    }

    switch (widget.eventType) {
      case EventType.training:
        return Scaffold(
            appBar: topAppBar,
            body: CreateTraining(widget.type, currentHorseId, start, this));
      case EventType.outing:
        return Scaffold(
            appBar: topAppBar,
            body: CreateOuting(widget.type, currentHorseId, start, this));
      case EventType.care:
        return Scaffold(
            appBar: topAppBar,
            body: CreateCare(widget.type, currentHorseId, start, this));
    }
  }

  initCurrentHorse(Horse horse) {
    setState(() {
      currentHorseName = horse.name;
      currentHorseId = horse.id;
      isCurrentHorseInit = true;
    });
  }

  Widget horseName(BuildContext context) {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);

    return StreamBuilder<List<Horse>>(
        stream: applicationBloc.getHorses,
        initialData: applicationBloc.horses,
        builder: (context, snapshotHorses) {
          return EquidoForm.namedInformationOnPressed(
              "Cheval", currentHorseName, () {
            _selectHorse(snapshotHorses.data as List<Horse>, context);
          });
        });
  }

  Future _selectHorse(List<Horse> horses, BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
              builder: (context, setState) => AlertDialog(
                    content: SizedBox(
                      height: 250.0, // Change as per your requirement
                      width: 250.0, //
                      child: // Ch
                          ListView.builder(
                        itemCount: horses.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ListTile(
                              title: Text(
                                horses[index].name,
                                style: TextStyle(
                                    color: isSelectedHorse(horses[index])
                                        ? Colors.blue
                                        : Colors.black,
                                    fontWeight: isSelectedHorse(horses[index])
                                        ? FontWeight.bold
                                        : FontWeight.normal),
                              ),
                              onTap: () {
                                initCurrentHorse(horses[index]);
                                Navigator.pop(context);
                              });
                        },
                      ),
                    ),
                    actions: <Widget>[
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        style: ButtonStyle(
                            backgroundColor:
                                WidgetStateProperty.all(Colors.red)),
                        child: const Text("Annuler"),
                      ),
                      ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                WidgetStateProperty.all(Colors.cyan)),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text("OK"),
                      ),
                    ],
                  ));
        });
  }

  Future _selectDate(BuildContext context) async {
    const EventCreationAsync().getDatePicked(context, date, (dynamic picked) {
      if (picked != null && picked != date) {
        setState(() {
          date = picked;
        });
      }
      _selectTimeStart(context);
      return picked;
    });
  }

  Future _selectTimeStart(BuildContext context) async {
    final TimeOfDay? picked =
        await showTimePicker(context: context, initialTime: hourStart);

    if (picked != null && picked != hourStart) {
      setState(() {
        hourStart = picked;
      });
    }
  }

  Widget selectDateStart(DateTime start) {
    return EquidoForm.namedInformationOnPressed("Heure de début",
        "${EquidoDate.makeDateEvent(start)}, ${hourToString(TimeOfDay(hour: start.hour, minute: start.minute))}",
        () {
      _selectDate(context);
    });
  }

  bool isSelectedHorse(Horse horse) {
    return horse.name == currentHorseName;
  }

  String hourToString(TimeOfDay time) {
    String hour = time.hour.toString();
    String minute =
        time.minute < 10 ? "0${time.minute}" : time.minute.toString();

    return "$hour:$minute";
  }

  String _appBarTitle() {
    switch (widget.eventType) {
      case EventType.training:
        return "Entrainement";
      case EventType.care:
        return "Soin";
      case EventType.outing:
        return "Sortie";
    }
  }
}
