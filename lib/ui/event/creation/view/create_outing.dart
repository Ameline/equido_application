import 'dart:async';

import 'package:equido_flutter/src/bloc/events_bloc.dart';
import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/src/models/event_output/event_output.dart';
import 'package:equido_flutter/src/models/event_output/outing_output.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/horse/logo.dart';
import 'package:equido_flutter/ui/equido_alert_dialog.dart';
import 'package:equido_flutter/ui/event/creation/view/create_event_interface.dart';
import 'package:equido_flutter/ui/event/creation/view/event_details_create.dart';
import 'package:equido_flutter/ui/form/equido_form.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:numberpicker/numberpicker.dart';

class CreateOuting extends StatefulWidget {
  final String title;
  final String horseId;
  final DateTime start;
  final EventDetailsCreateState eventDetailsCreate;

  const CreateOuting(
      this.title, this.horseId, this.start, this.eventDetailsCreate,
      {super.key});

  @override
  State<StatefulWidget> createState() => _CreateOutingState();
}

class _CreateOutingState extends State<CreateOuting> implements CreateEventI {
  late OutingOutput updated;
  late DateTime stop;
  int duration = 30;

  @override
  void initState() {
    super.initState();

    stop = DateTime(widget.start.year, widget.start.month, widget.start.day,
        widget.start.hour, widget.start.minute + duration);

    updated = OutingOutput(
        horseId: widget.horseId,
        title: widget.title,
        beginAt: widget.start,
        endedAt: stop,
        id: Horse.dId,
        place: widget.title);
  }

  @override
  Widget build(BuildContext context) {
    DateTime stop = DateTime(widget.start.year, widget.start.month,
        widget.start.day, widget.start.hour, widget.start.minute + duration);

    updated = OutingOutput(
        horseId: widget.horseId,
        title: widget.title,
        beginAt: widget.start,
        endedAt: DateTime(widget.start.year, widget.start.month,
            widget.start.day, stop.hour, stop.minute),
        place: widget.title,
        id: -1);

    return SingleChildScrollView(
        child: Container(
            padding:
                const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  getLogoEvent(),
                  const SizedBox(height: 30),
                  widget.eventDetailsCreate.horseName(context),
                  getTypeEvent(),
                  getDurationEvent(),
                  widget.eventDetailsCreate.selectDateStart(updated.beginAt),
                  getTimeStopEvent(),
                  const SizedBox(height: 20),
                  getNoteEvent(),
                  createEventButton(),
                ])));
  }

  void _selectDuration() {
    // This need to be here, otherise the NumberPicker didn't update his view on the AlertDialog
    int newDuration = duration;

    showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              content: NumberPicker(
                onChanged: (value) {
                  setState(() {
                    newDuration = value;
                    duration = value;
                    stop = DateTime(
                        updated.beginAt.year,
                        updated.beginAt.month,
                        updated.beginAt.day,
                        updated.beginAt.hour,
                        updated.beginAt.minute + newDuration);
                  });
                },
                minValue: 0,
                step: 30,
                maxValue: 300,
                value: newDuration,
              ),
              actions: <Widget>[
                ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(EquidoLocalizations.of(context).getText("ok")))
              ],
            );
          });
        });
  }

  @override
  Widget getDurationEvent() {
    return EquidoForm.namedInformationOnPressed("Durée", "$duration minutes",
        () {
      _selectDuration();
    });
  }

  Future _selectTimeStop(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
        context: context,
        initialTime: TimeOfDay(hour: stop.hour, minute: stop.minute));

    if (picked != null &&
        picked != TimeOfDay(hour: stop.hour, minute: stop.minute)) {
      setState(() {
        stop = DateTime(updated.beginAt.year, updated.beginAt.month,
            updated.beginAt.day, picked.hour, picked.minute);
        DateTime tmp = DateTime(updated.beginAt.year, updated.beginAt.month,
            updated.beginAt.day, stop.hour, stop.minute);
        duration = tmp.difference(updated.beginAt).inMinutes;
      });
    }
  }

  @override
  Widget getLogoEvent() {
    return Logo.outingLogo(size: LogoSize.max);
  }

  @override
  Widget getTypeEvent() {
    return EquidoWidget.makeTextInformationJustify("Lieu", updated.title);
  }

  @override
  Widget getNoteEvent() {
    return const SizedBox();
  }

  Widget getTimeStopEvent() {
    return EquidoForm.namedInformationOnPressed(
        "Heure de fin",
        widget.eventDetailsCreate
            .hourToString(TimeOfDay(hour: stop.hour, minute: stop.minute)), () {
      _selectTimeStop(context);
    });
  }

  @override
  Widget createEventButton() {
    final applicationBloc = BlocProvider.of<EventBloc>(context);

    return Container(
        padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        child: ElevatedButton(
            style: ButtonStyle(
                backgroundColor: WidgetStateProperty.all<Color>(Colors.cyan)),
            child: const Text("Ok"),
            onPressed: () async {
              EquidoAlertDialog.onLoading(context);
              DateStatus status = updated.verifyDate();
              if (status == DateStatus.ok) {
                EventBlocEvent blocEvent = EventBlocEvent(
                    eventType: EventBlocType.addOuting, object: updated);
                applicationBloc.actionEventsController.add(blocEvent);
              }
              Navigator.popUntil(context, ModalRoute.withName('/'));
            }));
  }
}
