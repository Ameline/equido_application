import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/src/models/event/care.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/event/outing.dart';
import 'package:equido_flutter/src/models/event/training.dart';
import 'package:equido_flutter/src/models/horse/logo.dart';
import 'package:equido_flutter/ui/event/creation/dialog/select_event_dialog.dart';
import 'package:equido_flutter/ui/event/creation/view/event_details_create.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EventSelection extends StatefulWidget {
  // Cette vue est-elle empillé sur une autre (retour en arrière possible)
  final bool stacked;
  final DateTime? date;

  const EventSelection(this.stacked, {super.key, this.date});

  @override
  State<StatefulWidget> createState() => _EventSelectionState();
}

class _EventSelectionState extends State<EventSelection> {
  final double _size = 150;
  late DateTime date;

  _EventSelectionState();

  @override
  initState() {
    super.initState();

    if (widget.date == null) {
      date = DateTime.now();
    } else {
      date = widget.date!;
    }
  }

  @override
  Widget build(BuildContext context) {
    final applicationBloc = BlocProvider.of<HorseBloc>(context);

    final horsesSize = applicationBloc.horses.length;

    if (widget.stacked) {
      return Scaffold(appBar: _appBar(context), body: _body(context, horsesSize));
    } else {
      return NestedScrollView(headerSliverBuilder: _headerSliverBar(), body: _body(context, horsesSize));
    }
  }

  NestedScrollViewHeaderSliversBuilder _headerSliverBar() {
    return (BuildContext context, bool innerBoxScrolled) => EquidoWidget.collapsingToolbar(
          EquidoLocalizations.of(context).getText("add_event"),
        );
  }

  PreferredSizeWidget _appBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0.0,
      title: const Text(
        "Ajouter un évènement",
        style: TextStyle(color: Colors.cyan),
      ),
      leading: IconButton(
        icon: const Icon(
          Icons.arrow_back,
          color: Colors.cyan,
        ),
        onPressed: () => Navigator.pop(context, false),
      ),
    );
  }

  Widget _body(BuildContext context, int horsesSize) {
    return SingleChildScrollView(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        EquidoWidget.titleBold("Entrainement"),
        const SizedBox(height: 20),
        SizedBox(
          height: _size,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              _logoSubtitleTraining(TrainingType.jumping, horsesSize, context),
              _logoSubtitleTraining(TrainingType.dressage, horsesSize, context),
              _logoSubtitleTraining(TrainingType.liberty, horsesSize, context),
              _logoSubtitleTraining(TrainingType.outside, horsesSize, context),
            ],
          ),
        ),
        EquidoWidget.titleBold("Soin"),
        const SizedBox(height: 20),
        SizedBox(
          height: _size,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              _logoSubtitleCare(CareType.shoeing, horsesSize, context),
              _logoSubtitleCare(CareType.vaccine, horsesSize, context),
              _logoSubtitleCare(CareType.wormer, horsesSize, context),
              _logoSubtitleCare(CareType.other, horsesSize, context),
            ],
          ),
        ),
        EquidoWidget.titleBold("Sortie"),
        const SizedBox(height: 20),
        _logoSubtitleOuting("Près", OutingType.defaultOuting, horsesSize, context),
      ],
    ));
  }

  GestureDetector _logoSubtitleTraining(String type, int horsesSize, BuildContext context) {
    String assets = TrainingType.getAssetFromType(type);
    String title = TrainingType.getTitleFromType(type);

    return GestureDetector(
        onTap: () {
          if (horsesSize == 0) {
            SelectEventDialog.showErrorMessage(context);
          } else {
            Navigator.push(context, MaterialPageRoute(builder: (context) => EventDetailsCreate(type, EventType.training, date)));
          }
        },
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              child: Logo.trainingLogo(assets, size: LogoSize.max),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 15),
                child: Text(title, style: const TextStyle(fontSize: 20, color: Colors.blueGrey)))
          ],
        ));
  }

  GestureDetector _logoSubtitleCare(CareType type, int horsesSize, BuildContext context) {
    String title = CareLogo.getTitleFromType(type, context);
    return GestureDetector(
        onTap: () {
          if (horsesSize == 0) {
            SelectEventDialog.showErrorMessage(context);
          } else {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => EventDetailsCreate(type.toString(), EventType.care, date)));
          }
        },
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              child: Logo.careLogo(CareLogo.getAssetFromType(type), size: LogoSize.max),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 15),
                child: Text(title, style: const TextStyle(fontSize: 20, color: Colors.blueGrey)))
          ],
        ));
  }

  GestureDetector _logoSubtitleOuting(String title, String type, int horsesSize, BuildContext context) {
    return GestureDetector(
        onTap: () {
          if (horsesSize == 0) {
            SelectEventDialog.showErrorMessage(context);
          } else {
            Navigator.push(context, MaterialPageRoute(builder: (context) => EventDetailsCreate(type, EventType.outing, date)));
          }
        },
        child: Column(
          children: <Widget>[
            Padding(padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10), child: Logo.outingLogo(size: LogoSize.max)),
            Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 15),
                child: Text(title, style: const TextStyle(fontSize: 20, color: Colors.blueGrey)))
          ],
        ));
  }
}
