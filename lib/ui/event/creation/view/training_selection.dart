import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/event/training.dart';
import 'package:equido_flutter/src/models/horse/logo.dart';
import 'package:equido_flutter/ui/event/creation/view/event_details_create.dart';
import 'package:flutter/material.dart';

class TrainingSelection extends StatefulWidget {
  final DateTime date;

  const TrainingSelection(this.date, {super.key});

  @override
  State<StatefulWidget> createState() => _TrainingSelectionState();
}

class _TrainingSelectionState extends State<TrainingSelection> {
  _TrainingSelectionState();

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: _appBar(context), body: _body(context));
  }

  PreferredSizeWidget _appBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0.0,
      title: const Text(
        "Ajouter un évènement",
        style: TextStyle(color: Colors.cyan),
      ),
      leading: IconButton(
        icon: const Icon(
          Icons.arrow_back,
          color: Colors.cyan,
        ),
        onPressed: () => Navigator.pop(context, false),
      ),
    );
  }

  Widget _body(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 20),
        SizedBox(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _logoSubtitleTraining(TrainingType.jumping, context),
                _logoSubtitleTraining(TrainingType.dressage, context),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _logoSubtitleTraining(TrainingType.liberty, context),
                _logoSubtitleTraining(TrainingType.outside, context),
              ],
            ),
          ],
        )),
      ],
    ));
  }

  GestureDetector _logoSubtitleTraining(String type, BuildContext context) {
    String assets = TrainingType.getAssetFromType(type);
    String title = TrainingType.getTitleFromType(type);

    return GestureDetector(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => EventDetailsCreate(type, EventType.training, widget.date)));
        },
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              child: Logo.trainingLogo(assets, size: LogoSize.maxmax),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 15),
                child: Text(title, style: const TextStyle(fontSize: 20, color: Colors.blueGrey)))
          ],
        ));
  }
}
