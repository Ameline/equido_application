import 'package:equido_flutter/src/bloc/events_bloc.dart';
import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/models/event/care.dart';
import 'package:equido_flutter/src/models/event_output/care_output.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/horse/logo.dart';
import 'package:equido_flutter/src/models/horse/tracked_care.dart';
import 'package:equido_flutter/ui/event/creation/view/create_event_interface.dart';
import 'package:equido_flutter/ui/event/creation/view/event_details_create.dart';
import 'package:equido_flutter/ui/form/equido_form.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CreateCare extends StatefulWidget {
  final String title;
  final String horseId;
  final DateTime start;
  final EventDetailsCreateState eventDetailsCreate;

  const CreateCare(
      this.title, this.horseId, this.start, this.eventDetailsCreate,
      {super.key});

  @override
  State<StatefulWidget> createState() => _CreateCareState();
}

class _CreateCareState extends State<CreateCare> implements CreateEventI {
  late CareOutput updated;
  String careName = "Soin";
  late String note;
  late String type;

  @override
  void initState() {
    super.initState();
    if (careName != "Soin") {
      type = careName;
    } else {
      type = widget.title;
    }

    updated = CareOutput(
        horseId: widget.horseId,
        title: type,
        doneOn: widget.start,
        id: Horse.dId,
        careType: Care.getFromString(type.split(".")[1]));
  }

  @override
  Widget build(BuildContext context) {
    updated = CareOutput(
        horseId: widget.horseId,
        title: _getCareName(),
        doneOn: widget.start,
        id: Horse.dId,
        careType: Care.getFromString(type.split(".")[1]));

    return SingleChildScrollView(
        child: Container(
            padding:
                const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  getLogoEvent(),
                  const SizedBox(height: 30),
                  widget.eventDetailsCreate.horseName(context),
                  getTypeEvent(),
                  getDurationEvent(),
                  widget.eventDetailsCreate.selectDateStart(updated.doneOn),
                  const SizedBox(height: 20),
                  getNoteEvent(),
                  createEventButton(),
                ])));
  }

  @override
  Widget getLogoEvent() {
    return Logo.careLogo(CareLogo.getAssetFromType(updated.careType),
        size: LogoSize.max);
  }

  @override
  Widget getTypeEvent() {
    return type == "CareType.OTHER"
        ? EquidoForm.makeTextFielForm2(
            "Type", careName, (value) => setState(() => careName = value))
        : EquidoWidget.makeTextInformationJustify(
            "Type",
            TrackedCare.careTypeToString(
                Care.getFromString(type.split(".")[1])));
  }

  @override
  Widget getNoteEvent() {
    return Card(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            maxLines: 8,
            decoration: const InputDecoration.collapsed(
                hintText: "Ajouter une note pour ce soin."),
            onChanged: (value) {
              setState(() => note = value);
            },
          ),
        ));
  }

  @override
  Widget getDurationEvent() {
    return const SizedBox();
  }

  @override
  Widget createEventButton() {
    final applicationBloc = BlocProvider.of<EventBloc>(context);
    final horseBloc = BlocProvider.of<HorseBloc>(context);

    return Container(
        padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        child: ElevatedButton(
            style: ButtonStyle(
                backgroundColor: WidgetStateProperty.all(Colors.cyan)),
            onPressed: () async {
              EventBlocEvent blocEvent = EventBlocEvent(
                  eventType: EventBlocType.addCare, object: updated);
              applicationBloc.actionEventsController.add(blocEvent);

              HorseBlocEvent blocEventHorse = HorseBlocEvent(
                  eventType: HorseBlocType.refreshCurrentHorse,
                  object: widget.horseId);
              horseBloc.actionEventsController.add(blocEventHorse);

              Navigator.popUntil(context, ModalRoute.withName('/'));
            },
            child: const Text("OK")));
  }

  String _getCareName() {
    if (careName != "Soin") {
      return careName;
    } else {
      return TrackedCare.careTypeToString(
          Care.getFromString(type.split(".")[1]));
    }
  }
}
