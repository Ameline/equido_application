import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:flutter/material.dart';

class SelectEventDialog {
  static Future showErrorMessage(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title:
                Text(EquidoLocalizations.of(context).getText("horses_empty")),
            content: Text(EquidoLocalizations.of(context)
                .getText("horses_empty_subtitle")),
            actions: <Widget>[
              TextButton(
                style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all(Colors.cyan)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(EquidoLocalizations.of(context).getText("ok"),
                    style: const TextStyle(color: Colors.white)),
              ),
            ],
          );
        });
  }
}
