import 'package:equido_flutter/ui/event/calendar/view/calendar.dart';
import 'package:equido_flutter/ui/event/creation/view/event_selection.dart';
import 'package:equido_flutter/ui/user/view/home_page.dart';
import 'package:equido_flutter/ui/user/view/user_info.dart';
import 'package:flutter/material.dart';

class BottomTab extends StatefulWidget {

  const BottomTab({super.key});

  @override
  BottomTabState createState() => BottomTabState();
}

class BottomTabState extends State<BottomTab> {
  int selectedIndex = 0; //to handle which item is currently selected in the bottom app bar

  //call this method on click of each bottom app bar item to update the screen
  void _updateTabSelection(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = <Widget>[
      const HomePage(),
      const EventSelection(false),
      const Calendar(),
      const UserInfo(),
    ];

    return Scaffold(
      //appBar: _appBar(),
      body: Center(
        child: widgets.elementAt(selectedIndex),
      ),
      bottomNavigationBar: _bottomAppBar(),
    );
  }

  Widget _bottomAppBar() {
    return BottomAppBar(
      shape: const CircularNotchedRectangle(),
      //color of the BottomAppBar
      color: Colors.white,
      child: Container(
        margin: const EdgeInsets.only(left: 12.0, right: 12.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              //update the bottom app bar view each time an item is clicked
              onPressed: () {
                _updateTabSelection(0);
              },
              iconSize: 27.0,
              icon: Icon(
                Icons.home_filled,
                color: selectedIndex == 0 ? Colors.black : Colors.grey.shade400,
              ),
            ),
            IconButton(
              onPressed: () {
                _updateTabSelection(1);
              },
              iconSize: 27.0,
              icon: Icon(
                Icons.add,
                color: selectedIndex == 1 ? Colors.black : Colors.grey.shade400,
              ),
            ),
            IconButton(
              onPressed: () {
                _updateTabSelection(2);
              },
              iconSize: 27.0,
              icon: Icon(
                Icons.today,
                color: selectedIndex == 2 ? Colors.black : Colors.grey.shade400,
              ),
            ),
            IconButton(
              onPressed: () {
                _updateTabSelection(3);
              },
              iconSize: 27.0,
              icon: Icon(
                Icons.account_circle,
                color: selectedIndex == 3 ? Colors.black : Colors.grey.shade400,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
