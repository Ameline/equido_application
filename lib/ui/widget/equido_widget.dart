import 'package:flutter/material.dart';

class EquidoWidget {
  ///*********** GRADIENT ***********///
  static LinearGradient blueGradient() {
    return const LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: <Color>[Colors.cyanAccent, Colors.blueAccent],
    );
  }

  static LinearGradient blueGradientSoft() {
    return LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: <Color>[Colors.lightBlue.shade200, Colors.cyan.shade200],
    );
  }

  static LinearGradient blueGradientButton({double opacity = 1}) {
    return LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: <Color>[Color.fromRGBO(0, 213, 240, opacity), Colors.blueAccent],
    );
  }

  static LinearGradient orangeGradient() {
    return const LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: <Color>[Colors.orangeAccent, Colors.deepOrange],
    );
  }

  static LinearGradient greenGradient() {
    return const LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: <Color>[Colors.lightGreenAccent, Colors.lightGreen],
    );
  }

  static LinearGradient orangeGradientSoft() {
    return LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: <Color>[Colors.orange.shade50, Colors.white],
    );
  }

  static LinearGradient redGradient() {
    return const LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: <Color>[
        Colors.red,
        Colors.pink,
      ],
    );
  }

  static LinearGradient greyGradient() {
    return const LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: <Color>[
        Colors.grey,
        Colors.white,
      ],
    );
  }

  static LinearGradient redGradientSoft() {
    return LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: <Color>[
        Colors.red.shade50,
        Colors.white,
      ],
    );
  }

  ///*********** TITLE ***********///
  static Widget titleBorderBottum(String text) {
    return Container(
      alignment: const Alignment(-1.0, -1.0),
      padding: const EdgeInsets.only(left: 0, top: 15, right: 15, bottom: 10),
      decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(width: 0.2, color: Colors.black))),
      child: Text(text, style: const TextStyle(fontSize: 20)),
    );
  }

  static Widget titleContainer(String text) {
    return Container(
      alignment: const Alignment(-1.0, -1.0),
      padding: const EdgeInsets.only(left: 15, top: 0, right: 15, bottom: 10),
      child: Text(text, style: const TextStyle(fontSize: 20)),
    );
  }

  static Widget titleBold(String text, {centered = false}) {
    if (!centered) {
      return Container(
        alignment: const Alignment(-1.0, -1.0),
        padding:
            const EdgeInsets.only(left: 15, top: 15, right: 15, bottom: 10),
        child: Text(text,
            style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      );
    } else {
      return Center(
          child: Padding(
        padding:
            const EdgeInsets.only(left: 15, top: 15, right: 15, bottom: 10),
        child: Text(text,
            style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      ));
    }
  }

  ///*********** TEXT ***********///
  static Widget informationTextItalic(String text,
      {Color color = Colors.grey}) {
    return Text(text,
        style: TextStyle(color: color, fontStyle: FontStyle.italic));
  }

  static Widget makeTextInformation(String text, String value) {
    return Container(
      padding: const EdgeInsets.only(top: 15, left: 10, right: 10),
      child: Row(
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Text(
                text,
                textAlign: TextAlign.left,
                style: const TextStyle(color: Colors.grey),
              )),
          Expanded(flex: 2, child: Text(value, textAlign: TextAlign.right))
        ],
      ),
    );
  }

  static Widget makeTextInformationJustify(String text, String value) {
    return Container(
      padding: const EdgeInsets.only(top: 15, right: 20),
      child: Row(
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Text(
                text,
                textAlign: TextAlign.start,
                style: const TextStyle(color: Colors.grey, fontSize: 20),
              )),
          Expanded(
              flex: 1,
              child: Text(
                value,
                textAlign: TextAlign.end,
                style: const TextStyle(fontSize: 20),
              )),
        ],
      ),
    );
  }

  static Widget makeSubTitle(String text) {
    return Expanded(
        flex: 1,
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: const TextStyle(color: Colors.grey),
        ));
  }

  ///*********** BUTTON**********///
  static Widget gradientButtonBlue(
      BuildContext context, String text, void Function() onPressed) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
          padding: EdgeInsets.zero,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
      child: Container(
        decoration: BoxDecoration(
            gradient: blueGradient(), borderRadius: BorderRadius.circular(20)),
        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20),
        child: Text(
          text,
          style: const TextStyle(fontSize: 15),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  static Widget gradientButtonBlueSoft(
      BuildContext context, String text, void Function() onPressed) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
          padding: EdgeInsets.zero,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
      child: Container(
        decoration: BoxDecoration(
            gradient: blueGradientSoft(),
            borderRadius: BorderRadius.circular(10)),
        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10),
        child: Text(
          text,
          style: const TextStyle(fontSize: 15),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  static Widget gradientButtonRed(
      BuildContext context, String text, void Function() onPressed) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
          padding: EdgeInsets.zero,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
      child: Container(
        decoration: BoxDecoration(
            gradient: redGradient(), borderRadius: BorderRadius.circular(20)),
        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20),
        child: Text(text, style: const TextStyle(fontSize: 15)),
      ),
    );
  }

  static Widget blueButton(
      BuildContext context, String text, void Function() onPressed) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ButtonStyle(
          elevation: WidgetStateProperty.all(1),
          padding: WidgetStateProperty.all(
              const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 5.0)),
          backgroundColor: WidgetStateProperty.all(Colors.cyan),
          shape: WidgetStateProperty.all<OutlinedBorder>(
              const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(18)))),
          side: WidgetStateProperty.all(
              const BorderSide(color: Colors.cyan, width: 0.5))),
      child: Text(
        text,
        style: const TextStyle(fontSize: 15, color: Colors.white),
        textAlign: TextAlign.center,
      ),
    );
  }

  static Widget blueButtonGradient(
      BuildContext context, String text, void Function() onPressed,
      {bool fullWidth = false, double opacity = 1}) {
    return Container(
        width: fullWidth ? double.infinity : null,
        decoration: BoxDecoration(
          boxShadow: const [
            BoxShadow(
                color: Colors.black26, offset: Offset(0, 4), blurRadius: 5.0)
          ],
          gradient: blueGradientButton(opacity: opacity),
          borderRadius: BorderRadius.circular(20),
        ),
        child: ElevatedButton(
          onPressed: onPressed,
          style: ButtonStyle(
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
            ),
            backgroundColor: WidgetStateProperty.all(Colors.transparent),
            // elevation: WidgetStateProperty.all(3),
            shadowColor: WidgetStateProperty.all(Colors.transparent),
          ),
          child: Text(
            text,
            style: const TextStyle(fontSize: 15, color: Colors.white),
            textAlign: TextAlign.center,
          ),
        ));
  }

  static Widget lightGreyButtonFullWidth(
      BuildContext context, String text, void Function() onPressed) {
    return Container(
        width: double.infinity,
        decoration: BoxDecoration(
            boxShadow: const [
              BoxShadow(
                  color: Colors.black26, offset: Offset(0, 4), blurRadius: 5.0)
            ],
            color: Colors.grey.shade100, //gradient: blueGradientButton(),
            borderRadius: BorderRadius.circular(20)),
        child: ElevatedButton(
          onPressed: onPressed,
          style: ButtonStyle(
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
            ),
            backgroundColor: WidgetStateProperty.all(Colors.transparent),
            shadowColor: WidgetStateProperty.all(Colors.transparent),
          ),
          child: Text(
            text,
            style: const TextStyle(fontSize: 15, color: Colors.grey),
            textAlign: TextAlign.center,
          ),
        ));
  }

  static Widget lightGreyButton(
      BuildContext context, String text, void Function() onPressed) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ButtonStyle(
          elevation: WidgetStateProperty.all(1),
          padding: WidgetStateProperty.all(const EdgeInsets.all(5.0)),
          backgroundColor: WidgetStateProperty.all(Colors.white),
          shape: WidgetStateProperty.all<OutlinedBorder>(
              const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(18)))),
          side: WidgetStateProperty.all(
              const BorderSide(color: Colors.grey, width: 0.5))),
      child: Text(
        text,
        style: const TextStyle(fontSize: 15, color: Colors.grey),
        textAlign: TextAlign.center,
      ),
    );
  }

  static Widget redButton(
      BuildContext context, String text, void Function() onPressed) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ButtonStyle(
          elevation: WidgetStateProperty.all(1),
          padding: WidgetStateProperty.all(const EdgeInsets.all(5.0)),
          backgroundColor: WidgetStateProperty.all(Colors.redAccent),
          shape: WidgetStateProperty.all<OutlinedBorder>(
              const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(18)))),
          side: WidgetStateProperty.all(
              const BorderSide(color: Colors.redAccent, width: 0.5))),
      child: Text(
        text,
        style: const TextStyle(fontSize: 15, color: Colors.white),
        textAlign: TextAlign.center,
      ),
    );
  }

  ///*********** TEXT FIELD **********///

  static TextField buildRoundedTextField(
      TextEditingController value, String hintText, String errorText) {
    return TextField(
        obscureText: false,
        controller: value,
        decoration: InputDecoration(
            contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            errorText: errorText,
            hintText: hintText,
            focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(32.0),
                borderSide: const BorderSide(color: Colors.cyan, width: 2)),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(32.0),
                borderSide: const BorderSide(color: Colors.cyan)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(32.0),
                borderSide: const BorderSide(color: Colors.cyan)),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))));
  }

  static TextField buildRoundedTextFieldVisibleControl(
      TextEditingController value,
      String hintText,
      String errorText,
      bool passwordVisible,
      void Function() setState) {
    return TextField(
        obscureText: passwordVisible,
        controller: value,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          errorText: errorText,
          hintText: hintText,
          focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: const BorderSide(color: Colors.cyan, width: 2)),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: const BorderSide(color: Colors.cyan)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: const BorderSide(color: Colors.cyan)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          suffixIcon: IconButton(
            icon: Icon(
              // Based on passwordVisible state choose the icon
              passwordVisible ? Icons.visibility_off : Icons.visibility,
              color: Colors.grey,
            ),
            onPressed: setState,
          ),
        ));
  }

  static List<Widget> collapsingToolbar(String title, {List<Widget>? actions}) {
    return <Widget>[
      SliverAppBar(
          backgroundColor: Colors.grey.shade50,
          automaticallyImplyLeading: false,
          expandedHeight: 100.0,
          floating: false,
          pinned: false,
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: false,
            titlePadding: const EdgeInsets.only(left: 20, bottom: 15),
            title: Text(
              title,
              style: const TextStyle(color: Colors.cyan),
            ),
          ),
          actions: actions)
    ];
  }

  ///*********** SHADOW **********///

  static List<BoxShadow> shadow() {
    return <BoxShadow>[
      const BoxShadow(
        color: Colors.black12,
        blurRadius: 10.0,
        offset: Offset(0.0, 10.0),
      ),
    ];
  }
}
