class EquidoAsset {
  static const String whitePaddockLogo = "assets/logos/paddock_logo_white.png";
  static const String blackPaddocLogo = "assets/logos/paddock_logo_black.png";
  static const String whiteEquidoLogo = "assets/logos/equido_logo_white.png";
  static const String cyanEquidoLogo = "assets/logos/equido_logo_cyan.png";
  static const String blackEquidoLogo = "assets/logos/equido_logo_black.png";
  static const String horseHeadLineLogo = 'assets/logos/horse-head-lines.png';

  static const String whiteJumpingLogo = "assets/logos/jumping_logo_white.png";
  static const String whiteDressageLogo = "assets/logos/dressage_white.png";
  static const String whiteLibertyLogo = "assets/logos/free_logo_white.png";
  static const String whiteOutsideLogo = "assets/logos/outside_logo_white.png";

  static const String blackJumpingLogo = "assets/logos/jumping_logo_black.png";
  static const String blackDressageLogo = "assets/logos/dressage_logo_black.png";
  static const String blackLibertyLogo = "assets/logos/free_logo_black.png";
  static const String blackOutsideLogo = "assets/logos/outside_logo_black.png";
}
