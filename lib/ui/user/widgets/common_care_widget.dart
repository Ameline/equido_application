import 'package:equido_flutter/ui/horse/statisics/commonCare/care_alert_config.dart';
import 'package:equido_flutter/ui/horse/statisics/horse_statistics_tabs.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';

class CommonCareWidget {
  static Widget careAlertHomePage(
      BuildContext context, CareAlertConfig alertConfig, String horseId) {
    return GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => HorseStatisticsTabs(
                      horseId, HorseStatisticsTabs.careIndex)));
        },
        child: Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.all(10),
            height: 170,
            width: 170,
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20.0),
                gradient: alertConfig.softGradient,
                boxShadow: EquidoWidget.shadow()),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _iconMessage(Icons.warning_rounded, alertConfig.gradient),
                const SizedBox(height: 15),
                Text(alertConfig.text,
                    style: const TextStyle(
                        fontSize: 16,
                        color: Colors.black54,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left),
                Text(alertConfig.subText,
                    style: const TextStyle(fontSize: 14, color: Colors.grey),
                    textAlign: TextAlign.left)
              ],
            )));
  }

  static Widget careAlertHomePageStatistic(
      BuildContext context, CareAlertConfig careAlertConfig, String horseId) {
    return GestureDetector(
        child: Container(
            margin: const EdgeInsets.only(right: 15),
            height: 70,
            width: 70,
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20.0),
                gradient: careAlertConfig.gradient,
                boxShadow: EquidoWidget.shadow()),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const SizedBox(height: 10),
                Text(careAlertConfig.careName,
                    style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 12)),
                Text(careAlertConfig.subText,
                    style: const TextStyle(fontSize: 10, color: Colors.white),
                    textAlign: TextAlign.center),
                const SizedBox(height: 10)
              ],
            )),
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => HorseStatisticsTabs(
                      horseId, HorseStatisticsTabs.careIndex)));
        });
  }

  static Widget _iconMessage(IconData iconData, LinearGradient gradient) {
    return Container(
      margin: const EdgeInsets.only(left: 10),
      height: 60,
      width: 60,
      decoration: BoxDecoration(
          gradient: gradient,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(20.0),
          boxShadow: EquidoWidget.shadow()),
      child: Icon(iconData, color: Colors.white, size: 40),
    );
  }
}
