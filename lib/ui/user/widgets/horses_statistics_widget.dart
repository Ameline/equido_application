import 'package:equido_flutter/src/bloc/events_bloc.dart';
import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/bloc/user_bloc.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/horse/tracked_care.dart';
import 'package:equido_flutter/ui/event/creation/view/event_selection.dart';
import 'package:equido_flutter/ui/horse/creation/view/add_horse.dart';
import 'package:equido_flutter/ui/horse/statisics/commonCare/care_alert_config.dart';
import 'package:equido_flutter/ui/horse/statisics/horse_statistics_tabs.dart';
import 'package:equido_flutter/ui/user/services/horses_statistics_service.dart';
import 'package:equido_flutter/ui/user/widgets/common_care_widget.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../event/charts/view/planned_training_chart.dart';
import '../../event/charts/view/training_time_chart_tile.dart';

class HorsesStatisticsWidget extends StatefulWidget {
  const HorsesStatisticsWidget({super.key});

  @override
  createState() => _HorsesStatisticsWidgetState();
}

class _HorsesStatisticsWidgetState extends State<HorsesStatisticsWidget> {
  final HorseStatisticsService horseStatisticsService =
      HorseStatisticsService();
  final double _paddingHorrizontal = 10;
  final double _paddingVertical = 20;

  @override
  Widget build(BuildContext context) {
    final horsesBloc = BlocProvider.of<HorseBloc>(context);
    final eventsBloc = BlocProvider.of<EventBloc>(context);

    return StreamBuilder<List<Horse>>(
        stream: horsesBloc.getHorses,
        initialData: horsesBloc.horses,
        builder: (context, snapshotHorses) {
          List<Horse> horses = snapshotHorses.data!;
          if (horses.isEmpty) {
            return containerWidget(emptyHorse());
          }

          return StreamBuilder<List<Event>>(
              stream: eventsBloc.getEvents,
              initialData: eventsBloc.events,
              builder: (context, eventsSnapshot) {
                List<Widget> allHorsesStatistics = [];

                for (Horse horse in horses) {
                  // Initialisation du service pour chaque cheval
                  horseStatisticsService.setCurrentHorse(horse);
                  horseStatisticsService
                      .setEventsForCurrentHorse(eventsSnapshot.data!);

                  var horseStatistic = Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 20),
                      _horseNameTitleWidget(horse),
                      const SizedBox(height: 20),
                      containerWidget(_plannedTrainingWidget(horse.id)),
                      containerWidget(_trainingWidget(horse.id)),
                      containerWidget(_caresWidget(context, horse)),
                      containerWidget(_sharing(horse)),
                      const SizedBox(height: 3),
                      _moreDetailsButton(horse.id),
                      const SizedBox(height: 10)
                    ],
                  );
                  allHorsesStatistics.add(horseStatistic);
                }

                return Column(children: allHorsesStatistics);
              });
        });
  }

  Widget containerWidget(Widget? widget) {
    if (widget == null) {
      return const SizedBox();
    }
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(20.0),
          boxShadow: EquidoWidget.shadow()),
      child: widget,
    );
  }

  Widget _horseNameTitleWidget(Horse horse) {
    return GestureDetector(
      child: Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Text(horse.name,
              style: const TextStyle(
                  color: Colors.cyan,
                  fontSize: 20.0,
                  fontWeight: FontWeight.w500))),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => HorseStatisticsTabs(
                    horse.id, HorseStatisticsTabs.historicIndex)));
      },
    );
  }

  //******************** PAS DE CHEVAL ********************//

  Widget emptyHorse() {
    return Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
          const SizedBox(height: 20),
          Image.asset("assets/logos/trailer.png", color: Colors.grey),
          const SizedBox(
              width: 200,
              child: Text(
                "Tu n'as aucun cheval enregistré dans l'application",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.grey),
              )),
          const SizedBox(height: 20),
          SizedBox(
              width: 210,
              child: EquidoWidget.blueButtonGradient(
                  context,
                  "Enregistre ton cheval",
                  () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const CreateHorse())))),
          const SizedBox(height: 10),
        ]));
  }

  //******************** SOINS ********************//
  Widget? _caresWidget(BuildContext context, Horse horse) {
    if (!horseStatisticsService.haveTrackedCareWarning()) {
      return null;
    }
    return Container(
      padding: EdgeInsets.all(_paddingHorrizontal),
      color: Colors.transparent,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          careWidget(context, horse.vaccine, horse.id),
          careWidget(context, horse.wormer, horse.id),
          careWidget(context, horse.shoeing, horse.id)
        ],
      ),
    );
  }

  Widget careWidget(
      BuildContext context, TrackedCare trackedCare, String horseId) {
    switch (trackedCare.status) {
      case StatusTrackedCare.soon:
        CareAlertConfig careAlertConfig = CareAlertConfig.warning(trackedCare);
        return CommonCareWidget.careAlertHomePageStatistic(
            context, careAlertConfig, horseId);
      case StatusTrackedCare.delay:
        CareAlertConfig careAlertConfig = CareAlertConfig.alert(trackedCare);
        return CommonCareWidget.careAlertHomePageStatistic(
            context, careAlertConfig, horseId);
      default:
        return const SizedBox();
    }
  }

  //******************** ENTRAINEMENT ********************//

  Widget _plannedTrainingWidget(String id) => Padding(
      padding: EdgeInsets.symmetric(
          horizontal: _paddingHorrizontal, vertical: _paddingVertical),
      child: PlannedTrainingChart(horseStatisticsService.eventsForCurrentHorse,
          horseStatisticsService.getSharing()));

  Widget _trainingWidget(String id) {
    return GestureDetector(
        child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: _paddingHorrizontal, vertical: _paddingVertical),
            child: TrainingTimeChartTile(
                horseStatisticsService.eventsForCurrentHorse)),
        onTap: () {
          if (horseStatisticsService.haveTrainingLastWeek()) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const EventSelection(true)));
          } else {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => HorseStatisticsTabs(
                        id, HorseStatisticsTabs.historicIndex)));
          }
        });
  }

  //******************** SORTIES ********************//
  // Outing chart have been removed

  //******************** SHARING ********************//

  Widget _sharing(Horse horse) {
    final userBloc = BlocProvider.of<UserBloc>(context);

    if (userBloc.user.id == horse.ownerId) {
      if (horse.getNumberOfSharing() > 0) {
        return _sharingContainer(
            "${horse.getNumberOfSharing()} partage(s)", horse.id);
      }
      return const SizedBox();
    }

    return _sharingContainer("Cheval partagé par ${horse.ownerName}", horse.id);
  }

  Widget _sharingContainer(String text, String horseId) {
    return GestureDetector(
        onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => HorseStatisticsTabs(
                    horseId, HorseStatisticsTabs.sharingIndex))),
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(
              horizontal: _paddingHorrizontal * 2,
              vertical: _paddingHorrizontal),
          child: Text(text,
              style: const TextStyle(color: Colors.black54, fontSize: 12)),
        ));
  }

//******************** PLUS D'INFO ********************//
  Widget _moreDetailsButton(String horseId) {
    return GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => HorseStatisticsTabs(
                      horseId, HorseStatisticsTabs.historicIndex)));
        },
        child: Container(
            padding: EdgeInsets.all(_paddingHorrizontal),
            height: 50,
            constraints: const BoxConstraints(minWidth: double.infinity),
            alignment: Alignment.centerLeft,
            child: const Row(
              children: [
                Icon(Icons.add, color: Colors.grey, size: 13),
                SizedBox(width: 6),
                Text("Plus de détails",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.grey)),
              ],
            )));
  }
}
