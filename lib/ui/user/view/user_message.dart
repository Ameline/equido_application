import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';

class UserMessage {
  final double marginMessage = 10;
  final double heightMessage = 90.0;

  Widget welcomUserMessage() {
    return Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.all(10),
        height: 160,
        width: 250,
        decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: EquidoWidget.shadow()),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            _iconMessage(Icons.wb_sunny_outlined, EquidoWidget.blueGradient()),
            const SizedBox(height: 15),
            const Text("Bienvenu sur Equido !",
                style: TextStyle(fontSize: 16, color: Colors.black54, fontWeight: FontWeight.bold), textAlign: TextAlign.left),
            const Text("Nous sommes heureux de t'acceuillir dans notre équipe",
                style: TextStyle(fontSize: 14, color: Colors.grey), textAlign: TextAlign.left),
          ],
        ));
  }

  Widget emptyEvent() {
    return const Text("Aucun evenements");
  }

  Widget _iconMessage(IconData iconData, LinearGradient gradient) {
    return Container(
      margin: const EdgeInsets.only(left: 10),
      height: 60,
      width: 60,
      decoration: BoxDecoration(
          gradient: gradient,
          //color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(20.0),
          boxShadow: EquidoWidget.shadow()),
      child: Icon(
        iconData,
        color: Colors.white,
      ),
    );
  }
}
