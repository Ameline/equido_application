import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/bloc/user_bloc.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/user/user.dart' as equido_user;
import 'package:equido_flutter/src/utils/equido_utils.dart';
import 'package:equido_flutter/ui/horse/creation/view/add_horse.dart';
import 'package:equido_flutter/ui/horse/info/view/horse_details.dart';
import 'package:equido_flutter/ui/user/services/user_info_service.dart';
import 'package:equido_flutter/ui/user/view/update_user_logo.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserInfo extends StatefulWidget {
  const UserInfo({super.key});

  @override
  State<StatefulWidget> createState() => UserInfoState();
}

class UserInfoState extends State<UserInfo> {
  final double _horseContainerMargin = 130;
  final double _iconSize = 79;

  @override
  Widget build(BuildContext context) {
    final userBloc = BlocProvider.of<UserBloc>(context);
    final horseBloc = BlocProvider.of<HorseBloc>(context);

    return StreamBuilder<equido_user.EquiUser>(
        stream: userBloc.getUser,
        initialData: userBloc.user,
        builder: (context, snapshotUser) {
          return StreamBuilder<List<Horse>>(
              stream: horseBloc.getHorses,
              initialData: horseBloc.horses,
              builder: (context, snapshotHorses) {
                return Scaffold(
                    body: _body(
                        snapshotUser.data!, snapshotHorses.data!, context));
              });
        });
  }

  Widget _body(
      equido_user.EquiUser user, List<Horse> horses, BuildContext context) {
    return NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxScrolled) =>
            _collapsingToolbar(user),
        body: SingleChildScrollView(
            padding:
                const EdgeInsets.symmetric(vertical: 16.0, horizontal: 10.0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  _container("Profil utilisateur", _userProfile(user, context)),
                  const SizedBox(height: 15.0),
                  _container("Mes chevaux", _userHorses(horses, user)),
                  const SizedBox(height: 15.0),
                  _container(
                      "Chevaux partagés avec moi", _sharedHorses(horses, user)),
                  const SizedBox(height: 35.0),
                  _logout(context)
                ])));
  }

  List<Widget> _collapsingToolbar(equido_user.EquiUser user) {
    return collapsingToolbar(
      user,
      actions: <Widget>[
        IconButton(
            icon: const Icon(
              Icons.edit,
              color: Colors.white,
            ),
            // TODO: edit user
            onPressed: () {})
      ],
    );
  }

  List<Widget> collapsingToolbar(equido_user.EquiUser user,
      {List<Widget>? actions}) {
    return <Widget>[
      SliverAppBar(
          backgroundColor: Colors.grey.shade50,
          automaticallyImplyLeading: false,
          expandedHeight: 100.0,
          floating: false,
          pinned: false,
          flexibleSpace: Container(
            decoration:
                BoxDecoration(gradient: user.pictureProfile.getGradient()),
            child: FlexibleSpaceBar(
              centerTitle: false,
              titlePadding: const EdgeInsets.only(left: 20, bottom: 15),
              title: Text(
                user.username,
                style: const TextStyle(color: Colors.white),
              ),
            ),
          ),
          actions: actions)
    ];
  }

  Widget _container(String title, Widget widget) {
    return Container(
        decoration: _decoration(),
        padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        //color: Colors.white,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              EquidoWidget.titleContainer(title),
              const SizedBox(height: 5.0),
              widget
            ]));
  }

  Widget _userProfile(equido_user.EquiUser user, BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        EquidoWidget.makeTextInformation("Nom d'utilisateur", user.username),
        const SizedBox(height: 5.0),
        EquidoWidget.makeTextInformation(
            "Prénom", user.firstName ?? EquidoUtils.nR),
        const SizedBox(height: 5.0),
        EquidoWidget.makeTextInformation(
            "Nom", user.lastName ?? EquidoUtils.nR),
        const SizedBox(height: 5.0),
        EquidoWidget.makeTextInformation("Mail", user.email),
        const SizedBox(height: 10.0),
        _updateUserLogo(context, user)
      ],
    );
  }

  Widget _updateUserLogo(BuildContext context, equido_user.EquiUser user) {
    return Padding(
        padding: const EdgeInsets.only(left: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            user.pictureProfile.widget(35),
            EquidoWidget.lightGreyButton(
                context,
                "Modifier le logo utilisateur",
                () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => UpdateUserLogo(user))))
          ],
        ));
  }

  Widget _userHorses(List<Horse> horses, equido_user.EquiUser user) {
    List<Horse> newHorses = UserInfoService.getOwnerHorses(horses, user.id);

    return SizedBox(
        height: _horseContainerMargin,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: newHorses.length + 1,
            itemBuilder: (BuildContext context, int index) {
              if (index == newHorses.length) {
                return _adHorseTile(context);
              } else {
                return _horseTile(
                    context,
                    newHorses[index],
                    UserInfoService.sharingMessage(
                        horses[index].getNumberOfSharing()));
              }
            }));
  }

  Widget _sharedHorses(List<Horse> horses, equido_user.EquiUser user) {
    List<Horse> newHorses = UserInfoService.getSharingHorses(horses, user.id);

    if (newHorses.isEmpty) {
      return const Text(
          "Aucun utilisateur a partagé son cheval avec vous pour le moment",
          style: TextStyle(color: Colors.grey));
    }

    return SizedBox(
        height: _horseContainerMargin,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: newHorses.length,
            itemBuilder: (BuildContext context, int index) {
              return _horseTile(
                  context, newHorses[index], newHorses[index].ownerName);
            }));
  }

  Widget _horseTile(BuildContext context, Horse horse, [String? subtitle]) {
    return GestureDetector(
      child: SizedBox(
          width: 100,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(5),
                child: Image.asset(
                  "assets/logos/horse_icon.png",
                  height: _iconSize,
                  width: _iconSize,
                  color: Colors.grey,
                ),
              ),
              Text(horse.name,
                  textAlign: TextAlign.center, overflow: TextOverflow.ellipsis),
              subtitle != null
                  ? Text(subtitle, style: const TextStyle(color: Colors.cyan))
                  : const SizedBox()
            ],
          )),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => HorseDetails(horse.id)),
        );
      },
    );
  }

  Widget _adHorseTile(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const CreateHorse()));
        },
        child: const Column(
          children: <Widget>[
            Icon(
              Icons.add_circle,
              color: Colors.lightGreen,
              size: 90,
            ),
            Text("Nouveau cheval", style: TextStyle(color: Colors.lightGreen))
          ],
        ));
  }

  Widget _logout(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 15),
      child: EquidoWidget.redButton(context, "Déconnexion", () {
        FirebaseAuth.instance.signOut();
        // widget._streamController.add(AuthenticationState.signedOut());
        Navigator.popUntil(context, ModalRoute.withName("/"));
      }),
    );
  }

  BoxDecoration _decoration() {
    return BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: EquidoWidget.shadow());
  }
}
