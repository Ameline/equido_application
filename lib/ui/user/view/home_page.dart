import 'package:equido_flutter/src/bloc/events_bloc.dart';
import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/bloc/user_bloc.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/user/user.dart';
import 'package:equido_flutter/ui/user/services/home_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../widget/equido_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final HomeService homeService = HomeService();

    final userBloc = BlocProvider.of<UserBloc>(context);
    final eventsBloc = BlocProvider.of<EventBloc>(context);
    final horsesBloc = BlocProvider.of<HorseBloc>(context);

    final EquiUser user = userBloc.user;

    return StreamBuilder<List<Event>>(
        stream: eventsBloc.getEvents,
        initialData: eventsBloc.events,
        builder: (context, eventsSnapshot) {
          return StreamBuilder<List<Horse>>(
              stream: horsesBloc.getHorses,
              initialData: horsesBloc.horses,
              builder: (context, horsesSnapshot) {
                homeService.resetWidget();

                // Soins a prévoir ou en retard
                // MESSAGE D'INFORMATION
                homeService.careWarningAndAlertMessage(
                    context, horsesSnapshot.data);
                // Message de bienvenu pour les nouveau utilisateurs
                homeService.firstDayMessage(user);

                return NestedScrollView(
                    headerSliverBuilder:
                        (BuildContext context, bool innerBoxScrolled) =>
                            EquidoWidget.collapsingToolbar(
                              "Quoi de neuf ?",
                            ),
                    body: RefreshIndicator(
                      onRefresh: () => homeService.refreshData(context),
                      child: SingleChildScrollView(
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                            SizedBox(
                                height: 200, child: _buildContent(homeService)),
                            homeService.horseStatistics()
                          ])),
                    ));
              });
        });
  }

  Widget _buildContent(HomeService homeService) {
    if (homeService.widgets.isEmpty) {
      return homeService.haveANiceDayMessage();
    } else {
      // Fix error "RenderFlex children have non-zero flex but incoming width constraints are unbounded."
      List<Widget> widgets = [];
      widgets.addAll(homeService.widgets);

      return SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widgets));
    }
  }
}
