import 'package:equido_flutter/src/models/user/profile_picture.dart';
import 'package:equido_flutter/src/models/user/user.dart';
import 'package:equido_flutter/ui/widget/equido_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../src/bloc/user_bloc.dart';

class UpdateUserLogo extends StatefulWidget {
  final EquiUser user;

  const UpdateUserLogo(this.user, {super.key});

  @override
  State<StatefulWidget> createState() => UpdateUserLogoState();
}

class UpdateUserLogoState extends State<UpdateUserLogo> {
  ProfilePicture profilePicture = ProfilePicture("", 1, []);

  @override
  void initState() {
    super.initState();
    profilePicture = widget.user.pictureProfile;
  }

  EquiUser _updatedUser() {
    EquiUser user = EquiUser.copy(widget.user);
    user.pictureProfile = profilePicture;

    return user;
  }

  @override
  Widget build(BuildContext context) {
    final userBloc = BlocProvider.of<UserBloc>(context);

    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: const Text(
            "Modification du logo",
            style: TextStyle(color: Colors.cyan),
          ),
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.cyan,
            ),
            onPressed: () => Navigator.pop(context, false),
          )),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            profilePicture.widget(200),
            const SizedBox(height: 40),
            EquidoWidget.lightGreyButton(
                context,
                "Logo aléatoire",
                () => setState(() {
                      profilePicture =
                          ProfilePicture.generateRandom(widget.user.username);
                    })),
            EquidoWidget.blueButton(context, " Sauvegarder ", () {
              UserBlocEvent userBlocEvent = UserBlocEvent(
                  eventType: UserBlocType.updateUserPictureProfile,
                  object: _updatedUser());
              userBloc.actionEventsController.add(userBlocEvent);
              Navigator.of(context).pop();
            })
          ],
        ),
      ),
    );
  }
}
