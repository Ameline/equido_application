import 'package:equido_flutter/src/bloc/events_bloc.dart';
import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/bloc/user_bloc.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/event/outing.dart';
import 'package:equido_flutter/src/models/event/training.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/horse/tracked_care.dart';
import 'package:equido_flutter/src/models/user/user.dart';
import 'package:equido_flutter/ui/horse/statisics/commonCare/care_alert_config.dart';
import 'package:equido_flutter/ui/user/view/user_message.dart';
import 'package:equido_flutter/ui/user/widgets/common_care_widget.dart';
import 'package:equido_flutter/ui/user/widgets/horses_statistics_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeService {
  final UserMessage userMessage = UserMessage();

  HomeService();

  List<Widget> widgets = [];

  resetWidget() {
    widgets = [];
  }

  refreshData(BuildContext context) async {
    final horsesBloc = BlocProvider.of<HorseBloc>(context);
    final userBloc = BlocProvider.of<UserBloc>(context);
    final eventsBloc = BlocProvider.of<EventBloc>(context);

    userBloc.getAllData();
    horsesBloc.getAllData();
    eventsBloc.getAllData(horsesBloc.horses.map((h) => h.id).toList());
  }

  Training? getLastTraining(List<Event> events) {
    DateTime today = DateTime.now();

    events = events
        .where((event) =>
            event.start.isBefore(today) &&
            event.eventType == EventType.training)
        .toList();
    events.sort((a, b) => b.start.compareTo(a.start));

    if (events.isEmpty) {
      return null;
    }

    return events.first as Training;
  }

  Outing? getLastOuting(List<Event> events) {
    DateTime today = DateTime.now();

    events = events
        .where((event) =>
            event.start.isBefore(today) && event.eventType == EventType.outing)
        .toList();
    events.sort((a, b) => a.start.compareTo(b.start));

    if (events.isEmpty) {
      return null;
    }

    return events.first as Outing;
  }

  //******************** WIDGETS ********************//
  firstDayMessage(EquiUser user) {
    DateTime today = DateTime.now();

    if (user.registrationDate
        .isAfter(today.subtract(const Duration(days: 5)))) {
      widgets.add(userMessage.welcomUserMessage());
    }
  }

  careWarningAndAlertMessage(BuildContext context, List<Horse>? horses) {
    if (horses == null || horses.isEmpty) {
      return widgets;
    }

    List<Widget?> careAlertWidget = [];

    for (var horse in horses) {
      careAlertWidget
          .add(_careWarningAndAlertMessage(context, horse.shoeing, horse.id));
      careAlertWidget
          .add(_careWarningAndAlertMessage(context, horse.wormer, horse.id));
      careAlertWidget
          .add(_careWarningAndAlertMessage(context, horse.vaccine, horse.id));
    }

    widgets.addAll(
        careAlertWidget.where((e) => e != null).map((e) => e!).toList());
  }

  Widget? _careWarningAndAlertMessage(
      BuildContext context, TrackedCare care, String horseId) {
    switch (care.status) {
      case StatusTrackedCare.soon:
        CareAlertConfig alertConfig = CareAlertConfig.warning(care);
        return CommonCareWidget.careAlertHomePage(
            context, alertConfig, horseId);

      case StatusTrackedCare.delay:
        CareAlertConfig alertConfig = CareAlertConfig.alert(care);
        return CommonCareWidget.careAlertHomePage(
            context, alertConfig, horseId);
      default:
        return null;
    }
  }

  Widget haveANiceDayMessage() {
    return SizedBox(
        height: 100,
        width: double.infinity,
        child: Image.asset(
          "assets/logos/image_5.jpg",
          fit: BoxFit.cover,
        ));
  }

  horseStatistics() {
    // Not const here otherwise the HorseBloc was not called in HorsesStatisticsWidget
    widgets.add(const HorsesStatisticsWidget());
    return const HorsesStatisticsWidget();
  }
}
