import 'package:equido_flutter/src/models/horse/horse.dart';

class UserInfoService {
  static getOwnerHorses(List<Horse> horses, String userId) {
    return horses.where((horse) => horse.ownerId == userId).toList();
  }

  static getSharingHorses(List<Horse> horses, String userId) {
    return horses.where((horse) => horse.ownerId != userId).toList();
  }

  static String sharingMessage(int sharingNumber) {
    if (sharingNumber == 0) {
      return "";
    } else if (sharingNumber == 1) {
      return "$sharingNumber partage";
    }
    return "$sharingNumber partages";
  }
}
