import 'package:equido_flutter/src/models/event/care.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/event/training.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/horse/tracked_care.dart';
import 'package:equido_flutter/src/models/user/profile_picture.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';

import '../../horse/chart/service/training_bar_chart.dart';

class HorseStatisticsService {
  late Map<String, List<Event>> eventByDate;
  bool isUserFirstDayOnApp = false;
  bool isUserHaveNoHorse = false;
  late Training lastTraining;

  late Horse horse;
  List<Event> eventsForCurrentHorse = [];
  List<Care> caresForCurrentHorse = [];

  setCurrentHorse(Horse horse) {
    this.horse = Horse.copy(horse);
  }

  bool haveTrainingLastWeek() {
    DateTime today = DateTime.now();
    DateTime previous = today
        .subtract(const Duration(days: TrainingsBarChartService.numberOfDay));

    return eventsForCurrentHorse
        .where((e) => e.start.isBefore(today) && e.start.isAfter(previous))
        .isEmpty;
  }

  setEventsForCurrentHorse(List<Event> events) {
    List<Event> test =
        events.where((event) => event.horseId == horse.id).toList();

    eventsForCurrentHorse = test;
  }

  Map<DateTime, SharedUserInfo> getSharing() {
    Map<DateTime, SharedUserInfo> userByDay = {};
    DateTime today = DateTime.now();

    List.generate(7, (index) {
      DateTime currentDay =
          DateTime(today.year, today.month, today.day + index);
      for (var element in horse.sharing) {
        List<DateTime> filtered = element.sharedDays
            .where((sharedDay) => sharedDay.isSameDate(currentDay))
            .toList();
        if (filtered.isNotEmpty) {
          userByDay.putIfAbsent(
              currentDay,
              () => SharedUserInfo(
                  element.userSharedName, element.sharedPictureProfile));
        }
      }
    });
    return userByDay;
  }

  bool haveTrackedCareWarning() {
    return horse.vaccine.status == StatusTrackedCare.delay ||
        horse.vaccine.status == StatusTrackedCare.soon ||
        horse.wormer.status == StatusTrackedCare.delay ||
        horse.wormer.status == StatusTrackedCare.soon ||
        horse.shoeing.status == StatusTrackedCare.delay ||
        horse.shoeing.status == StatusTrackedCare.soon;
  }
}

class SharedUserInfo {
  String userName;
  ProfilePicture randomPictureProfile;

  SharedUserInfo(this.userName, this.randomPictureProfile);
}
