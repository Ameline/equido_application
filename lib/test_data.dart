class TestData {
  static var today = DateTime.now();
  static var today1 = today.add(const Duration(days: 1));
  static var today2 = today.add(const Duration(days: 2));
  static var today3 = today.add(const Duration(days: 3));
  static var today4 = today.add(const Duration(days: 4));
  static var today7 = today.add(const Duration(days: 7));
  static var today8 = today.add(const Duration(days: 8));
  static var today_1 = today.add(const Duration(days: -1));
  static var today_2 = today.add(const Duration(days: -2));
  static var today_3 = today.add(const Duration(days: -3));
  static var today_7 = today.add(const Duration(days: -7));
  static var today_10 = today.add(const Duration(days: -10));

  static var horsesBody = '''    [{"id": 1,
    "name": "Domino",
    "size": 0,
    "kind": "Hongre",
    "color": "Noir",
    "ownerName": "Ameline",
    "ownerId": 1,
    "birthDate": "2005-04-23T18:25:43.511Z",
    "shoeing": {
      "frequency": 10
       },
    "wormer": {
      "deadline": 25,
      "dateDeadline": "${today.add(const Duration(days: 3)).toIso8601String()}",
      "careType": "WORMER",
      "nextCare": "${today.add(const Duration(days: 10)).toIso8601String()}"
       },
    "vaccine": {
      "dateDeadline": "${today.subtract(const Duration(days: 2)).toIso8601String()}",
      "careType": "VACCINE",
      "care": {
            "id": 1,
            "name": "Vaccin",
            "doneOn": "2021-07-10T18:25:43.511Z",
            "healerName": "Toto",
            "note": "",
            "horseName": "Domino",
            "horseId": 1,
            "careType": "VACCINE"
        }
       },
       "sharing": [
       {
       "beginAt": "2021-06-10T18:25:43.511Z",
       "endedAt": "2022-06-10T18:25:43.511Z",
       "horseId": 1,
       "userId": 1,
       "sharedUserId": 2,
       "sharedUserName": "Polo",
       "pictureProfile": {
          "offset": "0.6768126668962222",
          "colors": [{
              "r": 55,
              "g": 112,
              "b": 193
              },
              {
              "r": 0,
              "g": 0,
              "b": 0
              }
              ]
          }
       },
       {
       "beginAt": "2021-06-10T18:25:43.511Z",
       "endedAt": "2022-06-10T18:25:43.511Z",
       "horseId": 1,
       "userId": 1,
       "sharedUserId": 4,
       "sharedUserName": "Sihem",
       "pictureProfile": {
          "offset": "0.5464324244597535",
          "colors": [{
              "r": 243,
              "g": 67,
              "b": 173
              },
              {
              "r": 143,
              "g": 84,
              "b": 218
              }
              ]
          },
       "sharedCalendar": [
           {"date": "${today2.toIso8601String()}"},
           {"date": "${today4.toIso8601String()}"}
           ]
       }      
       ]
    },
    {"id": 2,
    "name": "Devil",
    "size": 0,
    "kind": "Hongre",
    "color": "Bai",
    "ownerName": "Polo",
    "ownerId": 2,
        "birthDate": "2015-04-23T18:25:43.511Z"
    },
    {"id": 3,
    "name": "Kondo",
    "size": 0,
    "kind": "Hongre",
    "color": "Bai",
    "ownerName": "Truddy",
    "ownerId": 3,
    "birthDate": "2015-04-23T18:25:43.511Z"
    }]''';

  static var shoeing = '''{
    "id": 1,
    "name": "Ferrure",
    "doneOn": "2021-06-10T18:25:43.511Z",
    "healerName": "Toto",
    "note": "Parrure, tout va bien",
    "deadlineWarning": "GOOD",
    "horseName": "Domino",
    "horseId": 1,
    "careType": "SHOEING" }''';

  static var vaccin = '''{
    "id": 2,
    "name": "Vaccin",
    "doneOn": "2021-06-10T18:25:43.511Z",
    "healerName": "Toto",
    "note": "Super",
    "deadlineWarning": "GOOD",
    "horseName": "Domino",
    "horseId": 1,
    "careType": "VACCINE" }''';

  static var user = '''
    {
     "id": 1,
    "username": "Ameline",
    "firstName": "Ameline",
    "lastName": "Moreau",
    "email": "ameline@orange.fr",
    "registrationDate": "2019-04-23T18:25:43.511Z",
    "pictureProfile": {
      "offset": "0.6740210816619133",
      "colors": [{
          "r": 12,
          "g": 199,
          "b": 230
          },
          {
          "r": 243,
          "g": 67,
          "b": 173
          }
          ]
      }
    }''';

  static var events = '''[
    {   "id": 1,
        "horseId": 1,
        "eventType": "ADDED_OUTING",
        "userId": 1,
        "createdAt": "2021-06-23T18:25:43.511Z",
        "outingId": 1,
        "horseName": "Domino",
        "userName": "Ameline"
        }
    ]''';

  static var horsesEvents = '''[{
    "id": 1,
    "title": "Dressage",
     "beginAt" : "${today_1.toIso8601String()}",
     "endedAt" : "${today_1.add(const Duration(minutes: 70)).toIso8601String()}",
     "trainerName": "Ameline",
     "comment" : "Super séance",
      "horseName": "Domino",
      "horseId": 1,
      "eventType": "TRAINING"
    },
    {
    "id": 11,
    "title": "Travail à pied",
     "beginAt" : "${today_10.toIso8601String()}",
     "endedAt" : "${today_10.add(const Duration(minutes: 70)).toIso8601String()}",
     "trainerName": "Ameline",
     "comment" : "Super séance",
      "horseName": "Domino",
      "horseId": 1,
      "eventType": "TRAINING"
    },
    {
    "id": 12,
    "title": "Travail à pied",
     "beginAt" : "${today_7.toIso8601String()}",
     "endedAt" : "${today_7.add(const Duration(minutes: 30)).toIso8601String()}",
     "trainerName": "Ameline",
     "comment" : "Super séance",
      "horseName": "Domino",
      "horseId": 1,
      "eventType": "TRAINING"
    },
    {
    "id": 2,
    "title": "Obstacle",
     "beginAt" : "${today_2.toIso8601String()}",
     "endedAt" : "${today_2.add(const Duration(minutes: 45)).toIso8601String()}",
     "trainerName": "Ameline",
     "comment" : "Super séance",
      "horseName": "Domino",
      "horseId": 1,
      "eventType": "TRAINING"
    },
        {
    "id": 2,
    "title": "Obstacle",
     "beginAt" : "${today_3.toIso8601String()}",
     "endedAt" : "${today_3.add(const Duration(minutes: 60)).toIso8601String()}",
     "trainerName": "Ameline",
     "comment" : "Super séance",
      "horseName": "Domino",
      "horseId": 1,
      "eventType": "TRAINING"
    },
            {
    "id": 2,
    "title": "Obstacle",
     "beginAt" : "${today7.toIso8601String()}",
     "endedAt" : "${today7.add(const Duration(minutes: 60)).toIso8601String()}",
     "trainerName": "Ameline",
     "comment" : "Super séance",
      "horseName": "Domino",
      "horseId": 1,
      "eventType": "TRAINING"
    },
            {
    "id": 2,
    "title": "Obstacle",
     "beginAt" : "${today8.toIso8601String()}",
     "endedAt" : "${today8.add(const Duration(minutes: 60)).toIso8601String()}",
     "trainerName": "Ameline",
     "comment" : "Super séance",
      "horseName": "Domino",
      "horseId": 1,
      "eventType": "TRAINING"
    },
            {
    "id": 2,
    "title": "Obstacle",
     "beginAt" : "${today8.toIso8601String()}",
     "endedAt" : "${today8.add(const Duration(minutes: 60)).toIso8601String()}",
     "trainerName": "Ameline",
     "comment" : "Super séance",
      "horseName": "Domino",
      "horseId": 1,
      "eventType": "TRAINING"
    },
    {
    "id": 3,
    "title": "Dressage",
     "beginAt" : "${today1.toIso8601String()}",
     "endedAt" : "${today1.add(const Duration(minutes: 60)).toIso8601String()}",
     "trainerName": "Ameline",
     "comment" : "Super séance",
      "horseName": "Devil",
      "horseId": 2,
      "eventType": "TRAINING"
    },
    {
    "id": 4,
    "place": "Pres",
     "createdAt" : "${today_1.toIso8601String()}",
     "endedAt" : "${today_1.add(const Duration(minutes: 60)).toIso8601String()}",
     "trainerName": "Ameline",
     "comment" : "Super séance",
      "horseName": "Devil",
      "horseId": 2,
      "eventType": "OUTING"
    },
    {
    "id": 5,
    "place": "Pres",
     "createdAt" : "${today4.toIso8601String()}",
     "endedAt" : "${today4.add(const Duration(minutes: 60)).toIso8601String()}",
     "trainerName": "Ameline",
     "comment" : "Super séance",
      "horseName": "Devil",
      "horseId": 2,
      "eventType": "OUTING"
    },
    {
    "id": 6,
    "place": "Pres",
     "createdAt" : "${today4.toIso8601String()}",
     "endedAt" : "${today4.add(const Duration(minutes: 60)).toIso8601String()}",
     "trainerName": "Ameline",
     "comment" : "Super séance",
      "horseName": "Devil",
      "horseId": 2,
      "eventType": "OUTING"
    }
    ]''';

  static var trackedCare = '''{
  "vaccine": {
    "id": 2,
    "name": "Vaccin",
    "doneOn": "2021-06-10T18:25:43.511Z",
    "healerName": "Toto",
    "note": "Super",
    "deadlineWarning": "GOOD",
    "horseName": "Domino",
    "horseId": 1,
    "careType": "VACCINE" 
   },
   "shoeing": {
    "id": 2,
    "name": "Ferrure",
    "doneOn": "2021-06-10T18:25:43.511Z",
    "healerName": "Toto",
    "note": "Super",
    "deadlineWarning": "GOOD",
    "horseName": "Domino",
    "horseId": 1,
    "careType": "SHOEING" 
   },
   "vermifuge": {
    "id": 2,
    "name": "Vermifuge",
    "doneOn": "2021-06-10T18:25:43.511Z",
    "healerName": "Toto",
    "note": "Super",
    "deadlineWarning": "GOOD",
    "horseName": "Domino",
    "horseId": 1,
    "careType": "WORMER"
   }
  }''';

  static var sharings = '''[
       {
       "beginAt": "2021-06-10T18:25:43.511Z",
       "endedAt": "2022-06-10T18:25:43.511Z",
       "horseId": 1,
       "userId": 1,
       "sharedUserId": 2,
       "sharedUserName": "Polo",
       "pictureProfile": {
          "offset": "0.6768126668962222",
          "colors": [{
              "r": 55,
              "g": 112,
              "b": 193
              },
              {
              "r": 0,
              "g": 0,
              "b": 0
              }
              ]
          }
       },
       {
       "beginAt": "2021-06-10T18:25:43.511Z",
       "endedAt": "2022-06-10T18:25:43.511Z",
       "horseId": 1,
       "userId": 1,
       "sharedUserId": 4,
       "sharedUserName": "Sihem",
       "profilePicture":
       "pictureProfile": {
          "offset": "0.5464324244597535",
          "colors": [{
              "r": 243,
              "g": 67,
              "b": 173
              },
              {
              "r": 143,
              "g": 84,
              "b": 218
              }
              ]
          },
       "sharedCalendar": [
           {"date": "${today2.toIso8601String()}"},
           {"date": "${today4.toIso8601String()}"}
           ]
       }      
       ]''';
}
