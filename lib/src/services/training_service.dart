import 'dart:async';

import 'package:equido_flutter/src/models/event/training.dart';
import 'package:equido_flutter/src/models/event_output/training_output.dart';
import 'package:equido_flutter/src/repository/training_repository.dart';

class TrainingService {
  /// Create training
  ///
  static Future create(TrainingOutput training) async {
    var response = await TrainingRepository.create(training);
    return Training.fromJson(response.first.toColumnMap());
  }

  /// Delete training
  static Future<Training> delete(int trainingId) async {
    var response = await TrainingRepository.delete(trainingId);
    return Training.fromJson(response.first.toColumnMap());
  }
}
