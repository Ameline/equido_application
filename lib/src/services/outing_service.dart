import 'dart:async';

import 'package:equido_flutter/src/models/event/outing.dart';
import 'package:equido_flutter/src/models/event_output/outing_output.dart';
import 'package:equido_flutter/src/repository/outing_repository.dart';

class OutingService {
  /// Create outing
  ///
  static Future<Outing> create(OutingOutput outing) async {
    var response = await OutingRepository.create(outing);

    return Outing.fromJson(response.first.toColumnMap());
  }

  /// Delete outing
  ///
  static Future delete(int outingId) async {
    await OutingRepository.delete(outingId);
  }
}
