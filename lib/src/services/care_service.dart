import 'dart:async';

import 'package:equido_flutter/src/models/event/care.dart';
import 'package:equido_flutter/src/models/event_output/care_output.dart';
import 'package:equido_flutter/src/repository/care_repository.dart';

class CareService {
  static Future<Care> create(CareOutput careOutput) async {
    var result = await CareRepository.create(careOutput);
    return Care.fromJson(result.first.toColumnMap());
  }

  static Future<Care> delete(int careId) async {
    var result = await CareRepository.delete(careId);
    return Care.fromJson(result.first.toColumnMap());
  }
}
