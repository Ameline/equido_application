import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/user/user.dart' as equido_user;
import 'package:equido_flutter/src/repository/outing_repository.dart';
import 'package:equido_flutter/src/repository/training_repository.dart';
import 'package:equido_flutter/src/repository/user_repository.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:equido_flutter/test_data.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'package:global_configs/global_configs.dart';
import 'package:postgres/postgres.dart';

import '../models/user/user.dart';

class UserService {
  // Les évènements sont récupérer par tranche de 10 jours
  static const int rangeEvent = 10;

  /// Get all user in application
  ///
  static Future<List<EquiUser>> searchUser() async {
    var result = await UserRepository.searchUser();

    return result
        .toList()
        .map((e) => EquiUser.fromJson(e.toColumnMap()))
        .toList();
  }

  /// Get current user.
  ///
  static Future<EquiUser?> getUser() async {
    firebase.User? firebaseUser = firebase.FirebaseAuth.instance.currentUser;

    if (firebaseUser == null || firebaseUser.email == null) {
      // Deconnexion
      return null;
    } else {
      var user = await UserRepository.searchUserByEmail(firebaseUser.email!);
      return equido_user.EquiUser.fromJson(user.docs.first.data());
    }
  }

  /// Get all user in application
  ///
  static Future<EquiUser?> searchUserByEmail(String email) async {
    var result = await UserRepository.searchUserByEmail(email);

    return result.docs.isNotEmpty
        ? EquiUser.fromJson(result.docs.first.data().values.first)
        : null;
  }

  /// Update current user
  ///
  static Future<equido_user.EquiUser> updateUser(EquiUser user) async {
    await UserRepository.updateUser(user);

    return user;
  }

  /// Get horse event between today
  ///
  static Future<List<Event?>?> getHorsesEventsInit(
      List<String> horseIds) async {
    int dayStop = 1 + rangeEvent; // +1 pour inclure le jour courant
    int dayStart = -rangeEvent - rangeEvent;

    DateTime today = DateTime.now();
    DateTime stop = DateTime(today.year, today.month, today.day + dayStop);
    DateTime start = DateTime(today.year, today.month, today.day + dayStart);

    String body;
    if (GlobalConfigs().get("test_mode")) {
      body = TestData.horsesEvents;
      Iterable list = json.decode(body);
      List<Event?> events = list.map((model) => Event.fromJson(model)).toList();
      return events;
    } else {
      return _getAllEvents(horseIds, start, stop);
    }
  }

  /// Get horse event between Date
  ///
  static Future<List<Event>?> getHorsesEventsBefore(
      List<String> horseIds, int pagination) async {
    int dayStop =
        1 - (pagination * rangeEvent); // +1 pour inclure le jour courant
    int dayStart = -rangeEvent - (pagination * rangeEvent);

    DateTime today = DateTime.now();
    DateTime stop = DateTime(today.year, today.month, today.day + dayStop);
    DateTime start = DateTime(today.year, today.month, today.day + dayStart);

    return _getAllEvents(horseIds, start, stop);
  }

  /// Get horse event between Date
  ///
  static Future<List<Event>> getHorsesEventsAfter(
      List<String> horseIds, int pagination) async {
    int dayStop =
        1 + (pagination * rangeEvent); // +1 pour inclure le jour courant
    int dayStart = rangeEvent - (pagination * rangeEvent);

    DateTime today = DateTime.now();
    DateTime stop = DateTime(today.year, today.month, today.day + dayStop);
    DateTime start = DateTime(today.year, today.month, today.day + dayStart);

    return _getAllEvents(horseIds, start, stop);
  }

  static Future<List<Event>> _getAllEvents(
      List<String> horseIds, DateTime dateStart, DateTime datetStop) {
    String stop = EquidoDate.dateTimeToStringWithoutHour(dateStart);
    String start = EquidoDate.dateTimeToStringWithoutHour(datetStop);

    return _toEvent(OutingRepository.getOutings(horseIds, start, stop),
        TrainingRepository.getTrainings(horseIds, start, stop));
  }

  static Future<List<Event>> _toEvent(
      Future<List<Result>> result, Future<List<Result>> result2) {
    return result.then((results) {
      var res = results
          .map((result) => result
              .toList()
              .map((e) => Event.fromJson(e.toColumnMap()))
              .toList())
          .toList()
          .flattened
          .toList();

      result2.then((results2) => res.addAll(results2
          .map((result) => result
              .toList()
              .map((e) => Event.fromJson(e.toColumnMap()))
              .toList())
          .toList()
          .flattened
          .toList()));

      return res;
    });
  }
}
