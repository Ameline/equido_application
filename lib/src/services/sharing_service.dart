import 'dart:async';
import 'dart:convert';

import 'package:equido_flutter/src/models/user/sharing.dart';
import 'package:equido_flutter/src/models/user/sharing_update.dart';
import 'package:equido_flutter/src/repository/sharing_repository.dart';
import 'package:global_configs/global_configs.dart';

import '../../test_data.dart';

class SharingService {
  /// Update sharing calendar
  ///
  static Future<List<SharingUpdate>> updateSharing(
      List<SharingUpdate> sharingUpdates) async {
    var result = await SharingRepository.updateSharing(sharingUpdates);
// TODO important
    //Iterable list = json.decode(response.f);
    return SharingUpdate.fromJson(List.empty());
  }

  /// Get List of user shared this horse
  ///
  static Future<List<Sharing>> getSharing(String horseId) async {
    if (GlobalConfigs().get("test_mode")) {
      String body = TestData.sharings;
      Iterable list = json.decode(body);
      return list.map((model) => Sharing.fromJson(model)).toList();
    } else {
      var result = await SharingRepository.getSharing(horseId);
      return result
          .toList()
          .map((e) => Sharing.fromJson(e.toColumnMap()))
          .toList();
    }
  }
}
