import 'dart:async';

import 'package:equido_flutter/src/models/event/care.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/repository/horse_repository.dart';

import '../models/horse/tracked_care.dart';

class HorseService {
  /// Get all Horses for current user
  ///
  static Future<List<Horse>> getAllHorsesForCurrentUser() async {
    var response = await HorseRepository.getAllForCurrentUser();

    return response.docs.map((e) => Horse.fromJson(e.data())).toList();
  }

  /// Get Horse whith horseId
  ///
  static Future<Horse> get(String horseId) async {
    var result = await HorseRepository.get(horseId);
    return Horse.fromJson(result.data()?.values.first);
  }

  /// Create horse
  ///
  /// return Horse created
  static Future<Horse> create(Horse horse) async {
    await HorseRepository.create(horse);
    return horse;
  }

  /// Update horse
  ///
  static Future<Horse> update(Horse horse) async {
    var result = await HorseRepository.update(horse);
    return Horse.fromJson(result.first.toColumnMap());
  }

  /// Delete horse
  static Future delete(int horseId) async {
    await HorseRepository.delete(horseId);
  }

  /// Get List of user shared this horse
  ///
  static Future<Horse> getTrackedCare(Horse horse) async {
    Horse update = Horse.copy(horse);

    update.vaccine =
        await _updateTrackedCare(horse.id, horse.vaccine, CareType.vaccine);
    update.shoeing =
        await _updateTrackedCare(horse.id, horse.shoeing, CareType.shoeing);
    update.wormer =
        await _updateTrackedCare(horse.id, horse.wormer, CareType.wormer);

    return update;
  }

  static _updateTrackedCare(
      String horseId, TrackedCare lastTrackedCare, CareType careType) async {
    // Get the last tracked care if exist
    var lastVaccineResult =
        await HorseRepository.getLastCare(horseId, careType);
    Care? lastVaccine = lastVaccineResult
        .toList()
        .map((e) => Care.fromJson(e.toColumnMap()))
        .toList()
        .firstOrNull;

    // Get the next tracked care if exist
    var nextVaccineResult =
        await HorseRepository.getLastCare(horseId, careType);
    Care? nextVaccine = nextVaccineResult
        .toList()
        .map((e) => Care.fromJson(e.toColumnMap()))
        .toList()
        .firstOrNull;

    // Update tracked care
    return TrackedCare.update(lastTrackedCare, lastVaccine, nextVaccine);
  }
}
