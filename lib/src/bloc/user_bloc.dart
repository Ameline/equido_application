import 'dart:async';

import 'package:equido_flutter/src/models/user/user.dart';
import 'package:equido_flutter/src/services/user_service.dart';
import 'package:equido_flutter/src/storage/storage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum UserBlocType { updateUserPictureProfile }

class UserBlocEvent<T> {
  UserBlocType eventType;
  T object;

  UserBlocEvent({required this.eventType, required this.object});
}

class UserBloc extends BlocBase {
  StreamController<UserBlocEvent> actionEventsController =
      StreamController<UserBlocEvent>.broadcast();

  late EquiUser user;
  StreamController<EquiUser> userController =
      StreamController<EquiUser>.broadcast();

  Stream<EquiUser> get getUser => userController.stream;

  UserBloc(super.state) {
    actionEventsController.stream.listen(_buildEvents);
  }

  void _buildEvents(UserBlocEvent blocEvent) async {
    switch (blocEvent.eventType) {
      case UserBlocType.updateUserPictureProfile:
        EquiUser newUser = blocEvent.object;
        await UserService.updateUser(newUser);

        user = newUser;
        userController.add(user);

        break;
    }
  }

  Future<bool> getAllData() async {
    await getUserData();
    await Storage.storeCurrentUserID(user.id);
    return true;
  }

  Future<void> getUserData() async {
    EquiUser? user = await UserService.getUser();

    if (user != null) {
      this.user = user;
      userController.add(this.user);
    }
  }

  //@override
  void dispose() {
    actionEventsController.close();
    actionEventsController.close();
  }
}
