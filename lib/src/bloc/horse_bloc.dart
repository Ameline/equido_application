import 'dart:async';

import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/models/user/sharing.dart';
import 'package:equido_flutter/src/models/user/sharing_update.dart';
import 'package:equido_flutter/src/repository/horse_repository.dart';
import 'package:equido_flutter/src/services/horse_service.dart';
import 'package:equido_flutter/src/services/sharing_service.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum HorseBlocType {
  addHorse,
  updateHorse,
  deleteHorse,
  refreshCurrentHorse,
  updateSharing
}

class HorseBlocEvent<T> {
  HorseBlocType eventType;
  T object;

  HorseBlocEvent({required this.eventType, required this.object});
}

class HorseBloc extends BlocBase {
  StreamController<HorseBlocEvent> actionEventsController =
      StreamController<HorseBlocEvent>.broadcast();

  late List<Horse> horses;
  StreamController<List<Horse>> horsesController =
      StreamController<List<Horse>>.broadcast();

  Stream<List<Horse>> get getHorses => horsesController.stream;

  HorseBloc(super.state) {
    actionEventsController.stream.listen(_buildEvents);
    horses = [];
  }

  void _buildEvents(HorseBlocEvent blocEvent) async {
    switch (blocEvent.eventType) {
      case HorseBlocType.addHorse:
        Horse horse = await HorseService.create(blocEvent.object);

        // TODO
        //List<Sharing> sharings = await SharingService.getSharing(horse.id);
        //horse.sharing = sharings;

        horses.add(horse);
        horsesController.add(horses);

        // Todo mettre a jour EventLog
        // Update EventLog to see new horse message
        //getEventLog();

        break;
      case HorseBlocType.updateHorse:
        Horse horse = getHorse(blocEvent.object.id);
        Horse newHorse = await HorseService.update(blocEvent.object);

        if (_isTrackingCareChanged(horse, newHorse)) {
          newHorse = await HorseService.getTrackedCare(horse);
        }

        horses.remove(horse);
        horses.add(newHorse);
        horsesController.add(horses);

        break;

      case HorseBlocType.updateSharing:
        Horse horse = getHorse(blocEvent.object.id);
        Horse newHorse = horse;

        // Here sharing list contains update and removed sharing like this :
        // [added, remove, added, remove, added, remove, ...]

        List<SharingUpdate> updates = [];
        int index = 0;

        while (blocEvent.object.sharing.length > index) {
          Sharing addedSharing = blocEvent.object.sharing[index];
          Sharing removedSharing = blocEvent.object.sharing[index + 1];

          updates.add(SharingUpdate(
              horseId: addedSharing.horseId,
              userId: addedSharing.userId,
              userSharedId: addedSharing.userSharedId,
              userSharedName: addedSharing.userSharedName,
              addedDays: addedSharing.sharedDays,
              removedDays: removedSharing.sharedDays));

          index = index + 2;
        }

        // TODO important
        List<SharingUpdate> result =
            await SharingService.updateSharing(updates);

        // Update all sharing day for all sharing with the result
        for (var update in updates) {
          Sharing sharing = Sharing.copy(horse.sharing
              .where((s) => s.userSharedId == update.userSharedId)
              .first);
          sharing.sharedDays = update.addedDays;

          newHorse.sharing
              .removeWhere((s) => s.userSharedId == update.userSharedId);

          newHorse.sharing.add(sharing);
        }

        horses.remove(horse);
        horses.add(newHorse);
        horsesController.add(horses);

        break;
      case HorseBlocType.deleteHorse:
        HorseRepository.delete(blocEvent.object.id);
        horses.remove(blocEvent.object);
        horsesController.add(horses);
        break;

      case HorseBlocType.refreshCurrentHorse:
        Horse horse = getHorse(blocEvent.object);
        Horse newHorse = await HorseService.getTrackedCare(horse);

        horses.remove(horse);
        horses.add(newHorse);
        horsesController.add(horses);
        break;
    }
  }

  Future<bool> getAllData() async {
    await getHorsesData();
    return true;
  }

  Horse getHorse(String id) {
    return horses.where((Horse h) => h.id == id).first;
  }

  Future<List<Horse>> getHorsesData() async {
    List<Horse> horses = await HorseService.getAllHorsesForCurrentUser();

    List<Horse> horseWithCommonCare = [];
    for (Horse horse in horses) {
      Horse h = await HorseService.getTrackedCare(horse);
      horseWithCommonCare.add(h);
    }

    this.horses = horseWithCommonCare;
    horsesController.add(horseWithCommonCare);

    return horseWithCommonCare;
  }

  void dispose() {
    actionEventsController.close();
    horsesController.close();
  }

  bool _isTrackingCareChanged(Horse horse, Horse updated) {
    return horse.wormer.frequency != updated.wormer.frequency ||
        horse.shoeing.frequency != updated.shoeing.frequency ||
        horse.vaccine.frequency != updated.vaccine.frequency;
  }
}
