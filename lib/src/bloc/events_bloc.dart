import 'dart:async';

import 'package:equido_flutter/src/models/event/care.dart';
import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/src/models/event/outing.dart';
import 'package:equido_flutter/src/models/event/training.dart';
import 'package:equido_flutter/src/models/event_output/care_output.dart';
import 'package:equido_flutter/src/models/event_output/outing_output.dart';
import 'package:equido_flutter/src/models/event_output/training_output.dart';
import 'package:equido_flutter/src/repository/care_repository.dart';
import 'package:equido_flutter/src/repository/outing_repository.dart';
import 'package:equido_flutter/src/repository/training_repository.dart';
import 'package:equido_flutter/src/services/care_service.dart';
import 'package:equido_flutter/src/services/outing_service.dart';
import 'package:equido_flutter/src/services/training_service.dart';
import 'package:equido_flutter/src/services/user_service.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum EventBlocType { deleteEvent, addTraining, addOuting, addCare }

class EventBlocEvent<T> {
  EventBlocType eventType;
  T object;

  EventBlocEvent({required this.eventType, required this.object});
}

class EventBloc extends BlocBase {
  StreamController<EventBlocEvent> actionEventsController =
      StreamController<EventBlocEvent>.broadcast();

  late List<Event> events;
  int eventsPaginationBefore = 0;
  int eventsPaginationNext = 0;
  StreamController<List<Event>> eventsController =
      StreamController<List<Event>>.broadcast();

  Stream<List<Event>> get getEvents => eventsController.stream;

  EventBloc(super.state) {
    actionEventsController.stream.listen(_buildEvents);

    events = [];
  }

  void _buildEvents(EventBlocEvent blocEvent) async {
    switch (blocEvent.eventType) {
      case EventBlocType.addTraining:
        TrainingOutput trainingOutput = blocEvent.object;
        Training training = await TrainingService.create(trainingOutput);

        events.add(training);
        eventsController.add(events);
        break;

      case EventBlocType.addOuting:
        OutingOutput outingOutput = blocEvent.object;
        Outing outing = await OutingService.create(outingOutput);

        events.add(outing);
        eventsController.add(events);

        break;
      case EventBlocType.addCare:
        CareOutput careOutput = blocEvent.object;
        Care care = await CareService.create(careOutput);

        events.add(care);
        eventsController.add(events);
        break;

      case EventBlocType.deleteEvent:
        if (blocEvent.object.eventType == EventType.training) {
          TrainingRepository.delete(blocEvent.object.id);
        } else if (blocEvent.object.eventType == EventType.outing) {
          OutingRepository.delete(blocEvent.object.id);
        } else if (blocEvent.object.eventType == EventType.care) {
          CareRepository.delete(blocEvent.object.id);
        }

        Event event =
            events.where((Event e) => e.id == blocEvent.object.id).first;
        events.remove(event);
        eventsController.add(events);
        break;
    }
  }

  Future<bool> getAllData(List<String> horseIds) async {
    await getEventDataFromToday(horseIds);
    return true;
  }

  Future<List<Event?>> getEventDataFromToday(List<String> horseIds) async {
    // init pagination before and after today
    eventsPaginationBefore = 0;
    eventsPaginationNext = 0;

    var li = await UserService.getHorsesEventsInit(horseIds);
    events = li!.map((e) => e!).toList();
    eventsController.add(events);

    return events;
  }

  Future<List<Event>> getBeforePageEvent(List<String> horseIds) async {
    eventsPaginationBefore = eventsPaginationBefore + 1;
    var li = await UserService.getHorsesEventsBefore(
        horseIds, eventsPaginationBefore);

    events = events + li!;
    eventsController.add(events);

    return events;
  }

  Future<List<Event>> getNextPageEvent(List<String> horseIds) async {
    eventsPaginationNext = eventsPaginationNext + 1;
    var li =
        await UserService.getHorsesEventsBefore(horseIds, eventsPaginationNext);

    events = events + li!;
    eventsController.add(events);

    return events;
  }

  //@override
  void dispose() {
    actionEventsController.close();
    eventsController.close();
  }
}
