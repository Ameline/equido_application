import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equido_flutter/src/services/user_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:uuid/uuid.dart';

import '../models/user/user.dart';

class GoogleAuthService {
  // Instances Firebase
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<void> signInWithGoogle() async {
    // Démarrer le processus de connexion
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();
    // Obtenir les détails d'authentification de la requête
    final GoogleSignInAuthentication googleAuth =
        await googleUser!.authentication;
    // Créer un nouveau identifiant pour l'utilisateur
    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    // Se connecter avec les identifiants
    UserCredential result = await _auth.signInWithCredential(credential);

    // Si l'utilisateur est nouveau, ajouter ses informations à Firestore
    var userUUID = _auth.currentUser!.uid;
    var userAlreadyExist =
        await UserService.searchUserByEmail(googleUser.email);
    if (userAlreadyExist == null) {
      var equiUser = EquiUser.fromGoogle(userUUID, googleUser);
      await _addGoogleUser(equiUser);
    }
  }

  // Todo mettre dans userRepo
  Future<void> _addGoogleUser(EquiUser user) {
    return _firestore
        .collection('equiUser')
        .doc(Uuid().v4())
        .set(user.toMap())
        .then((value) => print('Utilisateur ajouté'))
        .catchError((error) => print('Erreur : $error'));
  }
}
