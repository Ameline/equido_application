import 'dart:developer' as developer;

import 'package:firebase_auth/firebase_auth.dart';

class AuthentificationService {
  Future<String> createUser(String email, String username, String password) async {
    return FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password)
        .then((value) => null)
        .then((value) {
      return "connected";
    }).onError((error, stackTrace) {
      developer.log(
        '',
        name: 'equido.AuthenticationService.login',
        error: "Error connexion ${error.toString()}",
      );
      return "error";
    });

    /* ProfilePicture randomPictureProfile = ProfilePicture.generateRandom(username);
    var user = {
      "username": username,
      "email": mail,
      "password": password,
      "gradientOffset": randomPictureProfile.offset.toString(),
      "colors": randomPictureProfile.colors.map((e) => "${e.red} ${e.green} ${e.blue}").join(",")
    };

    Response response = await AuthentificationAPI.createUser(user);
    return response.statusCode.toString();

    */
  }

  signOut() async {
    FirebaseAuth.instance.signOut();
  }

  static Future<String?> login(String email, String password) async {
    return FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password).then((value) => null).then((value) {
      return "connected";
    }).onError((error, stackTrace) {
      developer.log(
        '',
        name: 'equido.AuthenticationService.login',
        error: "Error connexion ${error.toString()}",
      );
      return "error";
    });
  }

  static Future<void> forgotPassword(String email) async {
    return FirebaseAuth.instance.sendPasswordResetEmail(email: email);
  }
}
