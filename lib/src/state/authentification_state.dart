enum EState { initial, authenticated, failed, signedOut, serverError, signUp }

class AuthenticationState {
  EState state = EState.initial;

  AuthenticationState.initial({this.state = EState.initial});

  AuthenticationState.signUp({this.state = EState.signUp});

  AuthenticationState.authenticated({this.state = EState.authenticated});

  AuthenticationState.failed({this.state = EState.failed});

  AuthenticationState.signedOut({this.state = EState.signedOut});

  AuthenticationState.serverError({this.state = EState.serverError});
}
