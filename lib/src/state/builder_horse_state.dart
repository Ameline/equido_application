import 'package:equido_flutter/src/bloc/events_bloc.dart';
import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/bloc/user_bloc.dart';
import 'package:equido_flutter/ui/connection/view/loading_page.dart';
import 'package:equido_flutter/ui/main_bottom_tab.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BuilderHorse extends StatefulWidget {
  const BuilderHorse({super.key});

  @override
  createState() => _BuilderHorseState();
}

class _BuilderHorseState extends State<BuilderHorse> {
  bool initData = false;

  loadData(BuildContext context) async {
    final horsesBloc = BlocProvider.of<HorseBloc>(context);
    final userBloc = BlocProvider.of<UserBloc>(context);
    final eventsBloc = BlocProvider.of<EventBloc>(context);

    userBloc.getAllData().then((user) => horsesBloc
        .getAllData()
        .then((horses) => eventsBloc.getAllData(horsesBloc.horses.map((h) => h.id).toList()).then((value) => setState(() {
              initData = true;
            }))));

    setState(() {
      initData = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!initData) {
      loadData(context);
      return const LoadingPage();
    }

    return const BottomTab();
  }
}
