import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

class EquidoLocalizations {
  static EquidoLocalizations of(BuildContext context) {
    return Localizations.of<EquidoLocalizations>(context, EquidoLocalizations)
        as EquidoLocalizations;
  }

  String getText(String key) {
    String text = language[key];
    return text;
  }

  String getTextParameter(String key, String parameter) {
    return language[key].toString().replaceFirst("{}", parameter);
  }
}

late Map<String, dynamic> language;

class EquidoLocalizationsDelegate
    extends LocalizationsDelegate<EquidoLocalizations> {
  const EquidoLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['fr'].contains(locale.languageCode);

  @override
  Future<EquidoLocalizations> load(Locale locale) async {
    String string = await rootBundle
        .loadString("assets/string/string_${locale.languageCode}.json");
    language = json.decode(string);
    return SynchronousFuture<EquidoLocalizations>(EquidoLocalizations());
  }

  @override
  bool shouldReload(EquidoLocalizationsDelegate old) => false;
}
