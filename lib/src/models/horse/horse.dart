import 'package:equido_flutter/src/models/horse/tracked_care.dart';
import 'package:equido_flutter/src/models/user/sharing.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:uuid/uuid.dart';

import '../../utils/equido_utils.dart';

class Horse {
  static String defaultId = Uuid().v4();
  static const int dId = -1;

  final String id;
  String name;
  int? size;
  String kind;
  String color;
  String breed;
  DateTime? birthDate;
  String ownerName;
  String ownerId;

  TrackedCare shoeing;
  TrackedCare wormer;
  TrackedCare vaccine;

  List<Sharing> sharing;

  static const String idKey = 'id';
  static const String nameKey = 'name';
  static const String sizeKey = 'size';
  static const String kindKey = 'kind';
  static const String colorKey = 'color';
  static const String breedKey = 'breed';
  static const String ownerIdKey = 'owner_id';
  static const String birthDateKey = 'birth_date';
  static const String shoeingFrequencyKey = 'shoeing_frequency';
  static const String wormerFrequencyKey = 'wormer_frequency';
  static const String vaccinFrequencyKey = 'vaccine_frequency';
  static const String sharingKey = 'sharing';

  Horse(
      {required this.id,
      required this.name,
      required this.size,
      required this.kind,
      required this.color,
      required this.breed,
      this.birthDate,
      required this.ownerName,
      required this.ownerId,
      required this.shoeing,
      required this.vaccine,
      required this.wormer,
      required this.sharing});

  Horse.copy(Horse horse)
      : id = horse.id,
        name = horse.name,
        size = horse.size,
        kind = horse.kind,
        color = horse.color,
        breed = horse.breed,
        birthDate = horse.birthDate,
        ownerName = horse.ownerName,
        ownerId = horse.ownerId,
        vaccine = TrackedCare.copy(horse.vaccine),
        shoeing = TrackedCare.copy(horse.shoeing),
        wormer = TrackedCare.copy(horse.wormer),
        sharing = horse.sharing;

  factory Horse.fromJson(Map<String, dynamic> parsedJson) {
    List<Sharing> sharing = parsedJson[sharingKey] != null
        ? List<Sharing>.from(
            parsedJson[sharingKey].map((model) => Sharing.fromJson(model)))
        : List.empty();

    var horseName = parsedJson[nameKey];

    return Horse(
        id: parsedJson[idKey],
        name: horseName,
        size: parsedJson[sizeKey] ?? EquidoUtils.nR,
        kind: parsedJson[kindKey] ?? EquidoUtils.nR,
        color: parsedJson[colorKey] ?? EquidoUtils.nR,
        breed: parsedJson[breedKey] ?? EquidoUtils.nR,
        ownerName: "todo",
        // TODO parsedJson['ownerName'],
        ownerId: parsedJson[ownerIdKey],
        birthDate: parsedJson[birthDateKey] != null
            ? DateTime.parse(parsedJson[birthDateKey])
            : null,
        shoeing: TrackedCare.defaultshoeing(
            horseName, parsedJson[shoeingFrequencyKey]),
        vaccine: TrackedCare.defaultWormer(
            horseName, parsedJson[wormerFrequencyKey]),
        wormer: TrackedCare.defaultVaccine(
            horseName, parsedJson[vaccinFrequencyKey]),
        sharing: sharing);
  }

  factory Horse.fromJson2(Map<String, dynamic> parsedJson) {
    List<Sharing> sharing = parsedJson[sharingKey] != null
        ? List<Sharing>.from(
            parsedJson[sharingKey].map((model) => Sharing.fromJson(model)))
        : List.empty();

    var horseName = parsedJson[nameKey];

    return Horse(
        id: parsedJson[idKey],
        name: horseName,
        size: parsedJson[sizeKey] ?? EquidoUtils.nR,
        kind: parsedJson[kindKey] ?? EquidoUtils.nR,
        color: parsedJson[colorKey] ?? EquidoUtils.nR,
        breed: parsedJson[breedKey] ?? EquidoUtils.nR,
        ownerName: "todo",
        // TODO parsedJson['ownerName'],
        ownerId: parsedJson[ownerIdKey],
        birthDate: parsedJson[birthDateKey] != null
            ? DateTime.parse(parsedJson[birthDateKey])
            : null,
        shoeing: TrackedCare.defaultshoeing(
            horseName, parsedJson[shoeingFrequencyKey]),
        vaccine: TrackedCare.defaultWormer(
            horseName, parsedJson[wormerFrequencyKey]),
        wormer: TrackedCare.defaultVaccine(
            horseName, parsedJson[vaccinFrequencyKey]),
        sharing: sharing);
  }

  factory Horse.create(
      {required String name,
      required int? size,
      required String kind,
      required String color,
      required String breed,
      required DateTime? birthDate}) {
    return Horse(
        id: defaultId,
        name: name,
        size: size,
        kind: kind,
        color: color,
        breed: breed,
        ownerName: "",
        ownerId: defaultId,
        birthDate: birthDate,
        shoeing: TrackedCare.defaultshoeing(name, null),
        vaccine: TrackedCare.defaultVaccine(name, null),
        wormer: TrackedCare.defaultWormer(name, null),
        sharing: List.empty());
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};

    map[nameKey] = name;
    map[sizeKey] = size ?? 0;
    if (birthDate != null) {
      map[birthDateKey] = EquidoDate.dateTimeToString(birthDate!.toUtc());
    }
    map[shoeingFrequencyKey] = shoeing.frequency;
    map[wormerFrequencyKey] = wormer.frequency;
    map[vaccinFrequencyKey] = vaccine.frequency;

    if (kind != EquidoUtils.nR) {
      map[kindKey] = kind;
    }
    if (color != EquidoUtils.nR) {
      map[colorKey] = color;
    }
    if (breed != EquidoUtils.nR) {
      map[breedKey] = breed;
    }

    return map;
  }

  Map toJson() {
    return {
      idKey: id,
      nameKey: name,
      sizeKey: size,
      kindKey: kind,
      colorKey: color,
      breedKey: breed,
      birthDateKey: birthDate,
      'ownerName': ownerName,
      ownerIdKey: ownerId,
      vaccinFrequencyKey: vaccine.frequency,
      shoeingFrequencyKey: shoeing.frequency,
      wormerFrequencyKey: wormer.frequency
    };
  }

  String getBirthDateFormatted() {
    if (birthDate != null) {
      return EquidoDate.formatYear(birthDate!);
    }

    return EquidoUtils.nR;
  }

  String getYearsOldFormatted() {
    if (birthDate != null) {
      Duration dur = DateTime.now().difference(birthDate!);
      String differenceInYears = (dur.inDays / 365).floor().toString();
      return differenceInYears;
    }

    return EquidoUtils.nR;
  }

  String getDeadlineFormatted(int? deadline) {
    return deadline == null ? EquidoUtils.nR : deadline.toString();
  }

  int getNumberOfSharing() {
    return sharing.where((s) => s.userSharedId != s.userId).length;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Horse &&
          id == other.id &&
          name == other.name &&
          size == other.size &&
          kind == kind &&
          color == color &&
          breed == breed &&
          getBirthDateFormatted() == other.getBirthDateFormatted() &&
          ownerName == other.ownerName;

  @override
  int get hashCode =>
      id.hashCode + name.hashCode + size.hashCode + ownerName.hashCode;
}
