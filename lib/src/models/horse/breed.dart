import '../../utils/equido_utils.dart';

class Breed {
  static List<String> breeds = [
    "Autre",
    "Non renseigné",
    "ONC",
    "Akhal Téké",
    "Albanais",
    "Trotteur",
    "Andalou",
    "Pure race Espagnole",
    "Anglo arabe",
    "Appaloosa",
    "AQPS",
    "Arabe",
    "Barbe",
    "Percheron",
    "Islandais",
    "Holsteiner",
    "Henson",
    "Criollo",
    "Comptois",
    "Boulonnais",
    "Halfinger",
    "Lusitanien",
    "Frison",
    "Paint horse",
    "Quarter horse",
    "KWPN",
    "Tenessee Walker",
    "Welsh",
    "Lipizzan",
    "Mérens",
    "Fjord",
    "Hanovrien",
    "Camargue",
    "Connemara",
    "Trotteur francais",
    "Mustang",
    "Shetland",
    "Selle francais"
  ];

  static String defaultBreed() {
    return EquidoUtils.nR;
  }
}
