import 'package:equido_flutter/src/models/event/care.dart';

enum StatusTrackedCare { ok, soon, delay, unknow, unknowFrequency }

class TrackedCare {
  CareType careType;
  StatusTrackedCare status;

  Care? lastCare;
  int? frequency;
  int? dayBeforeDeadline;
  DateTime? dateDeadline;
  Care? nextCare;
  String horseName;

  TrackedCare(
      {this.lastCare,
      required this.frequency,
      required this.careType,
      required this.status,
      this.dayBeforeDeadline,
      this.dateDeadline,
      required this.horseName,
      this.nextCare});

  factory TrackedCare.update(
      TrackedCare previous, Care? lastCare, Care? nextCare) {
    DateTime? dateDeadline = lastCare != null && previous.frequency != null
        ? lastCare.start.add(Duration(days: previous.frequency ?? 0))
        : null;
    int? dayBeforeDeadline = dateDeadline?.difference(DateTime.now()).inDays;

    return TrackedCare(
        careType: previous.careType,
        lastCare: lastCare,
        frequency: previous.frequency,
        dayBeforeDeadline: dayBeforeDeadline,
        status: TrackedCare.setStatus(dayBeforeDeadline, previous.frequency),
        dateDeadline: dateDeadline,
        horseName: previous.horseName,
        nextCare: nextCare);
  }

  factory TrackedCare.fromJson(CareType type, Map<String, dynamic> parsedJson) {
    Care? lastCare = parsedJson['lastCare'] != null
        ? Care.fromJson(parsedJson['lastCare'])
        : null;

    int? frequency = parsedJson['frequency'] != null
        ? int.parse(parsedJson['frequency'])
        : null;

    if (frequency == 0) {
      frequency = null;
    }

    DateTime? dateDeadline = lastCare != null && frequency != null
        ? lastCare.start.add(Duration(days: frequency))
        : null;

    int? dayBeforeDeadline = dateDeadline?.difference(DateTime.now()).inDays;

    return TrackedCare(
        careType: type,
        lastCare: lastCare,
        frequency: parsedJson['frequency'] != null
            ? int.parse(parsedJson['frequency'])
            : null,
        dayBeforeDeadline: dayBeforeDeadline,
        status: TrackedCare.setStatus(dayBeforeDeadline, frequency),
        dateDeadline: dateDeadline,
        horseName: parsedJson['horseName'],
        nextCare: parsedJson['nextCare'] != null
            ? Care.fromJson(parsedJson['nextCare'])
            : null);
  }

  static Map toJson(TrackedCare trackedCare) {
    return {
      'dateDeadline': trackedCare.dateDeadline,
      'frequency': trackedCare.frequency,
      'careId': trackedCare.lastCare?.id,
    };
  }

  TrackedCare.copy(TrackedCare trackedCare)
      : careType = trackedCare.careType,
        lastCare = trackedCare.lastCare != null
            ? Care.copy(trackedCare.lastCare!)
            : null,
        frequency = trackedCare.frequency,
        status = trackedCare.status,
        horseName = trackedCare.horseName,
        dateDeadline = trackedCare.dateDeadline;

  TrackedCare.defaultshoeing(this.horseName, this.frequency)
      : careType = CareType.shoeing,
        status = StatusTrackedCare.unknow;

  TrackedCare.defaultWormer(this.horseName, this.frequency)
      : careType = CareType.wormer,
        status = StatusTrackedCare.unknow;

  TrackedCare.defaultVaccine(this.horseName, this.frequency)
      : careType = CareType.vaccine,
        status = StatusTrackedCare.unknow;

  static StatusTrackedCare setStatus(int? dayBeforeDeadline, int? frequency) {
    if (frequency == null) {
      return StatusTrackedCare.unknowFrequency;
    }

    if (dayBeforeDeadline == null) {
      return StatusTrackedCare.unknow;
    }
    if (dayBeforeDeadline < 0) {
      return StatusTrackedCare.delay;
    } else if (dayBeforeDeadline <= 10) {
      return StatusTrackedCare.soon;
    } else {
      return StatusTrackedCare.ok;
    }
  }

  static String careTypeToString(CareType type) {
    switch (type) {
      case CareType.shoeing:
        return "Ferrure";
      case CareType.vaccine:
        return "Vaccin";
      case CareType.wormer:
        return "Vermifuge";
      case CareType.other:
        return "Soin";
    }
  }

  static bool careTypeisMasculin(CareType type) {
    switch (type) {
      case CareType.shoeing:
        return false;
      case CareType.vaccine:
        return true;
      case CareType.wormer:
        return true;
      case CareType.other:
        return true;
    }
  }
}
