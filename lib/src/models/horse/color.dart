import '../../utils/equido_utils.dart';

class HorseColor {
  static List<String> colors = [
    "Autre",
    "Non renseigné",
    "Alezan",
    "Alezan brûlé",
    "Alezan crin lavé",
    "Aubere",
    "Appaloosa",
    "Bai",
    "Blanc",
    "Café au lait",
    "Chocolat",
    "Cremello",
    "Noir",
    "Souris",
    "Isabelle",
    "Louvet",
    "Gris",
    "Pie",
    "Rouan",
    "Palomino",
    "Gris pommelé",
    "Souris crins lavés",
    "Noir pangaré",
    "Léopard",
    "Tacheté"
  ];

  static String defaultColor() {
    return EquidoUtils.nR;
  }
}
