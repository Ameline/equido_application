import 'package:equido_flutter/ui/widget/equido_asset.dart';
import 'package:flutter/material.dart';

enum LogoSize { min, normal, max, maxmax }

class Logo {
  static const double _sizeMin = 35;
  static const double _size = 50;
  static const double _sizeMax = 90;
  static const double _sizeMaxMax = 130;

  static double _getSize(LogoSize size) {
    switch (size) {
      case LogoSize.normal:
        return _size;
      case LogoSize.min:
        return _sizeMin;
      case LogoSize.max:
        return _sizeMax;
      case LogoSize.maxmax:
        return _sizeMaxMax;
    }
  }

  static Widget trainingLogo(String assets,
      {LogoSize size = LogoSize.normal, Gradient? gradient}) {
    double width = _getSize(size);
    double height = _getSize(size);

    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        gradient: gradient ??
            const LinearGradient(
              colors: [
                Color.fromRGBO(251, 57, 139, 1),
                Color.fromRGBO(255, 76, 100, 1)
              ],
              begin: FractionalOffset(0.5, 0.0),
              end: FractionalOffset(0.0, 0.5),
            ),
      ),
      child: Image.asset(
        assets,
        width: width,
        height: height,
      ),
    );
  }

  static Image trainingLogoLight(String assets,
      {LogoSize size = LogoSize.normal}) {
    double width = _getSize(size);
    double height = _getSize(size);

    return Image.asset(
      assets,
      width: width,
      height: height,
    );
  }

  static Container outingLogo({LogoSize size = LogoSize.normal}) {
    double width = _getSize(size);
    double height = _getSize(size);

    return Container(
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(161, 240, 66, 1),
            Color.fromRGBO(69, 181, 47, 1)
          ],
          begin: FractionalOffset(0.5, 0.0),
          end: FractionalOffset(0.0, 0.5),
        ),
      ),
      child: Image.asset(
        EquidoAsset.whitePaddockLogo,
        width: width,
        height: height,
      ),
    );
  }

  static Container outingLogoLight({LogoSize size = LogoSize.normal}) {
    double width = _getSize(size);
    double height = _getSize(size);

    return Container(
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
      ),
      child: Image.asset(
        EquidoAsset.blackPaddocLogo,
        width: width,
        height: height,
      ),
    );
  }

  static Widget careLogo(String assets, {LogoSize size = LogoSize.normal}) {
    double width = _getSize(size);
    double height = _getSize(size);

    return Container(
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
        gradient: LinearGradient(
          colors: [Colors.yellow, Colors.deepOrange],
          begin: FractionalOffset(0.5, 0.0),
          end: FractionalOffset(0.0, 0.5),
        ),
      ),
      child: Image.asset(
        assets,
        width: width,
        height: height,
      ),
    );
  }

  static Image careLogoLight(String assets, {LogoSize size = LogoSize.normal}) {
    double width = _getSize(size);
    double height = _getSize(size);

    return Image.asset(assets, width: width, height: height);
  }

  static Widget horseLogo({LogoSize size = LogoSize.normal}) {
    double width = _getSize(size);
    double height = _getSize(size);

    return Container(
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
        gradient: LinearGradient(
            colors: [Colors.cyan, Colors.blue],
            begin: FractionalOffset(0.5, 0.0),
            end: FractionalOffset(0.0, 0.5),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp),
      ),
      child: Image.asset(
        EquidoAsset.whiteEquidoLogo,
        width: width,
        height: height,
      ),
    );
  }
}
