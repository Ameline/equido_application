import '../../utils/equido_utils.dart';

class Kind {
  static List<String> kinds = ["Entier", "Hongre", "Jument", "Non renseigné"];

  static String defaultKind() {
    return EquidoUtils.nR;
  }
}
