import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';

class SharingUpdate {
  String horseId;
  String userId;
  String userSharedId;
  String userSharedName;
  List<DateTime> addedDays;
  List<DateTime> removedDays;

  SharingUpdate(
      {required this.horseId,
      required this.userId,
      required this.userSharedId,
      required this.userSharedName,
      required this.addedDays,
      required this.removedDays});

  static List<SharingUpdate> fromJson(Iterable list) {
    Map<String, List<DateTime>> result = {};

    for (var element in list) {
      result.update(element['sharedUserId'], (value) {
        value.add(DateTime.parse(element['date']));
        return value;
      }, ifAbsent: () => ([DateTime.parse(element['date'])]));
    }

    List<SharingUpdate> update = [];
    result.forEach((key, value) => update.add(SharingUpdate(
        horseId: Horse.defaultId,
        userId: Horse.defaultId,
        userSharedId: key,
        userSharedName: "",
        removedDays: [],
        addedDays: value)));
    return update;
  }

  Map toJson() {
    return {
      'horseId': horseId,
      'userSharedId': userSharedId,
      'userId': userId,
      'addedDays':
          addedDays.map((e) => EquidoDate.dateTimeToString(e)).toList(),
      'removedDays':
          removedDays.map((e) => EquidoDate.dateTimeToString(e)).toList()
    };
  }
}
