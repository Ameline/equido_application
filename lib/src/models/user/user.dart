import 'package:equido_flutter/src/models/user/profile_picture.dart';
import 'package:equido_flutter/src/repository/user_keys.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:uuid/uuid.dart';

class EquiUser {
  final String id;
  String username;
  String? firstName;
  String? lastName;
  String email;
  String? googleId;
  DateTime registrationDate;
  ProfilePicture pictureProfile;

  EquiUser(
      {required this.id,
      this.googleId,
      required this.username,
      this.firstName,
      this.lastName,
      required this.email,
      required this.registrationDate,
      required this.pictureProfile});

  EquiUser.copy(EquiUser user)
      : id = user.id,
        googleId = user.googleId,
        username = user.username,
        firstName = user.firstName,
        lastName = user.lastName,
        email = user.email,
        registrationDate = user.registrationDate,
        pictureProfile = user.pictureProfile;

  factory EquiUser.fromGoogle(String userId, GoogleSignInAccount user) {
    return EquiUser(
        id: Uuid().v4(),
        googleId: userId,
        username: user.displayName.toString(),
        email: user.email,
        registrationDate: DateTime.now(),
        pictureProfile:
            ProfilePicture.generateRandom(user.displayName.toString()));
  }

  factory EquiUser.fromJson(Map<String, dynamic> parsedJson) {
    return EquiUser(
        id: parsedJson[UserKeys.idKey],
        username: parsedJson[UserKeys.usernameKey],
        firstName: parsedJson[UserKeys.firstNameKey],
        lastName: parsedJson[UserKeys.lastNameKey],
        email: parsedJson[UserKeys.emailKey],
        registrationDate:
            DateTime.parse(parsedJson[UserKeys.registrationDateKey]),
        pictureProfile: ProfilePicture.fromJson(
            parsedJson[UserKeys.profilPictureKey],
            parsedJson[UserKeys.usernameKey]));
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    map[UserKeys.idKey] = id;
    map[UserKeys.usernameKey] = username;
    map[UserKeys.firstNameKey] = firstName;
    map[UserKeys.lastNameKey] = lastName;
    map[UserKeys.emailKey] = email;
    map[UserKeys.registrationDateKey] =
        EquidoDate.dateTimeToStringWithoutHour(registrationDate);
    map[UserKeys.profilPictureKey] = pictureProfile.toMap();

    return map;
  }

  Map toJson() {
    var toto = {
      UserKeys.idKey: id,
      UserKeys.usernameKey: username,
      UserKeys.firstNameKey: firstName,
      UserKeys.lastNameKey: lastName,
      UserKeys.emailKey: email,
      'gradientOffset': pictureProfile.offset,
      'colors': pictureProfile.colors
          .map((e) => "${e.r} ${e.g} ${e.b}")
          .join(",") // todo faire une variable pour le patterne
    };
    return toto;
  }
}
