import 'dart:math';

import 'package:flutter/material.dart';

import '../../repository/user_keys.dart';

class ProfilePicture {
  String name;
  double offset;
  List<Color> colors = [];

  ProfilePicture(this.name, this.offset, this.colors);

  factory ProfilePicture.fromJson(
      Map<String, dynamic> parsedJson, String usernameLabel) {
    List<dynamic> tt = parsedJson[UserKeys.profilPictureColorKey];

    return ProfilePicture(
        usernameAbbreviation(usernameLabel),
        parsedJson[UserKeys.profilPictureOffsetKey],
        tt
            .map((parsedJsonColor) => Color.from(
                alpha: parsedJsonColor[UserKeys.profilPictureColorOpacityKey],
                red: parsedJsonColor[UserKeys.profilPictureColorRKey],
                green: parsedJsonColor[UserKeys.profilPictureColorGKey],
                blue: parsedJsonColor[UserKeys.profilPictureColorBKey]))
            .toList());
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    map[UserKeys.profilPictureOffsetKey] = offset;
    map[UserKeys.profilPictureColorKey] = colors.map((c) {
      var m = <String, dynamic>{};
      m[UserKeys.profilPictureColorRKey] = c.r;
      m[UserKeys.profilPictureColorGKey] = c.g;
      m[UserKeys.profilPictureColorBKey] = c.b;
      m[UserKeys.profilPictureColorOpacityKey] = c.a;

      return m;
    });

    return map;
  }

  ProfilePicture.copy(ProfilePicture randomPictureProfile)
      : name = randomPictureProfile.name,
        offset = randomPictureProfile.offset,
        colors = randomPictureProfile.colors;

  static generateRandom(String value) {
    var rng = Random();

    int colorsNumber = rng.nextInt(2) + 2;
    List<Color> colorsRng = generateColor(colorsNumber);
    double offset = generateDouble(0.5, 0.7);

    return ProfilePicture(usernameAbbreviation(value), offset, colorsRng);
  }

  getGradient() {
    return LinearGradient(
      colors: colors,
      begin: FractionalOffset(offset, 0.0),
      end: FractionalOffset(0.0, offset),
    );
  }

  static String usernameAbbreviation(String userName) {
    return userName
        .split(" ")
        .map((e) => e[0])
        .reduce((value, element) => value + element);
  }

  static double generateDouble(double min, double max) {
    var rng = Random();

    double result = rng.nextDouble();

    while (result > max || result < min) {
      result = rng.nextDouble();
    }
    return result;
  }

  static List<Color> generateColor(int colorsNumber) {
    var rng = Random();

    List<Color> generatedColor = List.generate(
        colorsNumber, (index) => colorsList[rng.nextInt(colorsList.length)]);

    while (!notAllSameColor(generatedColor)) {
      generatedColor = List.generate(
          colorsNumber, (index) => colorsList[rng.nextInt(colorsList.length)]);
    }
    return generatedColor;
  }

  static bool notAllSameColor(List<Color> colors) {
    Color first = colors.first;
    return colors.where((element) => element == first).length < 2;
  }

  Widget widget(double size) {
    return Container(
        height: size,
        width: size,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(size / 2)),
            gradient: getGradient()),
        child: Center(
          child: Text(name,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: size / 2,
                  fontWeight: FontWeight.bold)),
        ));
  }

  static nextDouble(double min) {
    var rng = Random();

    double result = rng.nextDouble();
    while (result < min) {
      result = rng.nextDouble();
    }
    return result;
  }

  static List<Color> colorsList = [
    const Color.fromRGBO(85, 220, 242, 1),
    const Color.fromRGBO(243, 67, 173, 1),
    const Color.fromRGBO(0, 0, 0, 1),
    const Color.fromRGBO(55, 112, 193, 1),
    const Color.fromRGBO(12, 199, 230, 1),
    const Color.fromRGBO(143, 84, 218, 1),
  ];
}
