import 'package:equido_flutter/src/models/user/profile_picture.dart';
import 'package:equido_flutter/src/utils/equido_date.dart';

class Sharing {
  DateTime beginAt;
  DateTime endedAt;
  String horseId;
  String userId;
  String userSharedId;
  String userSharedName;
  List<DateTime> sharedDays;
  ProfilePicture sharedPictureProfile;

  Sharing(
      {required this.beginAt,
      required this.endedAt,
      required this.horseId,
      required this.userId,
      required this.userSharedId,
      required this.userSharedName,
      required this.sharedDays,
      required this.sharedPictureProfile});

  factory Sharing.fromJson(Map<String, dynamic> parsedJson) {
    return Sharing(
        beginAt: DateTime.parse(parsedJson['beginAt']),
        endedAt: DateTime.parse(parsedJson['endedAt']),
        horseId: parsedJson['horseId'],
        userId: parsedJson['userId'],
        userSharedId: parsedJson['sharedUserId'],
        userSharedName: parsedJson['sharedUserName'],
        sharedDays: parsedJson['sharedCalendar'] != null
            ? List<DateTime>.from(parsedJson['sharedCalendar']
                .map((model) => _getDate(model['date'])))
            : [],
        sharedPictureProfile: ProfilePicture.fromJson(
            parsedJson['pictureProfile'], 'sharedUserName'));
  }

  static DateTime _getDate(dynamic parse) {
    DateTime date = DateTime.parse(parse);
    return DateTime(date.year, date.month, date.day);
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    map["beginAt"] = EquidoDate.dateTimeToString(beginAt.toUtc());
    map["endedAt"] = EquidoDate.dateTimeToString(endedAt.toUtc());
    map["horseId"] = horseId;
    map["userSharedId"] = userSharedId;

    return map;
  }

  Map<String, dynamic> toMapUpdate(
      List<DateTime> addedDays, List<DateTime> removedDays) {
    var map = <String, dynamic>{};
    map["addedDays"] =
        addedDays.map((e) => EquidoDate.dateTimeToString(e)).toList();
    map["removedDays"] =
        removedDays.map((e) => EquidoDate.dateTimeToString(e)).toList();
    map["horseId"] = horseId;
    map["userSharedId"] = userSharedId;
    map["userId"] = userId;

    return map;
  }

  Map toJson() {
    return {
      'beginAt': beginAt,
      'endedAt': endedAt,
      'horseId': horseId,
      'userSharedId': userSharedId,
      'userSharedName': userSharedName
    };
  }

  Sharing.copy(Sharing sharing)
      : beginAt = sharing.beginAt,
        endedAt = sharing.endedAt,
        horseId = sharing.horseId,
        userId = sharing.userId,
        userSharedId = sharing.userSharedId,
        userSharedName = sharing.userSharedName,
        sharedDays = sharing.sharedDays,
        sharedPictureProfile = sharing.sharedPictureProfile;
}
