import 'package:equido_flutter/src/models/event/event.dart';

enum DateStatus { ok, tooLong, tooShort, badOrder }

class EventOutput {
  late int id;
  late String horseId;
  late EventType eventType;

  EventOutput(this.id, this.horseId, this.eventType);
}
