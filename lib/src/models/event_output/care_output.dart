import 'package:equido_flutter/src/models/event/care.dart';
import 'package:equido_flutter/src/models/event/event.dart';

import '../../utils/equido_date.dart';
import 'event_output.dart';

class CareOutput extends EventOutput {
  String? comment;
  late DateTime doneOn;
  late String title;
  late CareType careType;

  CareOutput(
      {this.comment,
      required this.doneOn,
      required String horseId,
      required int id,
      required this.title,
      required this.careType})
      : super(id, horseId, EventType.care);

  Map toMap() {
    var map = <String, dynamic>{};
    map["title"] = title;
    map["doneOn"] = EquidoDate.dateTimeToString(doneOn.toUtc());
    map["comment"] = comment;
    map["type"] = careType.toString().split('.').last.toUpperCase();

    return map;
  }

  getTypeFromTitle(String title) {
    if (title == "Vaccin") {
      return CareType.vaccine;
    } else if (title == "Vermifuge") {
      return CareType.wormer;
    } else if (title == "Ferrure") {
      return CareType.shoeing;
    }
    return CareType.other;
  }
}
