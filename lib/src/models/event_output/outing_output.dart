import 'package:equido_flutter/src/models/event/event.dart';

import '../../utils/equido_date.dart';
import 'event_output.dart';

class OutingOutput extends EventOutput {
  late DateTime beginAt;
  late DateTime endedAt;
  late String title;
  late String place;

  OutingOutput(
      {required this.beginAt,
      required this.endedAt,
      required String horseId,
      required int id,
      required this.title,
      required this.place})
      : super(id, horseId, EventType.outing);

  DateStatus verifyDate() {
    if (beginAt.isAfter(endedAt)) {
      return DateStatus.badOrder;
    } else if (endedAt.difference(beginAt).inMinutes < 3) {
      return DateStatus.tooShort;
    } else if (endedAt.difference(beginAt).inDays > 1) {
      return DateStatus.tooLong;
    } else {
      return DateStatus.ok;
    }
  }

  Map toMap() {
    var map = <String, dynamic>{};
    map["beginAt"] = EquidoDate.dateTimeToString(beginAt.toUtc());
    map["endedAt"] = EquidoDate.dateTimeToString(endedAt.toUtc());
    map["title"] = 'Sortie';
    map["place"] = 'Paddoc';

    return map;
  }
}
