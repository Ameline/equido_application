import 'package:equido_flutter/src/models/event/event.dart';

import '../../utils/equido_date.dart';
import 'event_output.dart';

class TrainingOutput extends EventOutput {
  late DateTime beginAt;
  late DateTime endedAt;
  late String? comment;
  late String title;

  TrainingOutput(
      {required this.beginAt,
      required this.endedAt,
      this.comment,
      required String horseId,
      required int id,
      required this.title})
      : super(id, horseId, EventType.training);

  DateStatus verifyDate() {
    if (beginAt.isAfter(endedAt)) {
      return DateStatus.badOrder;
    } else if (endedAt.difference(beginAt).inMinutes < 3) {
      return DateStatus.tooShort;
    } else if (endedAt.difference(beginAt).inDays > 1) {
      return DateStatus.tooLong;
    } else {
      return DateStatus.ok;
    }
  }

  Map toMap() {
    var map = <String, dynamic>{};
    map["title"] = title;
    map["beginAt"] = EquidoDate.dateTimeToString(beginAt.toUtc());
    map["endedAt"] = EquidoDate.dateTimeToString(endedAt.toUtc());
    map["comment"] = comment;
    map["rate"] = 0;

    return map;
  }
}
