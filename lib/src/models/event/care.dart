import 'package:equido_flutter/src/models/event/event.dart';
import 'package:equido_flutter/ui/event/list/view/care_story_tile.dart';
import 'package:equido_flutter/ui/event/list/view/user_story_list_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum CareType { shoeing, vaccine, wormer, other }

class Care extends Event {
  late String note;
  late CareType careType;

  Care(
      {required int id,
      required String title,
      required DateTime start,
      required String actorName,
      String? note,
      required String horseName,
      required String horseId,
      required this.careType})
      : super(id, title, start, actorName, horseName, EventType.care, horseId) {
    this.note = note ?? "";
  }

  Care.copy(Care care)
      : super(care.id, care.title, care.start, care.actorName, care.horseName,
            EventType.care, care.horseId) {
    note = care.note;
    careType = care.careType;
  }

  factory Care.fromJson(Map<String, dynamic> parsedJson) {
    return Care(
        id: parsedJson["id"],
        title: parsedJson['title'],
        start: DateTime.parse(parsedJson['doneOn']).toLocal(),
        actorName: parsedJson['healerName'],
        note: parsedJson['note'] ?? "",
        horseName: parsedJson['horseName'],
        horseId: parsedJson['horseId'],
        careType: getFromString(parsedJson['type']));
  }

  Map toJson() {
    return {
      'id': id,
      'eventName': title,
      'date': start,
      'actorname': actorName,
      'note': note,
      'horseName': horseName
    };
  }

  static CareType getFromString(String value) {
    if (value.toUpperCase() == "VACCINE") {
      return CareType.vaccine;
    } else if (value.toUpperCase() == "SHOEING") {
      return CareType.shoeing;
    } else if (value.toUpperCase() == "WORMER") {
      return CareType.wormer;
    } else if (value.toUpperCase() == "OTHER") {
      return CareType.other;
    }
    return CareType.other;
  }

  //@override
  StoryListTile<Event> createStoryListTile(Event event) {
    return CareStoryTile(event);
  }
}

class CareLogo {
  static const String whiteShoeingLogo = "assets/logos/shoeing_logo_white.png";
  static const String whiteVaccineLogo = "assets/logos/vaccine_logo_white.png";
  static const String whiteWormerLogo = "assets/logos/wormer_logo_white.png";
  static const String whiteCareLogo = "assets/logos/care_logo_white.png";

  // TODO: before deploy: logo black
  static const String blackShoeingLogo = "assets/logos/shoeing_logo_white.png";
  static const String blackVaccineLogo = "assets/logos/vaccine_logo_white.png";
  static const String blackWormerLogo = "assets/logos/wormer_logo_white.png";
  static const String blackCareLogo = "assets/logos/care_logo_white.png";

  static getAssetFromType(CareType type) {
    switch (type) {
      case CareType.shoeing:
        return whiteShoeingLogo;
      case CareType.vaccine:
        return whiteVaccineLogo;
      case CareType.wormer:
        return whiteWormerLogo;
      case CareType.other:
        return whiteCareLogo;
    }
  }

  static String getAssetBlackFromType(CareType type) {
    switch (type) {
      case CareType.shoeing:
        return blackShoeingLogo;
      case CareType.vaccine:
        return blackVaccineLogo;
      case CareType.wormer:
        return blackWormerLogo;
      case CareType.other:
        return blackCareLogo;
    }
  }

  static String getTitleFromType(CareType type, BuildContext context) {
    switch (type) {
      case CareType.shoeing:
        return "Ferrure";
      case CareType.vaccine:
        return "Vaccin";
      case CareType.wormer:
        return "Vermifuge";
      case CareType.other:
        return "Autre";
    }
  }
}
