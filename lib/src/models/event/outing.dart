import 'event.dart';

class OutingType {
  static String defaultOuting = places[1];

  static List<String> places = ['Paddoc', 'Près'];
}

class Outing extends Event {
  late DateTime stop;

  Outing(
      {required int id,
      required String title,
      required DateTime start,
      required this.stop,
      required String actorName,
      required String horseName,
      required String horseId})
      : super(
            id, title, start, actorName, horseName, EventType.outing, horseId);

  factory Outing.fromJson(Map<String, dynamic> parsedJson) {
    return Outing(
        id: parsedJson["id"],
        title: parsedJson['place'],
        start: DateTime.parse(parsedJson["createdAt"]),
        stop: DateTime.parse(parsedJson["endedAt"]),
        actorName: parsedJson['trainerName'],
        horseName: parsedJson['horseName'],
        horseId: parsedJson['horseId']);
  }

  Map toJson() {
    return {
      'place': title,
      'start': start,
      'stop': stop,
      'actorname': actorName,
      'horseName': horseName
    };
  }

  Duration? getDuration() {
    return stop.difference(start);
  }
}
