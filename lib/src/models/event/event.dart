import 'package:equido_flutter/src/models/event/training.dart';
import 'package:equido_flutter/src/models/event_output/event_output.dart';

import 'care.dart';
import 'outing.dart';

enum EventType { outing, training, care }

abstract class Event {
  int id;
  String title;
  DateTime start;
  String actorName; // Todo revoir le fonctionnement avec les actors name
  String horseName;
  String horseId;
  EventType eventType;

  Event(this.id, this.title, this.start, this.actorName, this.horseName,
      this.eventType, this.horseId);

  static EventType _getEventType(String value) {
    return EventType.values.firstWhere((e) {
      String s = e.toString().split('.')[1].toLowerCase();
      return s == value.toLowerCase();
    });
  }

  static Event fromJson(Map<String, dynamic> body) {
    EventType eventType = Event._getEventType(body["eventType"]);

    switch (eventType) {
      case EventType.care:
        return Care.fromJson(body);
      case EventType.outing:
        return Outing.fromJson(body);
      case EventType.training:
        return Training.fromJson(body);
    }
  }

  static List<Event> listFromJson(Map<String, dynamic> body) {
    List<Event> events = [];

    List careList = body["caresOutput"];
    List trainingList = body["trainingsOutput"];
    List outingList = body["outingsOutput"];

    events.addAll(careList.map((model) => Care.fromJson(model)));
    events.addAll(trainingList.map((model) => Training.fromJson(model)));
    events.addAll(outingList.map((model) => Outing.fromJson(model)));

    return events;
  }

  static toEventOutput(Event event) {
    return EventOutput(event.id, event.horseId, event.eventType);
  }

  String eventTypeString() {
    switch (eventType) {
      case EventType.outing:
        return "une sortie";
      case EventType.training:
        return "un entrainement";
      case EventType.care:
        return "un soin";
    }
  }
}
