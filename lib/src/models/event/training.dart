import 'package:equido_flutter/ui/widget/equido_asset.dart';

import 'event.dart';

class TrainingType {
  static const String jumping = "Obstacle";
  static const String dressage = "Dressage";
  static const String liberty = "Travail à pied";
  static const String outside = "Travail en exterieur";

  static String defaultTraining = dressage;

  static getAssetFromType(String type) {
    switch (type) {
      case jumping:
        return EquidoAsset.whiteJumpingLogo;
      case dressage:
        return EquidoAsset.whiteDressageLogo;
      case liberty:
        return EquidoAsset.whiteLibertyLogo;
      case outside:
        return EquidoAsset.whiteOutsideLogo;
      default:
        return EquidoAsset.whiteDressageLogo;
    }
  }

  static getAssetBlackFromType(String type) {
    switch (type) {
      case jumping:
        return EquidoAsset.blackJumpingLogo;
      case dressage:
        return EquidoAsset.blackDressageLogo;
      case liberty:
        return EquidoAsset.blackLibertyLogo;
      case outside:
        return EquidoAsset.blackOutsideLogo;
      default:
        return EquidoAsset.blackDressageLogo;
    }
  }

  static String getTitleFromType(String type) {
    switch (type) {
      case jumping:
        return "Obstacle";
      case dressage:
        return "Dressage";
      case liberty:
        return "A pied";
      case outside:
        return "Exterieur";
      default:
        return "Dressage";
    }
  }
}

class Training extends Event {
  late DateTime stop;
  late String note;

  Training(
      {required int id,
      required String title,
      required DateTime start,
      required this.stop,
      String? note,
      required String actorName,
      required String horseName,
      required String horseId})
      : super(id, title, start, actorName, horseName, EventType.training,
            horseId) {
    this.note = note ?? "";
  }

  factory Training.fromJson(Map<String, dynamic> parsedJson) {
    return Training(
        id: parsedJson["id"],
        title: parsedJson['title'].replaceAll("_", " "),
        start: DateTime.parse(parsedJson['beginAt']),
        stop: DateTime.parse(parsedJson['endedAt']),
        actorName: parsedJson['trainerName'],
        note: parsedJson['comment'] ?? "",
        horseName: parsedJson['horseName'],
        horseId: parsedJson['horseId']);
  }

  Map toJson() {
    return {
      'id': id,
      'type': title,
      'start': start,
      'stop': stop,
      'note': note,
      'actorName': actorName,
      'horseName': horseName
    };
  }

  Duration? getDuration() {
    return stop.difference(start).abs();
  }
}
