import 'dart:developer' as developer;

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class Storage {
  static String userIdKey = 'userId';

  static storeCurrentUserID(String uuid) async {
    const storage = FlutterSecureStorage();
    return await storage.write(key: userIdKey, value: uuid.toString());
  }

  static Future<String> getCurrentUserId() async {
    const storage = FlutterSecureStorage();
    return await storage.read(key: userIdKey).then((result) {
      if (result == null || result.isEmpty) {
        developer.log(
          '',
          name: 'equido.AuthenticationService.getCurrentUserId',
          error: "Récupération de l'ID de l'utilisateur impossible",
        );
      }
      return result ?? '';
    });
  }
}
