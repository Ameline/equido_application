import 'package:intl/intl.dart';

import 'equido_utils.dart';

class EquidoDate {
  static const String minutes = "mn";

  static bool isSameDate(DateTime date1, DateTime date2) {
    return date1.year == date2.year &&
        date1.month == date2.month &&
        date1.day == date2.day;
  }

  static String databaseDate(DateTime date) {
    return DateFormat('yyyy-MM-ddThh:mm:ss').format(date);
  }

  static String dateTimeToString(DateTime date) {
    return DateFormat('yyyy-MM-ddThh:mm:ss.SSS+0000').format(date);
  }

  static String dateTimeToStringWithoutHour(DateTime date) {
    return DateFormat('yyyy-MM-dd').format(date);
  }

  static String toHourFormat(DateTime date) {
    return DateFormat('kk:mm').format(date);
  }

  static String toHourFormatString(DateTime date) {
    return DateFormat('kk:mm').format(date).replaceFirst(':', 'h');
  }

  static String printDuration(Duration? duration) {
    if (duration == null) {
      return EquidoUtils.nR;
    }
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    if (duration.inMinutes <= 200) {
      return duration.inMinutes.toString() + minutes;
    }

    String twoDigitMinutes = duration.inMinutes.remainder(60).toString();
    String twoDigitHours = twoDigits(duration.inHours);

    if (twoDigitHours == "00" && twoDigitMinutes == "00") {
      return "00$minutes";
    } else if (twoDigitMinutes == "0") {
      return "${twoDigitHours}h";
    } else if (twoDigitHours == "0") {
      return "${twoDigitMinutes}minutes";
    } else {
      return "${twoDigitHours}h$twoDigitMinutes";
    }
  }

  static String translateDate(String date) {
    date = date.replaceAll("Monday", "Lundi");
    date = date.replaceAll("Tuesday", "Mardi");
    date = date.replaceAll("Wednesday", "Mercredi");
    date = date.replaceAll("Thursday", "Jeudi");
    date = date.replaceAll("Friday", "Vendredi");
    date = date.replaceAll("Saturday", "Samedi");
    date = date.replaceAll("Sunday", "Dimanche");

    date = date.replaceAll("January", "Janvier");
    date = date.replaceAll("February", "Février");
    date = date.replaceAll("March", "Mars");
    date = date.replaceAll("April", "Avril");
    date = date.replaceAll("May", "Mai");
    date = date.replaceAll("June", "Juin");
    date = date.replaceAll("July", "Juillet");
    date = date.replaceAll("August", "Août");
    date = date.replaceAll("September", "Septembre");
    date = date.replaceAll("October", "Octobre");
    date = date.replaceAll("November", "Novembre");
    date = date.replaceAll("December", "Décembre");

    return date;
  }

  static String makeDateEvent(DateTime date) {
    DateTime today = DateTime.now();
    int diffDays = DateFormat("yyyy/MM/dd")
        .format(date)
        .compareTo(DateFormat("yyyy/MM/dd").format(today));

    if (diffDays == 0) {
      return "Aujourd'hui";
    }

    return translateDate(DateFormat('EEEE dd MMMM').format(date));
  }

  static String makeDateEventWithYear(DateTime date) {
    DateTime today = DateTime.now();
    int diffDays = DateFormat("yyyy/MM/dd")
        .format(date)
        .compareTo(DateFormat("yyyy/MM/dd").format(today));

    if (diffDays == 0) {
      return "Aujourd'hui";
    }

    return translateDate(DateFormat('EEEE dd MMMM yyyy').format(date));
  }

  // Todo convertir l'argument en String
  static String translateWeekday(String weekday) {
    switch (weekday) {
      case "1":
        return "L";
      case "2":
        return "M";
      case "3":
        return "Me";
      case "4":
        return "J";
      case "5":
        return "V";
      case "6":
        return "S";
      case "7":
        return "D";
      default:
        return "";
    }
  }

  // Todo rename and comment
  static String weekdayExpanded(String weekday) {
    switch (weekday) {
      case "1":
        return "Lun.";
      case "2":
        return "Mar.";
      case "3":
        return "Mer.";
      case "4":
        return "Jeu.";
      case "5":
        return "Ven.";
      case "6":
        return "Sam.";
      case "7":
        return "Dim.";
      default:
        return "";
    }
  }

  static String formatYear(DateTime date) {
    return DateFormat("dd/MM/yyyy").format(date);
  }
}

extension DateOnlyCompare on DateTime {
  bool isSameDate(DateTime other) {
    return year == other.year && month == other.month && day == other.day;
  }
}
