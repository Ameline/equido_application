import 'dart:async';
import 'dart:developer' as developer;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equido_flutter/src/models/event/care.dart';
import 'package:equido_flutter/src/models/horse/horse.dart';
import 'package:postgres/postgres.dart';

import '../storage/storage.dart';
import 'postgres.dart';

class HorseRepository {
  static Future<QuerySnapshot<Map<String, dynamic>>>
      getAllForCurrentUser() async {
    developer.log('Get all horses request', name: 'equido.HorseAPi.getAll');

    var db = FirebaseFirestore.instance;

    return await db
        .collection("horse")
        .where("ownerId", arrayContains: await Storage.getCurrentUserId())
        .get();
  }

  static Future<DocumentSnapshot<Map<String, dynamic>>> get(
      String horseId) async {
    developer.log('Get horse request', name: 'equido.HorseAPi.get');

    var db = FirebaseFirestore.instance;

    return await db.collection("horse").doc(horseId.toString()).get();
  }

  static Future<void> create(Horse horse) async {
    developer.log('Create horse request', name: 'equido.HorseAPi.create');

    var db = FirebaseFirestore.instance;

    return await db
        .collection("horse")
        .doc(horse.id.toString())
        .set(horse.toMap())
        .then((value) => value)
        .catchError((error) => print('Erreur : $error'));
  }

  static Future<Result> update(Horse horse) async {
    developer.log('Update horse request', name: 'equido.HorseAPi.update');

    var db = FirebaseFirestore.instance;

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(
          Sql.named("UPDATE horse "
              "SET createdat = @createdat, birthdate = @birthdate, breed = @breed, color = @color, kind = @kind, name = @name, shoeingfrequency = @shoeingfrequency, size = @size, vaccinefrequency = @vaccinefrequency, wormerfrequency = @wormerfrequency, user_id = @user_id)"
              " WHERE id = @horseId"),
          parameters: {
            'horseId': horse.id,
            'createdat': DateTime.now(),
            'birthdate': horse.birthDate,
            'breed': horse.breed,
            'color': horse.color,
            'kind': horse.kind,
            'name': horse.name,
            'shoeingfrequency': horse.shoeing.frequency,
            'size': horse.size,
            'vaccinefrequency': horse.vaccine.frequency,
            'wormerfrequency': horse.wormer.frequency,
            'user_id': await Storage.getCurrentUserId()
          });
    });
  }

  static Future delete(int horseId) async {
    developer.log('Get horse request', name: 'equido.HorseAPi.delete');

    var db = FirebaseFirestore.instance;

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(
          Sql.named("DELETE FROM horse WHERE id = @horseId"),
          parameters: {'horseId': horseId});
    });
  }

  static Future<Result> getLastCare(String horseId, CareType caretype) async {
    developer.log('Get tracked care horse request',
        name: 'equido.HorseAPi.getTrackedCare');

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(
          Sql.named(
              "SELECT * FROM care WHERE horse_id=@horseId AND caretype=@caretype AND doneOn<=@date ORDER BY doneOn DESC"),
          parameters: {
            'horseId': horseId,
            'caretype': caretype.toString(),
            'date': DateTime.now().toString()
          });
    });
  }

  static Future<Result> getNextCare(int horseId, CareType caretype) async {
    developer.log('Get tracked care horse request',
        name: 'equido.HorseAPi.getTrackedCare');

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(
          Sql.named(
              "SELECT * FROM care WHERE horse_id=@horseId AND type=@caretype AND doneOn>@date ORDER BY doneOn DESC"),
          parameters: {
            'horseId': horseId,
            'caretype': caretype.toString(),
            'date': DateTime.now().toString()
          });
    });
  }
}
