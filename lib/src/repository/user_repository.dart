import 'dart:developer' as developer;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equido_flutter/src/models/user/profile_picture.dart';
import 'package:equido_flutter/src/repository/postgres.dart';
import 'package:equido_flutter/src/repository/user_keys.dart';
import 'package:postgres/postgres.dart';

import '../models/user/user.dart';

class UserRepository {
  static Future<Result> searchUser() async {
    developer.log('Search user request', name: 'equido.UserApi.searchUser');

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(Sql.named("SELECT * FROM equido_user"));
    });
  }

  static Future<Result> getUser(String firebaseId) async {
    developer.log('', name: 'equido.UserApi.getUser');

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(
          Sql.named("SELECT * FROM equi_user WHERE firebase_id = @firebaseId"),
          parameters: {'firebaseId': firebaseId});
    });
  }

  static Future<QuerySnapshot<Map<String, dynamic>>> searchUserByEmail(
      String email) async {
    developer.log('Search user request',
        name: 'equido.UserApi.searchUserByEmail');

    return await FirebaseFirestore.instance
        .collection(UserKeys.tableName)
        .where(UserKeys.emailKey, isEqualTo: email)
        .get();
  }

  static Future<void> updateUser(EquiUser user) async {
    developer.log('', name: 'equido.UserApi.updateUserPictureProfile');

    return await FirebaseFirestore.instance
        .collection(UserKeys.tableName)
        .doc(user.id)
        .set(user.toMap())
        .then((value) => print('Utilisateur mis a jour'))
        .catchError((error) => print('Erreur : $error'));
  }
}
