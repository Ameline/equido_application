import 'package:postgres/postgres.dart';

class Postgres {
  static Future<Connection> getDataBase() async {
    // open the database
    // TODO mettre ans un fichier de conf
    return await Connection.open(
      Endpoint(
        host: 'localhost',
        // localhost
        port: 5432,
        database: 'equido',
        username: 'myuser',
        password: 'Unicorn1@',
      ),
      settings: const ConnectionSettings(sslMode: SslMode.disable),
    );
  }
}
