import 'dart:async';
import 'dart:developer' as developer;

import 'package:equido_flutter/src/models/user/sharing.dart';
import 'package:equido_flutter/src/models/user/sharing_update.dart';
import 'package:postgres/postgres.dart';

import 'postgres.dart';

class SharingRepository {
  static Future<void> updateSharing(List<SharingUpdate> sharingUpdates) async {
    developer.log('Update sharing request',
        name: 'equido.SharingApi.updateSharing');

    sharingUpdates.map((update) {
      update.addedDays.map((added) async {
        return await Postgres.getDataBase().then((db) async {
          return await db.execute(
              Sql.named("INSERT INTO "
                  "sharedcalendar (createdat, date, user_id, horse_id, sharedUser_id)"
                  " VALUE (@createdat, @date, @user_id, @horse_id, @sharedUser_id)"),
              parameters: {
                'createdat': DateTime.now(),
                'date': added,
                'user_id': update.userId,
                'horse_id': update.horseId,
                'sharedUser_id': update.userSharedId
              });
        });
      });

      update.removedDays
          .map((removed) async => await Postgres.getDataBase().then((db) async {
                return await db.execute(
                    Sql.named("DELETE FROM sharedcalendar"
                        " WHERE date = @date AND user_id = @userId AND horse_id = @horseId)"),
                    parameters: {
                      'date': removed,
                      'user_id': update.userId,
                      'horse_id': update.horseId,
                    });
              }));
    });
  }

  static Future<Result> getSharing(String horseId) async {
    developer.log('Get sharing horse request',
        name: 'equido.SharingApi.getSharing');

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(
          Sql.named("SELECT * FROM sharing WHERE horse_id = @horseId"),
          parameters: {'horseId': horseId});
    });
  }

  static Future<Result> addSharingHorse(Sharing sharing) async {
    developer.log('Add sharing user request',
        name: 'equido.SharingApi.addSharingHorse');

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(
          Sql.named("INSERT INTO "
              "sharing (createdat, beginat, endedat, user_id, horse_id, sharedUser_id)"
              " VALUE (@createdat, @beginat, @endedat, @user_id, @horse_id, @sharedUser_id)"),
          parameters: {
            'createdat': DateTime.now(),
            'beginat': sharing.beginAt,
            'endedat': sharing.endedAt,
            'user_id': sharing.userId,
            'horse_id': sharing.horseId,
            'sharedUser_id': sharing.userSharedId
          });
    });
  }

  static Future<Result> deleteSharingHorse(
      String horseId, String userSharingId) async {
    developer.log('', name: 'equido.SharingApi.deleteSharingHorse');

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(
          Sql.named(
              "DELETE FROM sharing WHERE horse_id = @horseId AND sharedUser_id = userSharingId"),
          parameters: {'horseId': horseId, 'userSharingId': userSharingId});
    });
  }
}
