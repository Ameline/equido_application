class UserKeys {
  static const String tableName = 'equiUser';

  static const String idKey = 'id';
  static const String usernameKey = 'username';
  static const String firstNameKey = 'firstname';
  static const String lastNameKey = 'lastname';
  static const String emailKey = 'email';
  static const String registrationDateKey = 'registration_date';
  static const String profilPictureKey = 'profile_picture';

  static const String profilPictureNameKey = 'profile_picture_name';
  static const String profilPictureOffsetKey = 'offset';
  static const String profilPictureColorKey = 'color';

  static const String profilPictureColorRKey = 'r';
  static const String profilPictureColorGKey = 'g';
  static const String profilPictureColorBKey = '_b';
  static const String profilPictureColorOpacityKey = 'opacity';
}
