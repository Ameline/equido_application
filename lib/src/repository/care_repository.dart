import 'dart:async';
import 'dart:developer' as developer;

import 'package:equido_flutter/src/models/event_output/care_output.dart';
import 'package:equido_flutter/src/storage/storage.dart';
import 'package:postgres/postgres.dart';

import 'postgres.dart';

class CareRepository {
  static Future<Result> create(CareOutput care) async {
    developer.log('Create care request', name: 'equido.CareApi.create');

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(
          Sql.named("INSERT INTO "
              "care (createdAt, comment, doneon, title, type, user_id, horse_id) "
              "VALUES (@createdAt, @comment, @doneon, @title, @type, @user_id, @horse_id)"),
          parameters: {
            'createdAt': DateTime.now(),
            "comment": care.comment,
            "doneon": care.doneOn,
            "title": care.title,
            "caretype": care.careType.name,
            "user_id": await Storage.getCurrentUserId(),
            "horse_id": care.horseId
          });
    });
  }

  static Future<Result> delete(int careId) async {
    developer.log('Delete care request', name: 'equido.CareApi.delete');

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(Sql.named("DELETE FROM care WHERE id = @careId"), parameters: {'careId': careId});
    });
  }
}
