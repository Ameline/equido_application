import 'dart:async';
import 'dart:developer' as developer;

import 'package:equido_flutter/src/models/event_output/training_output.dart';
import 'package:postgres/postgres.dart';

import '../storage/storage.dart';
import 'postgres.dart';

class TrainingRepository {
  static Future<List<Result>> getTrainings(
      List<String> horseIds, String dateStart, String dateStop) async {
    developer.log('start: $dateStart, stop: $dateStop',
        name: 'equido.UserApi.getHorsesEventsBetweenDate');

    var results = await Postgres.getDataBase().then((db) async {
      return horseIds
          .map((e) => db.execute(
                  Sql.named(
                      "SELECT * FROM training WHERE horse_id = @horseIds AND beginat = @beginat AND endedat = @endedat"),
                  parameters: {
                    'horseIds': e,
                    'beginat': dateStart,
                    'endedat': dateStop
                  }))
          .toList();
    });

    return Future.wait(results);
  }

  static Future<Result> create(TrainingOutput training) async {
    developer.log('Create training request', name: 'equido.TrainingApi.create');

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(
          Sql.named("INSERT INTO "
              "training (createdat, beginat, comment, endedat, title, user_id, horse_id)"
              " VALUES (@createdat, @beginat, @comment, @endedat, @title, @user_id, @horse_id)"),
          parameters: {
            'createdat': DateTime.now(),
            'beginat': training.beginAt,
            "comment": training.comment,
            'endedat': training.endedAt,
            'title': training.title,
            'user_id': await Storage.getCurrentUserId(),
            'horse_id': training.horseId,
          });
    });
  }

  static Future<Result> delete(int trainingId) async {
    developer.log('Delete training request', name: 'equido.TrainingApi.delete');

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(
          Sql.named("DELETE FROM training WHERE id = @trainingId"),
          parameters: {'trainingId': trainingId});
    });
  }
}
