import 'dart:async';
import 'dart:developer' as developer;

import 'package:equido_flutter/src/models/event_output/outing_output.dart';
import 'package:postgres/postgres.dart';

import '../storage/storage.dart';
import 'postgres.dart';

class OutingRepository {
  static Future<List<Result>> getOutings(
      List<String> horseIds, String dateStart, String dateStop) async {
    developer.log('start: $dateStart, stop: $dateStop',
        name: 'equido.UserApi.getHorsesEventsBetweenDate');

    var results = await Postgres.getDataBase().then((db) async {
      return horseIds
          .map((e) => db.execute(
                  Sql.named(
                      "SELECT * FROM outing WHERE horse_id IN (@horseIds) AND beginat = @beginat AND endedat = @endedat"),
                  parameters: {
                    'horseIds': e,
                    'beginat': dateStart,
                    'endedat': dateStop
                  }))
          .toList();
    });

    return Future.wait(results);
  }

  static Future<Result> create(OutingOutput outing) async {
    developer.log('Create outing request', name: 'equido.outingApi.create');

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(
          Sql.named("INSERT INTO "
              "outing (createdat, beginat, endedat, place, title, user_id, horse_id)"
              " VALUE (@createdat, @beginat, @endedat, @place, @title, @user_id, @horse_id)"),
          parameters: {
            'createdat': DateTime.now(),
            'beginat': outing.beginAt,
            'endedat': outing.endedAt,
            'place': outing.place,
            'title': outing.title,
            'user_id': await Storage.getCurrentUserId(),
            'horse_id': outing.horseId,
          });
    });
  }

  static Future delete(int outingId) async {
    developer.log('Delete outing request', name: 'equido.outingApi.delete');

    return await Postgres.getDataBase().then((db) async {
      return await db.execute(
          Sql.named("DELETE FROM outing WHERE id = @outingId"),
          parameters: {'outingId': outingId});
    });
  }
}
