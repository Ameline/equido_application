import 'dart:io';

import 'package:equido_flutter/src/bloc/events_bloc.dart';
import 'package:equido_flutter/src/bloc/horse_bloc.dart';
import 'package:equido_flutter/src/bloc/user_bloc.dart';
import 'package:equido_flutter/src/equido_localisations.dart';
import 'package:equido_flutter/src/state/builder_horse_state.dart';
import 'package:equido_flutter/ui/connection/view/bottom_tab_connection.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:global_configs/global_configs.dart';

import 'configs/dev.dart' as configs;
import 'firebase_options.dart';

late final FirebaseApp app;
late final FirebaseAuth auth;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  app = await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  auth = FirebaseAuth.instanceFor(app: app);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    const title = 'Equido';

    GlobalConfigs().loadFromMap(configs.dev);

    HttpOverrides.global = MyHttpOverrides();

    return BlocProvider<HorseBloc>(
        create: (BuildContext context) =>
            HorseBloc(context.findAncestorStateOfType()),
        child: BlocProvider<EventBloc>(
            create: (BuildContext context) =>
                EventBloc(context.findAncestorStateOfType()),
            child: BlocProvider<UserBloc>(
                create: (BuildContext context) =>
                    UserBloc(context.findAncestorStateOfType()),
                child: MaterialApp(
                    debugShowMaterialGrid: false,
                    locale: const Locale("fr"),
                    localizationsDelegates: const [
                      EquidoLocalizationsDelegate(),
                      GlobalCupertinoLocalizations.delegate,
                      GlobalMaterialLocalizations.delegate,
                      GlobalWidgetsLocalizations.delegate,
                    ],
                    supportedLocales: const [Locale('fr', '')],
                    title: title,
                    initialRoute: '/',
                    routes: {
                      '/': (context) => StreamBuilder<User?>(
                            stream: auth.authStateChanges(),
                            builder: (context, snapshot) {
                              if (snapshot.hasData && snapshot.data != null) {
                                return const BuilderHorse();
                              }
                              return const BottomTabConnection();
                            },
                          ),
                    },
                    theme: ThemeData(
                      // Define the default brightness and colors.
                      primaryColor: Colors.cyan,
                    )))));
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
