final Map<String, dynamic> dev = {
  "url": {
    "server": "http://localhost:8070",
    "localhost": "http://localhost:8070",
    "android": "http://10.0.2.2:8070",
    "prod": "https://ws.equido.io:1003"
  },
  "test_mode": false,
  "firebase_test_mode": false
};
